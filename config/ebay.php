<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Modes
    |--------------------------------------------------------------------------
    |
    | This package supports sandbox and production modes.
    | You may specify which one you're using throughout
    | your application here.
    |
    | Supported: "sandbox", "production"
    |
    */

    'mode' => env('EBAY_MODE', 'sandbox'),


    /*
    |--------------------------------------------------------------------------
    | Site Id
    |--------------------------------------------------------------------------
    |
    | The unique numerical identifier for the eBay site your API requests are to be sent to.
    | For example, you would pass the value 3 to specify the eBay UK site.
    | A complete list of eBay site IDs is available
    |(http://developer.ebay.com/devzone/finding/Concepts/SiteIDToGlobalID.html).
    |
    |
    */

    'siteId' => env('EBAY_SITE_ID','0'),

    /*
    |--------------------------------------------------------------------------
    | KEYS
    |--------------------------------------------------------------------------
    |
    | Get keys from EBAY. Create an app and generate keys.
    | (https://developer.ebay.com)
    | You can create keys for both sandbox and production also
    | User token can generated here.
    |
    */

    'sandbox' => [
        'credentials' => [
            'devId' => env('3e954e7d-9db5-4e6d-a3e6-6dee4f20ac69'),
            'appId' => env('NathanHa-britishr-SBX-c2cd16de6-9566e548'),
            'certId' => env('SBX-2cd16de6bf72-6580-4d71-8063-b57f'),
        ],
        'authToken' => env('EBAY_SANDBOX_AUTH_TOKEN'),
        'oauthUserToken' => env('v^1.1#i^1#I^3#p^3#f^0#r^0#t^H4sIAAAAAAAAAOVXe2wURRzu9UUaHiaGV4SQczVBgb2d3dt77MIdXmkbaqAtXCFAg2R2d7Zdurd72Zlte8TgpQgRMSgBrZGYFEJMCPIHSGIUUR5BMT4gxKgh0UjQCCWCiIoRjM5eW3qtAfogpon7z2Zmfq/vm29+mQHZ0rJZmxZuujHeN6awMwuyhT4fPxaUlZbMnlBU+FBJAcgz8HVmH80WtxddnIdhykzLSxFO2xZG/raUaWE5NxljXMeSbYgNLFswhbBMVDmZWLxIFgJATjs2sVXbZPzVFTFG0HkkCCgqBqEUUQWFzlq9MevtGINUXdJEPiJpPIC6Cuk6xi6qtjCBFqH+gI+yIMwKoXo+IgdFOQQCEgCrGP9y5GDDtqhJADDxXLlyztfJq/XupUKMkUNoECZenahK1iaqKypr6udxebHiPTwkCSQu7j9aYGvIvxyaLrp7GpyzlpOuqiKMGS7enaF/UDnRW8wwys9RjZQICAsACkGoKwII3xcqq2wnBcnd6/BmDI3Vc6YysohBMvdilLKhrEUq6RnV0BDVFX7vt8SFpqEbyIkxleWJlcuSlUsZf7KuzrFbDA1pHlI+KPIhPhwNMnGCMKUQOWsUR9VQS0+m7nA9PA9ItcC2NMNjDftrbFKOaNloIDlCHjnUqNaqdRI68UrKt5N6SIxK1I7r3UaXNFnexqIUZcKfG957C3o10aeC+6UKPRQUlQgUNTEKIwqIDlSFd9aHo4y4tzmJujrOqwUpMMOmoNOMSNqEKmJVSq+bQo6hycGQLgSjOmK1sKSzoqTrrBLSwiyvIwQQUhRViv6vBEKIYyguQbdFMnAhhzLGJFU7jeps01AzzECTXNfpkUQbjjFNhKRljmttbQ20BgO208gJAPDcisWLkmoTStG22mtr3NuYNXLiUBH1woZMMmlaTRvVHk1uNTLxoKPVQYdkyt0MHSeRadJfr377VRgfOHsHqNiDOrpAev6YBoBpI+DJO6DaKc6G9Dh7U2tyFfsHY8Qpbobm15ATcBDUbMvMDN6v0aXy7fYenBOmuxHoPokUxoCM3lkfWoAhJDWsFqpl28kMEWZ/5yH4QFW1XYsMJ12P6xA8dNfUDdP0jutwEua5D6VMC5oZYqh4OCnz2jGlFxuNTWSocegc7eHUX4UEmnafnEZ02BPpdLU2ug57DSRN0FoIWcWhnR83OWyyfAWrCqrGhzUUZqVQOIxCYnREuCtQy3+Iu7i9cN3gujmSQiKKaKykKSFWRGGNhUGKmQJHok7vk2pYGhHuBaZBZV+fGW0dfqGNCdJGBo3etEYXKE+3vbJV9IjAhkNRwIpahGejIBykV6+IPljI3B3vK/+6q3L9X4vxgtzHt/uOgHbfO/TBCSKA5WeDx0uLlhUXjWOwQVAAQ0tT7LaAAfUA7U8WfQw5KNCMMmloOIWlvoZpXfNv5b1TO1eDqbdfqmVF/Ni8ZyuY3rdSwj8wZTxP4QohPhIUQ2AVeKRvtZifXDzx03kd9ZfLpeRvV+Izbr23fd91+7O1YPxtI5+vpKC43VewpEqbcXD6bnD1wvd/7z/2rb369y0lu47d+mvyl7v/PHez9fypl3d8cvHQYXhcDL793JXSP95tOsSsSDZYBc8a2RNHxLe+e146UfJSar00t+CbcWeLfr16tGbv3G1ju/Z/dfz1X9K1Yw6eq9l02HiRa/7BmfmRs03/+sfO97Nl2xMtXZevL6u9OcVIxE7uTB2dpH+86wV382NvqODA1KMHu/Zt3HHpQMONoo5nak7PPFkbO3W580rFmQ8/3zo5W8VfW7/ziZUbXosd2nkhuW71q9yZOfKGiRWg8AutYOsHe2ZJT52es3H+009W7qnSOhoe5rZMO5+0Nl9z9j74CntWuoQmNa/c8dPPE7reHFc/YWL3Nv4DCZadk0EQAAA='),
    ],
    'production' => [
        'credentials' => [
            'devId' => env('3e954e7d-9db5-4e6d-a3e6-6dee4f20ac69'),
            'appId' => env('NathanHa-britishr-PRD-12ccf9842-a45ee08b'),
            'certId' => env('PRD-2ccf9842cff6-a3ec-4ae3-9cfc-e986'),
        ],
        'authToken' => env('EBAY_PROD_AUTH_TOKEN'),
        'oauthUserToken' => env('v^1.1#i^1#I^3#p^3#f^0#r^0#t^H4sIAAAAAAAAAOVXe2wURRzu9UUaHiaGV4SQczVBgb2d3dt77MIdXmkbaqAtXCFAg2R2d7Zdurd72Zlte8TgpQgRMSgBrZGYFEJMCPIHSGIUUR5BMT4gxKgh0UjQCCWCiIoRjM5eW3qtAfogpon7z2Zmfq/vm29+mQHZ0rJZmxZuujHeN6awMwuyhT4fPxaUlZbMnlBU+FBJAcgz8HVmH80WtxddnIdhykzLSxFO2xZG/raUaWE5NxljXMeSbYgNLFswhbBMVDmZWLxIFgJATjs2sVXbZPzVFTFG0HkkCCgqBqEUUQWFzlq9MevtGINUXdJEPiJpPIC6Cuk6xi6qtjCBFqH+gI+yIMwKoXo+IgdFOQQCEgCrGP9y5GDDtqhJADDxXLlyztfJq/XupUKMkUNoECZenahK1iaqKypr6udxebHiPTwkCSQu7j9aYGvIvxyaLrp7GpyzlpOuqiKMGS7enaF/UDnRW8wwys9RjZQICAsACkGoKwII3xcqq2wnBcnd6/BmDI3Vc6YysohBMvdilLKhrEUq6RnV0BDVFX7vt8SFpqEbyIkxleWJlcuSlUsZf7KuzrFbDA1pHlI+KPIhPhwNMnGCMKUQOWsUR9VQS0+m7nA9PA9ItcC2NMNjDftrbFKOaNloIDlCHjnUqNaqdRI68UrKt5N6SIxK1I7r3UaXNFnexqIUZcKfG957C3o10aeC+6UKPRQUlQgUNTEKIwqIDlSFd9aHo4y4tzmJujrOqwUpMMOmoNOMSNqEKmJVSq+bQo6hycGQLgSjOmK1sKSzoqTrrBLSwiyvIwQQUhRViv6vBEKIYyguQbdFMnAhhzLGJFU7jeps01AzzECTXNfpkUQbjjFNhKRljmttbQ20BgO208gJAPDcisWLkmoTStG22mtr3NuYNXLiUBH1woZMMmlaTRvVHk1uNTLxoKPVQYdkyt0MHSeRadJfr377VRgfOHsHqNiDOrpAev6YBoBpI+DJO6DaKc6G9Dh7U2tyFfsHY8Qpbobm15ATcBDUbMvMDN6v0aXy7fYenBOmuxHoPokUxoCM3lkfWoAhJDWsFqpl28kMEWZ/5yH4QFW1XYsMJ12P6xA8dNfUDdP0jutwEua5D6VMC5oZYqh4OCnz2jGlFxuNTWSocegc7eHUX4UEmnafnEZ02BPpdLU2ug57DSRN0FoIWcWhnR83OWyyfAWrCqrGhzUUZqVQOIxCYnREuCtQy3+Iu7i9cN3gujmSQiKKaKykKSFWRGGNhUGKmQJHok7vk2pYGhHuBaZBZV+fGW0dfqGNCdJGBo3etEYXKE+3vbJV9IjAhkNRwIpahGejIBykV6+IPljI3B3vK/+6q3L9X4vxgtzHt/uOgHbfO/TBCSKA5WeDx0uLlhUXjWOwQVAAQ0tT7LaAAfUA7U8WfQw5KNCMMmloOIWlvoZpXfNv5b1TO1eDqbdfqmVF/Ni8ZyuY3rdSwj8wZTxP4QohPhIUQ2AVeKRvtZifXDzx03kd9ZfLpeRvV+Izbr23fd91+7O1YPxtI5+vpKC43VewpEqbcXD6bnD1wvd/7z/2rb369y0lu47d+mvyl7v/PHez9fypl3d8cvHQYXhcDL793JXSP95tOsSsSDZYBc8a2RNHxLe+e146UfJSar00t+CbcWeLfr16tGbv3G1ju/Z/dfz1X9K1Yw6eq9l02HiRa/7BmfmRs03/+sfO97Nl2xMtXZevL6u9OcVIxE7uTB2dpH+86wV382NvqODA1KMHu/Zt3HHpQMONoo5nak7PPFkbO3W580rFmQ8/3zo5W8VfW7/ziZUbXosd2nkhuW71q9yZOfKGiRWg8AutYOsHe2ZJT52es3H+009W7qnSOhoe5rZMO5+0Nl9z9j74CntWuoQmNa/c8dPPE7reHFc/YWL3Nv4DCZadk0EQAAA='),
    ]
];