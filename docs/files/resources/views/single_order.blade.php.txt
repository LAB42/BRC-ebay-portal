@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-4">		
		<div style="
		cursor: pointer; color: #FFF; vertical-align: middle; text-align: center; font-weight: bold; padding: 5px; width: 30%;background: {{$status->background}};">
			{{$status->name}}
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-12">
				<h1>&pound;{{$total}}</h1>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-6">
				<h3>{{$shippingType}}</h3>
				<h4>
					{{$order->shippingCarrierCode}} 
					|
					{{$order->shippingServiceCode}}
				</h4>				
				<div class="row">
					<div class="col-sm-6">
						<p>
							<strong>Min Delivery</strong><br>
							{{$order->minEstimatedDeliveryDate}}
						</p>
						<p>
							<strong>Paid</strong><br>
							&pound;{{$shippingPaid}}
						</p>
					</div>
					<div class="col-sm-6">
						<p>
							<strong>Max Delivery</strong><br>
							{{$order->maxEstimatedDeliveryDate}}
						</p>
						<p>
							<strong>Actual</strong><br>							
						</p>
						<form action="#" method="post">
								<input type="text" name="actualPostage" id="actualPostage" value="{{$shippingActual}}" style="width: 100%;">
								<input type="submit" value="Update" style="width: 100%; background: #eee; color: #333; cursor: pointer;">
						</form>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<a href="#" class="cta" style="font-size: 0.8em;">Print Label</a><br>
				<a href="#" class="cta" style="font-size: 0.8em;">Book Courier</a><br>
			</div>
		</div>		
		<hr>		
	</div>
	<div class="col-sm-4">		
		<dl>
			<dt>Username</dt>
			<dd>{{$order->username}}</dd>

			<dt>Order date</dt>
			<dd>{{$order->orderDate}}</dd>

			<dt>Name</dt>
			<dd>{{$order->contactFullName}}</dd>

			<dt>Email</dt>
			<dd>{{$order->contactEmail}}</dd>

			<dt>Phone</dt>
			<dd>{{$order->contactPhoneNumber}}</dd>
		</dl>
	</div>	
	<div class="col-sm-4">
		<h4 class="sectionHeader">Payment Address</h4>
		<p>
			{{$order->addressLine1}}<br>
			{{$order->addressLine2}}<br>
			{{$order->city}}<br>
			{{($order->stateOrProvince) ? $order->stateOrProvince : ''}}
			{{$order->postalCode}}<br>
			{{$order->country}} ({{$order->countryCode}})
		</p>

		<h4 class="sectionHeader">Shipping Address</h4>
		<p>
			{{$order->contactAddressLine1}}<br>
			{{$order->contactAddressLine2}}<br>
			{{$order->contactCity}}<br>
			{{($order->contactStateOrProvince) ? $order->contactStateOrProvince : ''}}
			{{$order->contactPostalCode}}<br>
			{{$order->contactCountry}} ({{$order->contactCountryCode}})
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<table class="table">
			<tr>
				<th class="text-center">ID</th>
				<th class="text-center">Name</th>
				<th class="text-center">Location</th>
				<th class="text-center">QTY</th>			
				<th class="text-center">Price</th>
				<th class="text-center">Picture</th>
			</tr>

			@foreach($items as $item)

			<tr>
				<td style="width: 5%; text-align: center;">{{$item->id}}</td>
				<td style="width: 30%; text-align: center;">{{$item->name}}</td>			
				<td style="width: 15%; text-align: center;">{{$item->location}}</td>
				<td style="width: 5%; text-align: center;">{{$item->qty}}</td>
				<td style="width: 10%; text-align: center;">&pound;{{$item->price}}</td>			
				<td style="width: 15%; text-align: center;">
					<img src="{{asset('img/placeholder.jpg')}}" alt=""
					style="width: 125px;">
				</td>
			</tr>
			@endforeach
		</table>	
	</div>
</div>
@endsection
