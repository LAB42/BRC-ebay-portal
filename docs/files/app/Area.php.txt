<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'areas';
    protected $primaryKey = 'id';

	public function shop(){
		return $this->belongsTo('App\Shop');
	}    
}


