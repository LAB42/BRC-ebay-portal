<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InventoryStatus;
use App\Inventory;
use App\Area;
use App\Shop;

class InventoryController extends BaseController
{   

    /**
     * Default method for this controller
     *
     * Returns all items in inventory with the option to filter by month and year
     * 
     * @category Controller method
     * @param  string $month  Month for filter
     * @param  string $year   Year for filter
     * @return void         Blade
     */
    public function all($month = NULL, $year = NULL){
        
    	$data['page']['title'] = 'Inventory';

        //Get list of all areas         
        $data['areaList'] = array();
    	foreach(Area::all() as $area):
            $data['areaList'][] = $area->name;
        endforeach;

        //If month or year filter is set
        if($month != NULL && $year != NULL):                
            $sortByDate = BRC::getDateRange($month,$year);
            
            $data['items'] = Inventory::where('date_received', '>=', $sortByDate['lower'])->where('date_received', '<=', $sortByDate['upper'])->orderBy('date_received', 'ASC')->with('inventoryStatus')->with('shop')->get();            
        else:
        //Default page state
            $data['items'] = Inventory::orderBy('date_received', 'ASC')->with('inventoryStatus')->with('shop')->get();
        endif;

        //Set date filter info for conditional formatting
        $data['currentDateFilter'] = array(
            'month' => $month,
            'year' => $year
        );

    	return view('inventory', $data);
    }

    /**
     * Returns all inventory items marked as listed
     *
     * Has option to filter by month and year
     *
     * @category Controller method
     * @param  string $month    Month for filter
     * @param  string $year     Year for filter
     * @return void             Blade  
     */
    public function listed($month = NULL, $year = NULL){  
        $sortByDate = BRC::getDateRange($month,$year);

        $data['areaList'] = array();
        foreach(Area::all() as $area):
            $data['areaList'][] = $area->name;
        endforeach;

        $data['page']['title'] = 'Inventory';
        $data['areasList'] = Area::all();
        $data['page']['statusSet'] = TRUE;
        $data['page']['status'] = 'LISTED';
        
        $statusId = InventoryStatus::where('name', '=', 'Listed')->firstOrFail();
        $data['items'] = Inventory::where('date_received', '>=', $sortByDate['lower'])->where('date_received', '<=', $sortByDate['upper'])->where('status', '=', $statusId->id)->orderBy('date_received', 'ASC')->get();       

        return view('inventory', $data);
    }

    /**
     * Returns all inventory items marked as NIS
     *
     * Any item with a status containing RTS is considered to be NIS
     * 
     * Has option to filter by date
     * @param  string $month Month for filter
     * @param  string $year  Year for filter
     * @return void        Blade
     */
    public function nis($month = NULL, $year = NULL){  
        $sortByDate = BRC::getDateRange($month,$year);

        $data['areaList'] = array();
        foreach(Area::all() as $area):
            $data['areaList'][] = $area->name;
        endforeach;

        $data['page']['title'] = 'Inventory';
        $data['areasList'] = Area::all();
        $data['page']['statusSet'] = TRUE;
        $data['page']['status'] = 'NIS';
        
        $statusIds = InventoryStatus::select('id')->where('name', 'ILIKE', '%rts%')->get();
        $data['items'] = Inventory::where('date_received', '>=', $sortByDate['lower'])->where('date_received', '<=', $sortByDate['upper'])->whereIn('status', $statusIds)->orderBy('date_received', 'ASC')->get();        
        

        return view('inventory', $data);
    }

    public function pending($month = NULL, $year = NULL){  
        
        $sortByDate = BRC::getDateRange($month,$year);

        $data['areaList'] = array();
        foreach(Area::all() as $area):
            $data['areaList'][] = $area->name;
        endforeach;

        $data['page']['title'] = 'Inventory';
        $data['areasList'] = Area::all();
        $data['page']['statusSet'] = TRUE;
        $data['page']['status'] = 'Pending';
        
        $statusId = InventoryStatus::where('name', '=', 'Pending')->firstOrFail()->id;        
        $data['items'] = Inventory::where('date_received', '>=', $sortByDate['lower'])->where('date_received', '<=', $sortByDate['upper'])->where('status', '=', $statusId)->orderBy('date_received', 'ASC')->get();        
        

        return view('inventory', $data);
    }

    public function item($id){    	
    	$data['page']['title'] = 'Item #'.$id;
    	$data['item'] = Inventory::where('id', '=', $id)->with('inventoryStatus')->with('shop')->firstOrFail();

    	if($data['item'] == NULL):
    		echo 'Item not found';
    		exit();
    	endif;

    	return view('item', $data);
    }

    public function search(){
        $query = 'adorable';
        $results = Inventory::where('item_name', 'ILIKE', '%'.$query.'%')->get();
        foreach($results as $result):
            $url = url('item/'.$result['id']);
            echo '<a href="'.$url.'">'.$result['item_name'].'</a>';
            echo '<br>';
        endforeach;

    }

    public function inventoryByArea($area = NULL)
    {
        $data['areasList'] = Area::all();
        
        if($area != NULL):
            $area = Area::find($area);
            $data['page']['areaSet'] = TRUE;
            $data['page']['area'] = $area;            
            $data['page']['areaName'] = $area->name;
            $name = $data['page']['areaName'];
            $data['page']['areaId'] = $area;

            $shopsInArea = Shop::where('area', 'ILIKE', $area->name)->get();
            foreach($shopsInArea as $sia):
                $shopIds[] = $sia->id;
            endforeach;
            $data['items'] = Inventory::where('date_received', '>=', '2018-01-01')->whereIn('shopId', $shopIds)->with('shop')->with('inventoryStatus')->orderBy('date_received', 'ASC')->get();                       
        else:
            $area = 'All';
                $data['page']['areaSet'] = TRUE;                     
            $data['page']['areaName'] = 'All areas';
            $name = $data['page']['areaName'];
            $data['page']['areaId'] = $area;
            $data['items'] = Inventory::where('date_received', '>=', '2018-01-01')->orderBy('date_received', 'ASC')->get();
        endif;

            return view('inventory_by_area', $data);

    }
}

