<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;

class ListingController extends Controller
{
	public function index()
	{		
		$data['page']['title'] = 'Listings';

		return view('listings', $data);
	}

	public function createListing($id = NULL)
	{
		$data['page']['title'] = 'Create Listing';

		if($id != NULL):
			$data['template'] = Inventory::where('id', '=', $id)->firstOrFail();

			if($data['template']->gift_aid === 'N/A'):
				$data['template']->gift_aid = '';		
			endif;
		else:
			$data['template'] = NULL;
		endif;

		return view('create_listing', $data);
	}

	public function doCreateListing(Request $request)
	{
		$data['page']['title'] = 'Listed Created';

		$data['output'] = $request->all();

		return view('created_listing', $data);
	}
}

