<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAreaColShops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shops', function($table){
            $table->dropColumn('area');
        });

        Schema::table('shops', function($table){
            $table->integer('areaId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shops', function($table){
            $table->dropColumn('areaId');
        });

        Schema::table('shops', function($table){
            $table->integer('area');
        });
    }
}
