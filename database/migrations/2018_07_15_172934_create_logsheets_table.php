<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logsheets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id');
            $table->integer('items');
            $table->text('shop_manager');
            $table->text('received_by');
            $table->timestamp('due_date');
            $table->timestamp('received_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logsheets');
    }
}
