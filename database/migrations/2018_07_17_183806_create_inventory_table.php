<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shopId');
            $table->text('item_name');
            $table->text('comments');
            $table->integer('status');
            $table->string('location');
            $table->integer('crate_item_number')->nullable();
            $table->string('est_value')->nullable();
            $table->integer('price')->nullable();
            $table->integer('gift_aid')->nullable();            
            $table->text('category_text')->nullable();
            $table->integer('ebay_category_number')->nullable();
            $table->text('weight_text')->nullable();
            $table->integer('weight_int')->nullable();
            $table->string('weight_unit')->nullable();
            $table->string('size_text')->nullable();
            $table->string('condition')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory');
    }
}
