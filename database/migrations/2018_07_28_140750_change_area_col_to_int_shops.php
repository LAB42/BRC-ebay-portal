<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAreaColToIntShops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shops', function($table){
            $table->dropColumn('area');            
        });

        Schema::table('shops', function($table){
            $table->integer('area')->nullable();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shops', function($table){
            $table->dropColumn('area');            
        });

        Schema::table('shops', function($table){
            $table->string('area')->nullable();
        });
    }
}
