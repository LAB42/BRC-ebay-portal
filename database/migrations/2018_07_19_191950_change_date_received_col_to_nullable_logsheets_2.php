<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDateReceivedColToNullableLogsheets2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('logsheets', function($table){
            $table->dropColumn('date_received');
        });

        Schema::table('logsheets', function($table){
            $table->dropColumn('received_date');
        });

        Schema::table('logsheets', function($table){
            $table->timestamp('received_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logsheets', function($table){
            $table->timestamp('date_received')->change();
        });
    }
}
