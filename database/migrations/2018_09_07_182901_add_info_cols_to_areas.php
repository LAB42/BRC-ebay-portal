<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInfoColsToAreas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas', function($table){
            $table->string('area_manager')->nullable();
            $table->string('area_manager_email')->nullable();
            $table->string('cluster_manager')->nullable();
            $table->string('cluster_manager_email')->nullable();
            $table->text('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas', function($table){
            $table->dropColumn('area_manager');
            $table->dropColumn('area_manager_email');
            $table->dropColumn('cluster_manager');
            $table->dropColumn('cluster_manager_email');
            $table->dropColumn('notes');
        });
    }
}
