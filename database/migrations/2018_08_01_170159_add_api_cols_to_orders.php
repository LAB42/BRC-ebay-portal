<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApiColsToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function($table){
            $table->string('buyer');
            $table->string('buyer_checkout_notes');
            $table->text('cancel_status');
            $table->timestamp('creation_date');
            $table->string('destination_timezone');
            $table->string('fd_addressLine1');
            $table->string('fd_addressLine2');
            $table->string('fd_city');
            $table->string('fd_country_code');
            $table->string('fd_country');
            $table->string('fd_postal_code');
            $table->string('fd_state_or_province');
            $table->string('fullfillment_instuctions');
            $table->string('min_estimated_delivery_date');
            $table->string('max_estimated_delivery_date');
            $table->string('carrier_code');
            $table->string('service_code');
            $table->string('ship_addressLine1');
            $table->string('ship_addressLine2');
            $table->string('ship_city');
            $table->string('ship_country_code');
            $table->string('ship_country');
            $table->string('ship_postal_code');
            $table->string('ship_state_or_province');
            $table->string('full_name');
            $table->string('buyer_email');
            $table->string('buyer_phone');
            $table->string('reference_id');
            $table->timestamp('last_mod');
            $table->text('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function($table){
            $table->dropColumn('buyer');
            $table->dropColumn('buyer_checkout_notes');
            $table->dropColumn('cancel_status');
            $table->dropColumn('creation_date');
            $table->dropColumn('destination_timezone');
            $table->dropColumn('fd_addressLine1');
            $table->dropColumn('fd_addressLine2');
            $table->dropColumn('fd_city');
            $table->dropColumn('fd_country_code');
            $table->dropColumn('fd_country');
            $table->dropColumn('fd_postal_code');
            $table->dropColumn('fd_state_or_province');
            $table->dropColumn('fullfillment_instuctions');
            $table->dropColumn('min_estimated_delivery_date');
            $table->dropColumn('max_estimated_delivery_date');
            $table->dropColumn('carrier_code');
            $table->dropColumn('service_code');
            $table->dropColumn('ship_addressLine1');
            $table->dropColumn('ship_addressLine2');
            $table->dropColumn('ship_city');
            $table->dropColumn('ship_country_code');
            $table->dropColumn('ship_country');
            $table->dropColumn('ship_postal_code');
            $table->dropColumn('ship_state_or_province');
            $table->dropColumn('full_name');
            $table->dropColumn('buyer_email');            
            $table->dropColumn('buyer_phone');
            $table->dropColumn('reference_id');
            $table->dropColumn('last_mod');
            $items->dropColumn('items');
        });
    }
}
