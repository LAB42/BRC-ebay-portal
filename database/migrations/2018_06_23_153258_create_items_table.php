<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->text('area');
            $table->text('shop_code');
            $table->text('shop_name');
            $table->text('crate_id');
            $table->text('crate_item_number');
            $table->text('gift_aid');
            $table->text('est_value');
            $table->text('status');
            $table->text('comments');
            $table->timestamp('date_received');
            $table->text('weight_text');
            $table->text('size_text');
            $table->integer('weight_int');
            $table->text('weight_unit');
            $table->text('category');
            $table->integer('value_int');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
