let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react('resources/assets/js/app.js', 'public/js')		
.react('resources/assets/js/item.js', 'public/js')
.react('resources/assets/js/homeDashboard.js', 'public/js')
.react('resources/assets/js/createListing.js', 'public/js')
.js('resources/assets/js/keymaster.js', 'public/js' )
.sass('resources/assets/sass/theme.scss', 'public/css')
   .sass('resources/assets/sass/app.scss', 'public/css')
.sass('resources/assets/sass/returnCard.scss', 'public/css')
.sass('resources/assets/sass/binCard.scss', 'public/css')
.sass('resources/assets/sass/invoice.scss', 'public/css')
.sass('resources/assets/sass/orderSheet.scss', 'public/css')
.sass('resources/assets/sass/rtsLabel.scss', 'public/css')
   ;
