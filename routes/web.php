<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::auth();

Route::group(['middleware' => 'auth'], function()
{
Route::get('/', 'HomeController@index')->name('home');
Route::get('home', 'HomeController@index')->name('home');

Route::get('account/current', 'AccountController@getCurrentUserId');

Route::get('listings/mine', 'ListingController@mine');
Route::get('forecast', 'HomeController@forecast')->name('forecast');

Route::get('contact', 'HomeController@contact')->name('contact');

Route::group(['middleware' => ['role:owner']], function() {
    Route::resource('areas', 'AreaController', [
    'names' => [
        'index' => 'areas',
        'show' => 'area-show',
        'create' => 'area-create',
        'store' => 'area-store',
        'edit' => 'area-edit',
        'create' => 'area-create',
        'store' => 'area-store',
        'update' => 'area-update',
        //'destroy' => 'area-delete'
        ]
    ]);
});

Route::group(['middleware' => ['role:owner|ebaymanagement']], function() {
    Route::resource('feedback', 'FeedbackController', [
    'names' => [
        'index' => 'feedback',
        'show' => 'feedback-show',
        'create' => 'feedback-create',
        'store' => 'feedback-store',
        'edit' => 'feedback-edit',
        'create' => 'feedback-create',
        'store' => 'feedback-store',
        'update' => 'feedback-update',
        'destroy' => 'feedback-delete'
        ]
    ]);
});






Route::post('feedback/update-notes', 'FeedbackController@updateNotes')->name('feedback-update-notes');
Route::post('feedback/mark-as-read', 'FeedbackController@markAsRead')->name('feedback-mark-as-read');

//EBAY MANAGEMENT AND STAFF
Route::group(['middleware' => ['role:owner|ebaymanagement|ebay_staff']], function() {



    Route::post('p2g-quote', 'HomeController@p2gQuote')->name('p2g-quote');

    //Orders
    Route::get('orders', 'OrderController@index');
    Route::get('order/{id}', 'OrderController@singleOrder')->name('singleOrder');
    Route::get('orders', 'OrderController@index')->name('orders');
    Route::get('order/{id}', 'OrderController@singleOrder')->name('single-order');
    Route::post('orders/print-invoices', 'OrderController@printInvoices')->name('print-invoices');
    Route::post('orders/print-labels', 'OrderController@printLabels')->name('print-labels');

    //Listings
    Route::get('listings', 'ListingController@index')->name('listings');
    Route::get('listings/filter/{id}', 'ListingController@index')->name('listings-filter');
    Route::get('listing/create', 'ListingController@createListing')->name('create-listing');
    Route::get('list/{id}', 'ListingController@createListing')->name('list');
    Route::post('list/doCreateListing', 'ListingController@doCreateListing');

    //Folders
    Route::post('folders/move', 'ItemFolderController@moveItemToFolder')->name('move-item-to-folder');
    Route::post('folders/remove', 'ItemFolderController@removeItemFromFolder')->name('remove-item-from-folder');

    Route::resource('folders', 'ItemFolderController', [
    'names' => [
        'index' => 'item-folders',
        'show' => 'item-folder-show',
        'edit' => 'item-folder-edit',
        'create' => 'item-folder-create',
        'store' => 'item-folder-store',
        'update' => 'item-folder-update',
        'destroy' => 'item-folder-delete'
        ]
    ]);




    Route::get('photos/create/{id}', [
      'name' => 'photos-create',
      'uses' => 'ItemPhotosController@create'
    ]);

    Route::resource('photos', 'ItemPhotosController', [
    ['except' => ['create']],
    'names' => [
        'index' => 'photos',
        'show' => 'photo-show',
        'store' => 'photo-store',
        'edit' => 'phots-edit',
        'update' => 'photo-update',
        'destroy' => 'photo-delete'
        ]
    ]);

    // Other pages
    Route::get('invoice', 'HomeController@invoice');
    Route::get('order-sheet', 'OrderController@orderSheet');

    //Refresh API Tokens (still needed?)
    Route::get('refresh', 'GuideController@refreshTokens');

    //Inventory
    Route::get('inventory', 'InventoryController@all')->name('inventory');
    Route::get('inventory/add', 'ItemController@addItem');
    Route::get('inventory/add/crate', 'ItemController@addCrate');
    Route::get('inventory/filter', 'InventoryController@filter')->name('inventory-filter');
    Route::get('inventory/inventoryByArea/', 'InventoryController@inventoryByArea'); //MOVE TO  FILTER ROUTE
    Route::get('inventory/inventoryByArea/{area}', 'InventoryController@inventoryByArea'); //MOVE TO FILTER ROUTE
    Route::post('inventory/search', 'InventoryController@search')->name('inventory-search');

    //Item
    Route::get('item/{id}/{next?}', 'InventoryController@item')->name('item');
    Route::post('item/search/id', 'InventoryController@searchItemId')->name('search-item-id');
    Route::post('items/getUser/{id}', 'ItemController@getUser');
    //Warehouse
    Route::get('warehouse', 'WarehouseController@index')->name('warehouse');
    Route::post('warehouse/check-in', 'WarehouseController@doCheckIn')->name('warehouse-check-in');
    Route::get('warehouse/email-report', 'WarehouseController@sendEmailReport')->name('email-warehouse-report');

    //Pick List
    Route::get('picklist', 'PickController@index')->name('picklist');
    Route::get('picklist/{area}', 'PickController@index');
    Route::post('picklist/{area}/sortBy', 'PickController@index');
    Route::post('picklist/assign', 'PickController@assign');

    //Tools
    Route::get('bulk-estimate-value', 'GuideController@bulkEstValue')->name('findValue');
    Route::post('doBulkEstValue', 'GuideController@doBulkEstValue');
});


//BRC Staff
Route::group(['middleware' => ['role:owner|ebaymanagement|ebay_staff|ebay_volunteer|arm|shop']], function() {

    //Guides
    Route::get('guides', 'GuideController@index')->name('guides');
    Route::get('guides/size', 'GuideController@size')->name('sizeGuide');
    Route::get('guides/custominfo', 'GuideController@customInfo')->name('customInfo');
    Route::get('guides/categories', 'GuideController@categories')->name('categories');
    Route::get('guides/selecting', 'GuideController@selectingItems')->name('selectingItems');
    Route::get('guides/researching', 'GuideController@researchingItems')->name('researchingItems');
    Route::get('guides/clothing-brands', 'GuideController@clothingBrands')->name('clothingBrands');
    Route::get('guides/packing', 'GuideController@packingItems')->name('packingItems');
    Route::get('guides/warehouse-workflow', 'GuideController@warehouseWorkflow')->name('warehouseWorkflow');

    //The most important page
    Route::get('bears', 'BearController@index')->name('bears');

    //User account
    Route::get('account', 'AccountController@dashboard')->name('account');
    Route::get('account/manage', 'AccountController@manageAccount')->name('manage-account');

    //Shops
    Route::get('shops', 'ShopController@index')->name('shops');
    Route::get('shops/{id}', 'ShopController@singleShop')->name('single-shop');
    Route::get('shops/area/{id}', 'ShopController@singleArea');
    Route::post('shops/doFindShop', 'ShopController@doFindShop')->name('doFindShop');
});


//Reports
Route::group(['middleware' => ['role:owner|ebay_staff|arm|shop|ebaymanagement']], function() {
    Route::get('reports', 'ReportController@dashboard')->name('reports');
});

//Ebay Reports
    Route::group(['middleware' => ['role:owner|ebay_staff']], function() {
        Route::get('reports/team', 'ReportController@team')->name('teamReport');
        //Route::get('reports/feedback', 'ReportController@feedback')->name('feedback');
    });

    Route::group(['middleware' => ['role:shop']], function() {
        Route::post('report/generate/shop', 'ReportController@generateShopReport')->name('generate-shop-report');
    });

//Logsheet generation
Route::group(['middleware' => ['role:shop|ebaymanagement|ebay_staff|ebay_volunteer']], function() {
    Route::get('logsheet/generate', 'LogsheetController@generate')->name('create-logsheet');
    Route::post('logsheet/doGenerateLogsheet', 'LogsheetController@doGenerateLogsheet');
});

//Logsheet management
Route::group(['middleware' => ['role:ebaymanagement|ebay_staff|ebay_volunteer']], function() {
    Route::get('logbook', 'LogsheetController@index')->name('logbook');
    Route::get('logsheet/generateBinCard/{id}', 'LogsheetController@generateBinCard');
    Route::get('logsheet/generateReturnCard/{id}', 'LogsheetController@generateReturnCard');
    Route::get('logsheet/{id}', 'LogsheetController@singleLogsheet');
    Route::post('logsheet/logsheetToInventory', 'LogsheetController@logsheetToInventory');
    Route::get('logbook/{filter}', 'LogsheetController@index')->name('logbookFilter');
    Route::post('logbook/{filter}', 'LogsheetController@index')->name('logbookFilter');
    Route::get('logsheet/pdf/{id}', 'LogsheetController@pdfBinCard')->name('pdfBinCard');
});


Route::group(['middleware' => ['role:owner|ebaymanagement']], function() {
    Route::get('admin', 'AdminController@index')->name('admin');
    Route::get('admin/inventory-status', 'InventoryStatusController@index');
    Route::get('admin/inventory/status-settings', 'AdminController@inventoryStatusSettings');
    Route::post('admin/inventory/status-settings/doAddStatus', 'AdminController@inventoryDoAddStatus');

    Route::get('admin/shops/add', 'AdminShopController@addShop')->name('admin-add-shop');
    Route::post('admin/shops/doAdd', 'AdminShopController@doAddShop')->name('admin-shops-doAdd');
    Route::get('admin/shops/edit', 'AdminShopController@editShops')->name('admin-edit-shops');
    Route::post('admin/shops/doEdit', 'AdminShopController@doEditShop')->name('admin-shops-doEdit');

    Route::get('admin/warehouse/add', 'AdminWarehouseController@addWarehouse')->name('admin-add-warehouse');
    Route::post('admin/warehouse/doAdd', 'AdminWarehouseController@doAddWarehouse')->name('admin-warehouse-doAdd');
    Route::get('admin/warehouse/edit', 'AdminWarehouseController@editWarehouse')->name('admin-edit-warehouse');
    Route::post('admin/warehouse/doEdit', 'AdminWarehouseController@doEditWarehouse')->name('admin-warehouse-doEdit');
    Route::post('admin/warehouse/doDelete', 'AdminWarehouseController@doDeleteWarehouse')->name('admin-warehouse-doDelete'); //Replace with delete method to prevent CSRF

});

Route::group(['middleware' => ['role:owner']], function() {
    Route::resource('admin/users', 'UserController', [
        'names' => [
            'index' => 'admin-users',
            'show' => 'admin-user-show',
            'edit' => 'admin-user-edit',
            'create' => 'admin-user-create',
            'store' => 'admin-user-store',
            'update' => 'admin-user-update',
            //'destroy' => 'admin-user-delete'
        ]
    ]);

    Route::resource('admin/roles', 'RoleController', [
        'names' => [
            'index' => 'admin-roles',
            'show' => 'admin-role-show',
            'edit' => 'admin-role-edit',
            'create' => 'admin-role-create',
            'store' => 'admin-role-store',
            'update' => 'admin-role-update',
            //'destroy' => 'admin-user-delete'
        ]
    ]);

    Route::resource('admin/user/roles', 'UserRoleController', [
        'names' => [
            'edit' => 'admin-user-role-edit',
            //'create' => 'admin-user-role-create',
            'store' => 'admin-user-role-store',
            'update' => 'admin-user-role-update',
            //'destroy' => 'admin-user-role-delete'
        ]
    ]);

    Route::get('admin/user/roles/create/{user}', 'UserRoleController@create')->name('admin-user-role-create');
    Route::get('admin/user/roles/delete/{user}/{role}', 'UserRoleController@destroy')->name('admin-user-role-delete');
});


Route::resource('admin/packing', 'AdminPackingController', [
    'names' => [
        'index' => 'admin-packing',
        'show' => 'admin-packing-show',
        'edit' => 'admin-packing-edit',
        'create' => 'admin-packing-create',
        'store' => 'admin-packing-store',
        'update' => 'admin-packing-update',
        'destroy' => 'admin-packing-delete'
    ]
]);

Route::resource('admin/shipping', 'AdminShippingTypeController', [
    'names' => [
        'index' => 'admin-shipping-types',
        'show' => 'admin-shipping-type-show',
        'edit' => 'admin-shipping-type-edit',
        'create' => 'admin-shipping-type-create',
        'store' => 'admin-shipping-type-store',
        'update' => 'admin-shipping-type-update',
        'destroy' => 'admin-shipping-type-delete'
    ]
]);

Route::get('admin/api', 'AdminController@api')->name('key');
Route::post('admin/api/setKey', 'AdminController@setKey')->name('setKey');

Route::get('rts', 'RTSController@index')->name('rts');
Route::get('rts/generate-rts-label', 'RTSController@generateRTSLabel')->name('generate-rts-label');
Route::post('rts/doGenerateRTSLabel', 'RTSController@doGenerateRTSLabel')->name('do-generate-rts-label');

Route::get('logout', function(){
	Auth::logout();
	return redirect('/');
});

});
