<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('listing/getCategory', 'api\ListingController@getCategory');
Route::post('listing/getCustomInfo', 'api\ListingController@getCustomInfo');

Route::get('items/shop/{code}', 'api\ItemController@shop');
Route::get('items/area/{area}', 'api\ItemController@area');
Route::post('items/save/{id}', 'api\ItemController@save');
Route::post('items/logItemAction/{id}', 'api\ItemController@logItemAction');

Route::resources([
    'items' => 'api\ItemController',
]);
