<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemFolder extends Model
{
    protected $table = 'item_folders';
    protected $primaryId = 'id';
}
