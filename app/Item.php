<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Model used to manage items
 *
 * @category Model
 */

class Item extends Model
{
	/**
	 * Define table to use
	 * @var string
	 */
	protected $table = 'items';	

	/**
	 * Config timestamp fields
	 * @var boolean
	 */
	public $timestamps = false;	
}
