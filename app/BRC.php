<?php

namespace App;
use App\API;
use Illuminate\Support\Facades\Auth;

class BRC{
	public static function unique_multidim_array($array, $key) {
			$temp_array = array();
			$i = 0;
			$key_array = array();

			foreach($array as $val):
					if (!in_array($val[$key], $key_array)) {
							$key_array[$i] = $val[$key];
							$temp_array[$i] = $val;
					}
					$i++;
			endforeach;
			return $temp_array;
	}

	public static function logItemAction($item, $action)
	{
		// TODO - ADD VALIDATION
        $record = new ItemLog;
        $record->item_id = $item;
        $record->user_id = Auth::user()->id;
        $record->action = $action;
        $record->save();
	}

	static function getAuth(){
		return API::select('key')->orderBy('id', 'DESC')->limit(1)->firstOrFail()->key;
	}

	/**
	 * Fetch array of short month names for each month of the year
	 * @category Helper
	 * @return array
	 */
	public static function getMonths()
	{
		$months = array();
		for($m=1; $m<=12; ++$m):
    		$months[] = (object) array(
    			'number' => date('m', mktime(0, 0, 0, $m, 1)),
    			'name' => date('M', mktime(0, 0, 0, $m, 1))
    		);
		endfor;
		return $months;
	}

	/**
	 * Set conditional class for filtered month
	 * @category Helper
	 * @param  string $filterMonth		Filter month
	 * @param  string $filterYear		Filter year
	 * @param  string $selectedMonth	Current date month
	 * @param  string $selectedYear		Current date year
	 * @return string					HTML class tag set as currentMonth
	 */
	public static function selectedMonth($filterMonth, $filterYear, $selectedMonth, $selectedYear){
		if($filterMonth === $selectedMonth && $filterYear === $selectedYear):
			return 'class="selectedMonth"';
		endif;
	}

	/**
	 * Get a date range for the current month (YYYY-MM-DD)
	 * @param  string $month Month
	 * @param  string $year  Year
	 * @return array Lower & upper limits of date range
	 */
	public static function getDateRange($month, $year){

        if($month !== NULL && $year !== NULL):
            $nod = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $result = array(
                'lower' => $year.'-'.$month.'-01',
                'upper' => $year.'-'.$month.'-'.$nod,
            );
        else:
            $result = array(
                'lower' => '2016-01-01',
                'upper' => date('y-m-d', time()),
            );
        endif;

        return $result;
    }

    public static function getDateRangeString($month, $year)
    {
    	$range = Self::getDateRange($month, $year);

    	function format($date){
    		return date('d/m/y', strtotime($date));
    	}
    	return format($range['lower']).' &ndash; '.format($range['upper']);
    }

    public static function listYears($start, $end = NULL)
	{
		if($end === NULL):
		  $end = date('Y', time());
		endif;


		$listYears = array();

		while($start <= $end):
		  array_push($listYears, $start);
		  $start++;
		endwhile;

		return $listYears;
	}

}
