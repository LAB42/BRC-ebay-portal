<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseStaff extends Model
{
    public $table = 'warehouse_staff';
}
