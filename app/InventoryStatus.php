<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/**
 * Model used to manage inventory statuses
 *
 * @category  Model
 */

class InventoryStatus extends Model
{
	/**
	 * Define table to use
	 * @var string
	 */
	protected $table = 'inventory_statuses';

	/**
	 * Define the primary key used for the $table
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Define the relationship to the inventory table
	 * @return void
	 */
	public function inventory()
    {
        return $this->belongsTo('App\Inventory', 'status', 'status');
    }
	
}
