<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class API extends Model
{
    protected $table = 'api';
    protected $primaryId = 'id';
}
