<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $primaryId = 'id';

    public function orderStatus(){
		return $this->hasOne('App\OrderStatus', 'id', 'status');
	}

}
