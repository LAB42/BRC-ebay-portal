<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
	use Searchable;
	
	protected $table = 'areas';
}
