<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
	use Searchable;

    protected $table = 'inventory';
    protected $primaryKey = 'id';


		public function photos(){
		return $this->hasMany('App\ItemPhoto', 'item_id');
	}

    public function inventoryStatus(){
		return $this->hasOne('App\InventoryStatus', 'id', 'status');
	}

	public function shop(){
    	return $this->hasOne('App\Shop', 'id', 'shopId');
    }

    public function toSearchableArray()
    {
    	  $post = $this->toArray();

		  $post['shopName'] = $this->shop->name;
		  $post['shopCode'] = $this->shop->code;
		  $post['shopArea'] = $this->shop->area->name;
		  $post['status'] = $this->inventoryStatus->name;

		  return $post;
    }


    public function searchableAs()
    {
    	return 'inventory_index';
    }
 }
