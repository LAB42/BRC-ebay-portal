<?php

namespace App\Http\Controllers\api;


use App\Inventory;
use App\InventoryStatuses;
use App\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ItemLog;

use Auth;


class ItemController extends Controller
{

    public function __construct()
    {

    }


    public function logItemAction(Request $request)
    {
        switch($request->get('action')):
            case 'name':
                $message = 'Updated item name';
            break;

            case 'location':
                $message = 'Updated item location';
            break;

            case 'gift_aid':
                $message = 'Updated gift aid number';
            break;

            case 'est_value':
                $message = 'Updated estimated value';
            break;

            case 'price':
                $message = 'Updated price';
            break;

            case 'category':
                $message = 'Updated category';
            break;

            case 'description':
                $message = 'Updated description';
            break;

            case 'weight_int':
                $message = 'Updated weight number';
            break;

            case 'weight_unit':
                $message = 'Updated weight unit';
            break;

            case 'weight_text':
                $message = 'Updated weight text';
            break;

            case 'size':
                $message = 'Updated size information';
            break;

            case 'condition':
                $message = 'Updated condition';
            break;

            default:
                $message = 'Unknown action taken';
        endswitch;

        $record = new ItemLog;
        $record->item_id = $request->get('item');
        $record->user_id = $request->get('user');
        $record->action = $message;
        $record->save();

        return response()->json($request->all());
    }


    public function single($id)
    {
      if(Auth::check()):
        return Inventory::where('id', '=', $id)->with('inventoryStatus')->with('shop')->firstOrFail();
      else:
        return false;
      endif;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inventory::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    public function save(Request $request)
    {
        $item = Inventory::find($request->get('id'));

        if($request->has('item_name')):
            $item->item_name = $request->get('item_name');
        endif;

        if($request->has('desp')):
            $item->comments = $request->get('desp');
        endif;

        if($request->has('est_value')):
            $item->est_value = $request->get('est_value');
        endif;

        if($request->has('price')):
            $item->price = $request->get('price');
        endif;

        if($request->has('gift_aid')):
            $item->gift_aid = $request->get('gift_aid');
        endif;

        if($request->has('shop_name')):
            $item->shop_name = $request->get('shop_name');
        endif;

        if($request->has('shop_code')):
            $item->shop_code = $request->get('shop_code');
        endif;

        if($request->has('category')):
            $item->category = $request->get('category');
        endif;

        if($request->has('location')):
            $item->location = $request->get('location');
        endif;

        if($request->has('weight_int')){
            $item->weight_int = $request->get('weight_int');
        }

        if($request->has('weight_unit')){
            $item->weight_unit = $request->get('weight_unit');
        }

        if($request->has('weight_text')){
            $item->weight_text = $request->get('weight_text');
        }

        if($request->has('size_text')){
            $item->size_text = $request->get('size_text');
        }

        if($request->has('condition')){
            $item->condition = $request->get('condition');
        }

        $item->save();
        return response()->json($request->all());

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return Inventory::where('id', '=', $id)->with('inventoryStatus')->with('shop')->firstOrFail();    
    }

    public function shop($code)
    {
        return Inventory::where('shop_code', '=', $code)->orderBy('id', 'ASC')->get();
    }

    public function area($area)
    {
        return Inventory::where('area', '=', $area)->orderBy('id', 'ASC')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
