<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\BRC;

use DTS\eBaySDK\Credentials\CredentialsProvider;
use DTS\eBaySDK\Finding\Services\FindingService;
use DTS\eBaySDK\Sdk;
use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;

class ListingController extends Controller
{
    public function getCategory(Request $reactRequest)
    {
		$profile = 'production';
        $path = app_path('credentials.ini');

        $provider = CredentialsProvider::ini($profile, $path);

        $service = new Services\TradingService([
            'credentials' => $provider,
            'apiVersion' => '1065',
            'globalId'   => 'EBAY-GB',
            'siteId' => 3,
            'authorization' => BRC::getAuth()
        ]);

        $request = new Types\GetSuggestedCategoriesRequestType();
        
        $query = $reactRequest->input('query');
        if($query == NULL):
        	$request->Query = 'root';
        else:
        	$request->Query = $reactRequest->input('query');
        endif;

        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = BRC::getAuth();
        
        $response = $service->GetSuggestedCategories($request);

        $results = array();
        
        
        foreach($response->SuggestedCategoryArray->SuggestedCategory as $category):
            $results[] = array(
            	
                'id' => $category->Category->CategoryID,
                'name' => $category->Category->CategoryName
            );
            
        endforeach;

        return $results;

    }

    public function getCustomInfo(Request $reactRequest)
    {
		$profile = 'production';
        $path = app_path('credentials.ini');
      
        $provider = CredentialsProvider::ini($profile, $path);

        $service = new Services\TradingService([
            'credentials' => $provider,
            'apiVersion' => '1065',
            'globalId'   => 'EBAY-GB',
            'siteId' => 3,
            'authorization' => BRC::getAuth()
        ]);

        $request = new Types\GetCategorySpecificsRequestType();

        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = BRC::getAuth();

        
        // $request->CategoryID = array($reactRequest->get('query'));
        $request->CategoryID = array($reactRequest->get('query'));
        $response = $service->getCategorySpecifics($request);

        
        

        $results = array();
		foreach($response->Recommendations as $rec):
            
			foreach($rec->NameRecommendation as $nr):                
                $results[$nr->Name] = array('name' => $nr->Name, 'values' => array());
				foreach($nr->ValueRecommendation as $vr):                               
                    array_push($results[$nr->Name]['values'], array('name' => $nr->Name, 'value' => $vr->Value));
                endforeach;                
                
			endforeach;		  
		endforeach;

       
        return response()->json($results);


        // $newResults = array();
        // foreach($results as $result):


        //  $newResults[] = array(
                
        //         'name' => $result['name'],
        //         'value' => $result['value']
        //     );

        // endforeach;

        // function _group_by($array, $key) {
        //     $return = array();
        //     foreach($array as $val) {
        //         $return[$val[$key]][] = $val['value'];
        //     }
        //     return $return;
        // }
        
        // $newResults = _group_by($results, 'name');

        // return $newResults;	
        

        

    }
}
