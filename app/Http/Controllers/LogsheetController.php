<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\BRC;
use App\Logsheet;
use App\Shop;
use App\InventoryStatus;
use App\Inventory;

class LogsheetController extends BaseController
{
    public function index(Request $request, $filter = NULL){
		$data['page']['title'] = 'Logbook';

        $shopCode = ($request->has('shopCode')) ? $request->get('shopCode') : NULL;
        $shopId = ($shopCode !== NULL) ? Shop::where('code', '=', $shopCode)->with('area')->firstOrFail()->id : "";

        $crateCode = ($request->has('crateCode')) ? $request->get('crateCode') : NULL;
        
        switch ($filter):
            case 'received':
                $data['entries'] = Logsheet::where('received_by', '!=', NULL)->with('shop')->get();
                break;
            case 'not-received':
                $data['entries'] = Logsheet::where('received_by', '=', NULL)->with('shop')->get();
                break;
            case 'search':
                $data['entries'] = Logsheet::where('shop_id', '=', $shopId)->with('shop')->get();
                break;
            case 'crateCode':
                if(preg_match('^([0-9]{1,})^', $crateCode)):
                    $data['entries'] = Logsheet::where('id', '=', $crateCode)->with('shop')->get();
                else:
                    $data['entries'] = NULL;
                endif;
                break;
            default:
                $data['entries'] = Logsheet::orderBy('due_date', 'ASC')->with('shop')->get();
                break;
        endswitch;
// dd($data['entries'])
		return view('logbook', $data);
	}

	public function singleLogsheet($id){		
		$data['logsheet'] = Logsheet::with('shop')->where('id', '=', $id)->firstOrFail();
		$data['items'] = json_decode($data['logsheet']->items);
        $data['statuses'] = InventoryStatus::all();
		$data['page']['title'] = 'Logsheet #'.$data['logsheet']->id;
        
		return view('single_logsheet', $data);
	}

    public function generate(){
    	$data['page']['title'] = 'Add Logsheet';

    	return view('generate_logsheet', $data);
    }

    public function doGenerateLogsheet(Request $request){        
        $shop = array(            
            'code' => $request->get('shopCode'),                                   
            'deliveryDate' => $request->get('deliveryDate'),
        );

        $items = array();

        for($i=1; $i <= $request->get('numberOfItems'); $i++) { 
            if($request->get('name'.$i) !== null):
                $items[] = array(
                    'id' => $i,
                    'name' => $request->get('name'.$i),
                    'info' => $request->get('info'.$i),
                    'gift_aid' => $request->get('giftAid'.$i),
                    'value' => $request->get('value'.$i)
                );
            endif;
        }

        $shop = Shop::select('id')->where('code', '=', $request->get('shopCode'))->firstOrFail();

        $validatedData = $request->validate([
            'shopCode' => 'required|integer|digits_between:5,10',
            'deliveryDate' => 'required|date'
        ]);

        $logsheet = new Logsheet;

        $logsheet->shop_id = $shop->id;
        $logsheet->items = json_encode($items);        
        $logsheet->due_date = $request->get('deliveryDate');
        $logsheet->received_date = $request->get('receivedDate');
        $logsheet->received_by = $request->get('receivedDate');        
        $logsheet->save();

        return redirect('logbook')->with('message', 'Logbook saved - Crate ID #'.$logsheet->shop_id.'/'.$logsheet->id.'/'.count(json_decode($logsheet->items)));
    }

    public function generateBinCard($id)
    {
    	$data['logsheet'] = Logsheet::find($id);        
    	return view('bin_card', $data);
    }

    public function generateReturnCard($id)
    {
        $data['logsheet'] = Logsheet::find($id);

        $data['returnItems'] = array();
        foreach(json_decode($data['logsheet']->items) as $item):
            $status = InventoryStatus::find($item->status);
            if(preg_match('$RTS*$', $status)):
                $data['returnItems'][] = $item;
            endif;
        endforeach;
        
        return view('return_card', $data);
    }

    public function logsheetToInventory(Request $request)
    {
        $shop = array(
            'code' => $request->get('shopCode'),
            'area' => $request->get('shopArea'),
            'name' => $request->get('shopName')
        );

        $shopId = Shop::where('code', '=', $shop['code'])->firstOrFail()->id;

        $items = array();

    
        //update logsheet with receiver by name
        for ($i=1; $i <= $request->get('numberOfItems') ; $i++):
            $items[] = array(
                'shop_id' => $shopId,
                'name' => $request->get('name'.$i),
                'location' => $request->get('location'.$i),
                'crate_item_number' => $request->get('crate_item_number'.$i),
                'gift_aid' => $request->get('gift_aid'.$i),
                'est_value' => $request->get('est_value'.$i),                
                'comments' => $request->get('comments'.$i),
                'date_received' => $request->get('receivedDate'),
                'status' => $request->get('status'.$i),
                'id' => $request->get('crate_item_number'.$i),
                'value' => $request->get('est_value'.$i),
                'info' => $request->get('comments'.$i),
            );
        endfor;

        $serialFragments = array(
            'logsheetId' => $request->get('logsheetId'),
            'shopCode' => $shop['code'],            
        );
        $serial = md5(serialize($serialFragments));

        if(Logsheet::where('serial', '=', $serial)->count() > 0):
            return redirect()->route('logbook')->with('message', 'Logsheet already added to inventory');
        else:
            foreach($items as $item):                        
                $entry = new Inventory;            
                $entry->shopId = $item['shop_id'];
                $entry->item_name = $item['name'];
                $entry->location = $item['location'];
                $entry->crate_item_number = $item['crate_item_number'];
                $entry->gift_aid = $item['gift_aid'];
                $entry->est_value = $item['est_value'];
                $entry->comments = $item['comments'];
                $entry->date_received = $item['date_received'];
                $entry->status = $item['status'];            
                $entry->save();
                BRC::logItemAction($entry->id, 'Goods-in');
            endforeach;

            $updateLogsheet = Logsheet::find($request->get('logsheetId'));
            $updateLogsheet->items = json_encode($items);
            $updateLogsheet->received_by = $request->get('receivedBy');
            $updateLogsheet->received_date = $request->get('receivedDate');
            $updateLogsheet->serial = $serial;
            $updateLogsheet->save();


            return redirect('/logsheet/'.$request->get('logsheetId'))->with('message', 'Items moved to inventory');
        endif;
    }

    public function pdfBinCard($id)
    {

       
    }
}
