<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\Area;

class AdminShopController extends Controller
{
    /**
     * Controller for admin control of shop data
     *
     * @category Controller
     */
    
    public function addShop()
    {
    	$data['page']['title'] = 'Admin - Add Shop';
        $data['areas'] = Area::all();
    	return view('admin/add_shop', $data);
    }

    public function editShops()
    {

    	$data['page']['title'] = 'Admin - Edit Shops';

        $data['shops'] = Shop::orderBy('id', 'ASC')->with('area')->get();



        // $colors = array(
        //     'South 1' => '#1a3351',
        //     'South 2' => '#7d1c23',
        //     'South 3' => '#43a92c',
        //     'South 4' => '#baddea',
        //     'South 5' => '#5a98c0',
        //     'South 6' => '#e4d7aa',
        //     'South 7' => '#afa48f',
        //     'South 8' => '#627b80',
        //     'South 9' => '#f1b13b',
        //     'North 10' => '#1a3351',
        //     'North 11' => '#7d1c23',
        //     'North 12' => '#43a92c',
        //     'North 13' => '#baddea',
        //     'North 14' => '#5a98c0',
        //     'North 15' => '#e4d7aa',
        //     'North 16' => '#afa48f',
        //     'North 17' => '#627b80',
        //     'North 18' => '#f1b13b',
        // );

        $data['areas'] = Area::all();
        // foreach($data['areas'] as $area):
        //     $a = Area::where('name', '=', $area->name)->firstOrFail();
        //     $a->color = $colors[$area->name];
        //     $a->save();
        // endforeach;

    	return view('admin/edit_shops', $data);
    }

    public function doEditShop(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'area' => 'required',
            // 'sm' => 'required',
            // 'asm' => 'required'
        ]);

        if($request->has('id')):
            

            $table = Shop::where('id', '=', $request->get('id'))->with('area')->firstOrFail();            
            $table->name = $request->get('name');
            $table->areaId = $request->get('area');
            $table->manager = $request->get('sm');
            $table->assistant_manager = $request->get('asm');
            $table->save();


            return redirect()->route('admin-edit-shops')->with('message', 'Shop updated');
        else:
             return abort(404);
        endif;
    }

      public function doAddShop(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'name' => 'required',
            'areaId' => 'required',
            // 'sm' => 'required',
            // 'asm' => 'required'
        ]);

        $table = new Shop();
        $table->code = $request->get('code');
        $table->name = $request->get('name');
        $table->areaId = $request->get('areaId');
        $table->manager = $request->get('manager');
        $table->assistant_manager = $request->get('assistant_manager');
        $table->save();

        return redirect()->route('admin-edit-shops')->with('message', 'Shop added');       
    }

}
