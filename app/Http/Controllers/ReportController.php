<?php

namespace App\Http\Controllers;
use DTS\eBaySDK\Credentials\CredentialsProvider;
use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;


use App;
use Illuminate\Http\Request;
use App\Inventory;
use App\InventoryStatus;
use App\Shop;
use App\Area;

class ReportController extends Controller
{


    public function dashboard()
    {
        $data['page']['title'] = 'Reports Dashboard';

        $data['statuses'] = InventoryStatus::all();

        //BAD BAD CODE
        $listedStatus = InventoryStatus::where('name', '=', 'Listed')->firstOrFail()->id;
        $rtsStatus = InventoryStatus::select('id')->where('name', 'ILIKE', '%RTS%')->get();
        $pendingStatus =  InventoryStatus::where('name', '=', 'Pending')->firstOrFail()->id;

        $data['current_stock'] = array(
            'listed' => Inventory::where('status', '=', $listedStatus)->count(),
            'nis' => Inventory::whereIn('status', $rtsStatus)->count(),
            'pending' => Inventory::where('status', '=', $pendingStatus)->count()
        );

        $data['areas'] = Area::all();

        return view('report_dashboard', $data);
    }


    public function generateShopReport(Request $request){
        //Add validation
        
        $code = $request->input('code');
        $status = $request->input('status');

        //Get shop data
        $shop = Shop::where('code', '=', $code)->firstOrFail();     
        $type = InventoryStatus::where('id', '=', $status)->firstOrFail();
        $items = Inventory::where('status', '=', $type->id)->where('shopId', '=', $shop->id)->get();

        $pdf = App::make('dompdf.wrapper');      
    
            $data = '
                        <style>                        
                        .page-break {
                            page-break-after: always;
                        }
                        </style>
                        
                        <img src="'.asset('img/brc_logo.png').'" alt="" style="width: 240px; margin-bottom: 15px;">
                        <h1>Ebay Report for '.$shop->name.'</h1>
                        <p>Manager: '.$shop->manager.'</p>
                        <h2>'.$type->name.'</h2>
                        <h5>Generated: '.date('H:m d/m/Y', time()).'</h5>                        
                        <table style="width: 100%; text-align: center; padding: 10px;">
                            <thead style="background: #eee;">
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                    ';

            foreach($items as $item):
                $data .= '<tr>
                                    <td>'.$item->id.'</td>
                                    <td>'.$item->item_name.'</td>
                                </tr>';
            endforeach;

            $data .= '</tbody>
                        </table>';



            $pdf->setPaper('a4', 'portrait');
            $pdf->loadHTML($data);
            return $pdf->stream();
    }

    public function team()
    {
        $data['page']['title'] = 'Team Ebay';
        return view('team_report', $data);
    }

    public function feedback(){
 
        $profile = 'production';
        $path = app_path('credentials.ini');
        
        $config = array(    
                'oAuth' => 'AgAAAA**AQAAAA**aAAAAA**odU/Ww**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6ADlYWgDZiHoA6dj6x9nY+seQ**LE8EAA**AAMAAA**T3WMliKRnl1UfiwpzI3YY8n1aQZxrcnrYcXnQ+jjXS2FHS4OVa1qoj31b1B48mtKKlZ7WWcGfLo5JBySTSsJh+bwdm9rU8NLOx48C22t7MQSrYJSlTje1oxfpICMWiHK0wXacTREgJlzWD+5zX9ZIY4+v0wtOOFHhGn7L4h/SvPxEYj6fn6WShQTAqobpQyZeAmWAZ0g3l8qroogU7t1Zm1dZ4FhApZI8iDqrxCnJq3y0DW9NuCz/hiyvwkuqrbr/8mLOrRDg//d0z5xkFU/RpPXlSJ4gajw6SmoMMoN6wvrbIXH8vra2clRyIGEcoR0FqHg5nynP4Telb5cboJPrc4cvtGNuFWzaiUOZPZP4FV6wJ8BCCromPtxLsXctrwO3K8F1jA5WXSEddjjVMXj3hTEbZgVM6FsP0YuydQjdqLff+jntFx3HSNYWnIcLvSvfRU7W67dlcdplj3D8EwlhDAuUpRLgyN30Q01cDtYyLnbURRwAWNyoYfiUDneUBM7h3+Qoz6diDCTLiqljCLmCjOhPKlNn40KEZxhW1s7t3IJM36WtZr6pVwoBV6j1gm+AUORoXBKokXeYuEIltBvQuZ55gC/eiqh6M3mR5oKciX3gX3YR4T0xx8JgoOOfZqJu02b5w/TMdc0OiuAeWSR7Ixhr8BV9geJf0PFEogdLFA4J2w4+o754HNS5EAl1lQ6opt84N3br8j3xi0UsTqbrBd6GnPfnfy8oPku2pDSe2s3k2yFy0cNO9ELFt7sQpgQ'
            );


        $provider = CredentialsProvider::ini($profile, $path);

        $service = new Services\TradingService([
            'credentials' => $provider,
            'apiVersion' => '1065',
            'globalId'   => 'EBAY-GB',
            'siteId' => 3
        ]);


        $request = new Types\GetFeedbackRequestType();


        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $config['oAuth'];
        $request->DetailLevel = ['ReturnAll'];
        
        $response = $service->getFeedback($request);

       
        
        if (isset($response->Errors)):
            // foreach ($response->Errors as $error):
            //     printf(
            //         "%s: %s\n%s\n\n",
            //         $error->SeverityCode === Enums\SeverityCodeType::C_ERROR ? 'Error' : 'Warning',
            //         $error->ShortMessage,
            //         $error->LongMessage
            //     );
            // endforeach;
        endif;
        if ($response->Ack !== 'Failure'):
            $data['feedback'] = $response->FeedbackDetailArray->FeedbackDetail;

            $data['page']['title'] = 'Feedback';

            echo 'CREATE NEW FEEDBACK BLADE';
            //return view ('feedback', $data);
                 
            
        endif;
    }
}
