<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warehouse;

class AdminWarehouseController extends Controller
{
    /**
     * Controller for admin control of shop data
     *
     * @category Controller
     */
    
    public function addWarehouse()
    {
    	$data['page']['title'] = 'Admin - Add Warehouse Section/Process';
        
    	return view('admin/add_warehouse', $data);
    }

    public function editWarehouse()
    {

    	$data['page']['title'] = 'Admin - Edit Warehouse Section/Process';

        $data['warehouse'] = Warehouse::orderBy('id', 'ASC')->get();
        
        $data['types'] = array('storage', 'process');

    	return view('admin/edit_warehouse', $data);
    }

    public function doEditWarehouse(Request $request)
    {
        $request->validate([
            'section' => 'required',
            'type' => 'required',
            'low' => 'required',
            'medium' => 'required',
            'high' => 'required',
            'current' => 'required'
        ]);

        if($request->has('id')):
            $table = Warehouse::where('id', '=', $request->get('id'))->firstOrFail();            
            $table->section = $request->get('section');
	        $table->type = $request->get('type');
	        $table->low = $request->get('low');
	        $table->medium = $request->get('medium');
	        $table->high = $request->get('high');
	        $table->current = $request->get('current');
            $table->save();


            return redirect()->route('admin-edit-warehouse')->with('message', 'Section/Process updated');
        else:
             return abort(404);
        endif;
    }

    public function doAddWarehouse(Request $request)
    {
        $request->validate([
            'section' => 'required',
            'type' => 'required',
            'low' => 'required',
            'medium' => 'required',
            'high' => 'required',
            'current' => 'required'
        ]);

        $table = new Warehouse();
        $table->section = $request->get('section');
        $table->type = $request->get('type');
        $table->low = $request->get('low');
        $table->medium = $request->get('medium');
        $table->high = $request->get('high');
        $table->current = $request->get('current');
        $table->save();

        return redirect()->route('admin-edit-warehouse')->with('message', 'Section/process added');
    }

    public function doDeleteWarehouse(Request $request)
    {
        $request->validate([
            'deleteId' => 'required'
        ]);

        $section = Warehouse::find($request->get('deleteId'));
        $section->delete();

        return redirect()->route('admin-edit-warehouse')->with('message', 'Section/process removed');
    }

}
