<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule; 

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page']['title'] = 'Manage Roles';
        $data['roles'] = Role::all();

        return view('admin/manage_roles', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page']['title'] = 'Create Role';

        return view('admin/create_role', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles|max:255|alpha',
            'display_name' => 'required|unique:roles|max:255',
            'description' => 'required|max:255'
        ]);

        $record = new Role();
        $record->name = strtolower($request->get('name'));
        $record->display_name = $request->get('display_name');
        $record->description = $request->get('description');
        $record->save();

        return redirect()->route('admin-roles')->with('message', 'New role added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {   
        $data['role'] = $role;
        $data['page']['title'] = 'Update Role - '.$role->display_name;

        return view('admin/edit_role', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $request->validate([
            'display_name' => [
                'required',
                Rule::unique('roles')->ignore($role),
            ],
            'description' => 'required'
        ]);

        $record = Role::find($role->id);
        $record->display_name = $request->get('display_name');
        $record->description = $request->get('description');
        $record->update();

        return redirect()->route('admin-roles')->with('message', 'Role updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }
}
