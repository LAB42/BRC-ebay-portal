<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
  public function __construct()
  {    
    $colors = array(
    	'listed' => '#43a92c',
    	'nis' => '#e95351',
    	'a' => '#f1b13b',
        'pat' => '#f1b13b',
        'picked' => '#d870c5',
        'sold' => '#04923e',
        'pending' => '#afa48f',
        'missing' => '#d7d8d7',
        'south1' => '#1a3351',
        'south2' => '#7d1c23',
        'south3' => '#43a92c',
        'south4' => '#baddea',
        'south5' => '#5a98c0',
        'south6' => '#e4d7aa',
        'south7' => '#afa48f',
        'south8' => '#627b80',
        'south9' => '#f1b13b',
    	'north10' => '#1a3351',
    	'north11' => '#7d1c23',
    	'north12' => '#43a92c',
    	'north13' => '#baddea',
    	'north14' => '#5a98c0',
    	'north15' => '#e4d7aa',
    	'north16' => '#afa48f',
    	'north17' => '#627b80',
    	'north18' => '#f1b13b',
    	'' => '',
    );

    $areas = array(
    	'south1', 'south2', 'south3', 'south4', 'south5', 'south6', 'south7', 'south8', 'south9',
    	'north10', 'north11', 'north12', 'north13', 'north14', 'north15', 'north16', 'north17', 'north18'
    );

    // Sharing is caring
    view()->share('colors', $colors);
    view()->share('areas', $areas);
 }
}