<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ItemFolder;
use App\Inventory;

class ItemFolderController extends Controller
{

    public function moveItemToFolder(Request $request)
    {
        if(request('folder') === NULL):
            return redirect()->route('item', ['id' => request('item')])->with('message', 'Please select a folder');
        else:
            $item = Inventory::find(request('item'));
            $item->folder_id = request('folder');
            $item->update();

            $folderName = ItemFolder::find(request('folder'))->name;

            return redirect()->route('item', ['id' => request('item')])->with('message', 'Item moved to '. $folderName);
        endif;
    }

    public function removeItemFromFolder(Request $request)
    {
        $item = Inventory::find(request('item'));
        $item->folder_id = NULL;
        $item->update();

        return redirect()->route('item', ['id' => request('item')])->with('message', 'Item removed from folder');
    }

    /**
     * Display a item of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page']['title'] = 'Item Folders';

        $data['folders'] = ItemFolder::all();

        return view('item_folders', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page']['title'] = 'Create Item Folder';
        return view('create_item_folder', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'color' => 'required|size:7|regex:^#(?:[0-9a-fA-F]{3}){1,2}^'
        ]);

        $record = new ItemFolder;
        $record->name = request('name');
        $record->description = request('description');
        $record->color = request('color');
        $record->save();

        return redirect()->route('item-folders')->with('message', 'Added item folder');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['folder'] = ItemFolder::find($id);
        $data['items'] = Inventory::where('folder_id', '=', $id)->get();
        $data['page']['title'] = 'Item Folder - '. $data['folder']->name;
        return view('single_item_folder', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['folder'] = ItemFolder::find($id);

        $data['page']['title'] = 'Edit Item Folder - '.$data['folder']->name;

        return view('edit_item_folder', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'color' => 'required|size:7|regex:^#(?:[0-9a-fA-F]{3}){1,2}^'
        ]);

        $record = ItemFolder::find($id);
        $record->name = request('name');
        $record->description = request('description');
        $record->color = request('color');
        $record->save();

        return redirect()->route('item-folders')->with('message', 'Updated item folder');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hasItems = Inventory::where('folder_id', '=', $id)->count();

        if($hasItems > 0):
            return redirect()->route('item-folders')->with('message', 'Unable to delete a folder that contains items');
        else:
            $record = ItemFolder::find($id);
            $record->delete();

            return redirect()->route('item-folders')->with('message', 'Deleted item folder');
        endif;
    }
}
