<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PackingType;

class AdminPackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page']['title'] = 'Manage Packing Types';
        $data['types'] = PackingType::all();

        return view('admin/packing', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page']['title'] = 'Add packing type';
        return view('admin/create_packing', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $record = new PackingType;

        $record->name = $request->get('name');
        $record->length = $request->get('length');
        $record->width = $request->get('width');
        $record->height = $request->get('height');
        $record->stock = $request->get('stock');

        $record->save();

        return redirect()->route('admin-packing')->with('message', 'New packing type added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['type'] = PackingType::find($id);

        $data['page']['title'] = 'Packing Type - '.$data['type']->name;
        return view('admin/single_packing', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['type'] = PackingType::find($id);

        $data['page']['title'] = 'Edit Packing Type - '.$data['type']->name;

        return view('admin/edit_packing', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $table = PackingType::find($id);
        $table->name = $request->get('name');
        $table->length = $request->get('length');
        $table->width = $request->get('width');
        $table->height = $request->get('height');
        $table->stock = $request->get('stock');
        $table->update();
        
        return redirect()->route('admin-packing-show', ['id' => $id])->with('message', 'Packing type updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {           
        $record = PackingType::find($id);
        $record->delete();
        return redirect()->route('admin-packing', ['id' => $id])->with('message', 'Packing type deleted');
    }
}
