<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShippingType;

class AdminShippingTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page']['title'] = 'Shipping Types';
        $data['types'] = ShippingType::all();

        return view('admin/shipping_types', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page']['title'] = 'Add Shipping Type';

        return view('admin/create_shipping_type', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $record = new ShippingType;
        $record->name =  $request->get('name');
        $record->save();

        return redirect()->route('admin-shipping-types')->with('message', 'New shipping type added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['type'] = ShippingType::find($id);
        $data['page']['title'] = 'Shipping Type - '.$data['type']->name;

        return view('admin/single_shipping_type', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['type'] = ShippingType::find($id);
        $data['page']['title'] = 'Edit Shipping Type - '.$data['type']->name;

        return view('admin/edit_shipping_type', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $record = ShippingType::find($id);
        $record->name = $request->get('name');
        $record->update();

        return redirect()->route('admin-shipping-types')->with('message', 'Updated shipping type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = ShippingType::find($id);
        $record->delete();
        return redirect()->route('admin-shipping-types')->with('message', 'Deleted shipping type');
    }
}
