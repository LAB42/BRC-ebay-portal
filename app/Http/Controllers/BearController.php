<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BearController extends Controller
{
    public function index(){
    	$data['page']['title'] = 'Support / Technical Staff';

    	$data['bears'] = array(
    		array(
    			'name' => 'Geoffrey',
    			'img' => 'img/bears/geoffrey.jpg',
    			'role' => 'Human / Bear Relationship Manager'
    		),
    		array(
    			'name' => 'Bluebell',
    			'img' => 'img/bears/bluebell.jpg',
    			'role' => 'Technical Engineer (Ex-NSA Analyst)'
    		),
    		array(
    			'name' => 'Flat Bear',
    			'img' => 'img/bears/flatbear.jpg',
    			'role' => 'Pest Control'
    		),
    		array(
    			'name' => 'Johnny',
    			'img' => 'img/bears/johnny.jpg',
    			'role' => 'Health & Fitness Manager'
    		),
    		array(
    			'name' => 'Moz',
    			'img' => 'img/bears/moz.jpg',
    			'role' => 'Stock Manager'
    		),
    		array(
    			'name' => 'Mr. Fox',
    			'img' => 'img/bears/mrfox.jpg',
    			'role' => 'Head of Cunning Strategies'
    		),
    		array(
    			'name' => 'Paddington',
    			'img' => 'img/bears/paddington.jpg',
    			'role' => 'International Retail Manager'
    		),
    		array(
    			'name' => 'Rasta Mouse',
    			'img' => 'img/bears/rastermouse.jpg',
    			'role' => 'Head of Herbal Resources'
    		),
    		array(
    			'name' => 'Bruce',
    			'img' => 'img/bears/bruce.jpg',
    			'role' => 'The Muscle'
    		),

    	);

    	return view('bears', $data);
    }
}
