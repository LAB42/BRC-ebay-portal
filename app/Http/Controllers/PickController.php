<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;

class PickController extends Controller
{
    public function index($area = NULL, Request $request)
    {
    	$data['page']['title'] = "Picklist";
    	if($area != NULL):
    		
    		if($area === 'clearance'):
    			$data['area'] = ucfirst($area);
    			$data['pick'] = Inventory::where('status', '!=', 'pending')->where('price', '<', 30)->where('status', '=', 'A')->take(25)->orderBy('price', 'ASC')->get();
    		elseif($area = 'all' || $area == 'north' || $area == 'south'):    			
                $min = 0;
                $max = 10000;                
                if($request->has('sortPrice')):

                    $sortPrice = $request->get('sortPrice');

                    switch ($sortPrice) {
                        case 'under_30':
                            $min = 0;
                            $max = 30;
                            break;

                        case '30_50':
                            $min = 30;
                            $max = 50;
                            break;

                        case '50_100':
                            $min = 50;
                            $max = 100;
                            break;

                        case 'over_100':
                            $min = 100;
                            $max = 10000;
                            break;
                        
                        default:
                            $min = 0;
                            $max = 10000;
                            break;
                    }

                endif;
                $data['area'] = ucfirst($area); 

                if($area !== 'all'):	    		     
    			     $data['pick'] = Inventory::where('status', '!=', 'pending')->where('price', '>=', $min)->where('price', '<=', $max)->where('area', 'like', '%'.$data['area'].'%')->where('status', '=', 'A')->take(25)->orderBy('area', 'ASC')->orderBy('location', 'ASC')->orderBy('crate_item_number', 'ASC')->orderBy('price'
    				, 'DESC')->get();
                else:                     
                     $data['pick'] = Inventory::where('status', '!=', 'pending')->where('price', '>=', $min)->where('price', '<=', $max)->where('status', '=', 'A')->take(25)->orderBy('area', 'ASC')->orderBy('location', 'ASC')->orderBy('crate_item_number', 'ASC')->orderBy('price'
                    , 'DESC')->get();
                endif;
    		else:
    			abort(404);
    		endif;
    	else:
    		$data['area'] = 'All';
    		$data['pick'] = Inventory::where('status', '=', 'A')->take(25)->orderBy('price', 'DESC')->get();
    	endif;

        foreach($data['pick'] as $item):
            $data['items'][] = $item->id;
        endforeach;

        $request->flash();

   		return view('picklist', $data);
    }


    public function assign(Request $request)
    {
        $data['page']['title'] = 'Assign Picklist';

        $items = json_decode($request->get('items'));

        foreach($items as $item):
            $entry = Inventory::find($item);
            $entry->status = 'PENDING';
            $entry->save();
        endforeach;

        return redirect('picklist');
        
        
    }

    public function getAssigned(){

    }
}
