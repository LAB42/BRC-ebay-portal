<?php

namespace App\Http\Controllers;

use App\BRC;
//use DTS\eBaySDK\Finding\Types;
use Illuminate\Http\Request;

use DTS\eBaySDK\Credentials\CredentialsProvider;
use DTS\eBaySDK\Finding\Services\FindingService;
use DTS\eBaySDK\Finding\Types as FindingTypes;
use DTS\eBaySDK\Sdk;
use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;









class GuideController extends Controller
{

    public function selectingItems()
    {
        $data['page']['title'] = 'Selecting Items';

        return view('selecting_items', $data);
    }

    public function researchingItems()
    {
        $data['page']['title'] = 'Researching Items';

        return view('researching_items', $data);
    }

    public function packingItems()
    {
        $data['page']['title'] = 'Packing Items';

        return view('packing_items', $data);   
    }

    public function clothingBrands()
    {
        $data['page']['title'] = 'Clothing Brands';

        $data['brandValues'] = array(
            '£0 - £10' => array('Apricot (NewLook)','Atmosphere (Primark)','AX Paris (New Look)','Burton','Cameo Rose (New Look)','Cherokee (Tesco)','Clarks','Cotswold','Cotton Traders','Dash','Debenhams ','Dorothy Perkins','Edinburgh Wooden Mill','Esmara (Lidl)','EWM (Edinburgh Wooden Mill)','F & F (Florence and Fred at Tesco)','Fila','Fiorelli','Garrani Stoke','George (asda)','H & M','House of Jazz workout clothing','Jane Norman','Joe Brown','JOY','Kickers','Killah','Klass ','Knickerbox','Lands End','M & S basics','M&Co','Mela London','Miss Sixty','Mylene Klass (Littlewoods)','New Look','Old Navy','Papaya (Matalan)','Peacocks','Pilot','Postcard','Promod','Punky Fish','Rare','Red Herring','Risky','Rogers and Rogers (Matalan)','Schuh','Select','Soon (Matalan)','Soviet','Sports World','Stone Bay (Tesco)','Tesco Value','Tu (Sainsburys)','Twenty Eight','urban Spirit (Peacocks)','You (New Look )','Zara basic'),
            '£10 to £30' => array('& Other Stories','7 For All Mankind','Abercrombie & Fitch','Accessorise','Aden & Anais childrens','Adidas','Aftershock','Agatha childrens','Aldo','All Star Converse','American Vintage','Amy Vermont','Animal','Ana Alcazar','Anne Klein','Anthropologie','Antik Batik','Applebottom','Asos','Aspinall of London','Autograph (M & s)','Banana Republic','Bear Paw','Benetton','Berkshire','Bershka','Bertie','Bessie','Betsy Johnson','Betty Jackson','Billabong','Birkenstock','Blue Cult','Blue Inc ','Blue Velvet','Blumarine','Bodas ','Boden','Bull Frog','Buy','Camilla','Carvela','Cath Kidston','CDX','Champion','Club Monaco','Coach','Coast','Comme de Garcons','Converse','Cos','Country Casuals','Criminal Damage','Damart','Dance','Decathlon','Diesel','Doc Martin','Dune','East','Ellis Brigham','Esprit','Fat Face','FCUK','Fearne Cotton (very.co.uk)','Footlocker','Fossil','Fred Perry','French Connection','From Somewhere','Gap','Gaudi','Geox','Ghost','Gloria Vanderbilt','Great Plain','Guess','Guide London Clothing','Havren','Hawes & Curtis','Head over Heels By Dune shoes','Heidi Klein swimwear','Henleys','Hobbs','Holly Willoughby (Very.co.uk)','Hotter shoes','Hunza G swimwear','Indigo (M&S)','Intimimissi lingerie','Iron Fist','J Crew','Jack Wills','Jacques Vert','Jaeger','James Lakeland','Janet Reger','Jigsaw','Jo Jo Maman Bebe childrens','John Smedley','Joie','Joules','Juicy Couture','K Swiss sportswear','Karen Millen','Kate Kuba','Kath Kidson','Kew (Jigsaw outlet)','Khaadi','Killer Loop outdoor wear from Benetton','Killy - ski wear','kitristudio','Kookai','Koton','Krisp','La Senza','Lacoste','Lambretta','Laura Ashley','Lee Cooper','Levi','Lillywhites','Limited Collection (M&S)','Linea ( HOF)','Little Mistress (Debenhams)','Liz Claiborne','Liza Bruce','LK Bennett','Lola Rose (HOF)','Long Tall Sally','Lonsdale','Loomstate','Lotus','Lyle & Scott','Madewell','Mango','Marc O\' Polo','Marilyn Moore','Marimekko','Massimo Dutti (top end Zara)','Megan Park bags','Melissa Odabash beachwear','Mexx','Michael Stars','Mikey jewellery','Millets','Mint Velvet','Miss Selfridge','MNG suit','Monsoon','Morgan','Moss Bros','Needle and Thread','Next','Nike','Nine west','North Face','Northcrest','Notify','Nougat','Oakley','Oasis','Oeuf childrens','Office','Olly & Nic','Olsen','Omar Mansoor','O\'Neill outdoor wear','Osprey','Oui','Paper Denim & Cloth','Patagonia','Penguin','Pepe','Pepe jeans','Per Una ( M & S)','Petit Bateau childrens','Phase Eight','Philanthropy','Pied a terre','Pierce II fionda (Debenhams)','Pierre Cardin','Pineapple Dance wear','Pink Soda','Planet (HOF)','Platinum (HOF)','Police','Portfolio (Debenhams)','Precis Petite','Press & Bastyan','Princess Tam Tam ( nightwear, swimwear & lingerie)','Principles','Project E','Pull and Bear','Puma','Pure Collection cashmere','Pussycat Dolls (New Look)','Rachel Zoe','Racing Green','Reebok','Regatta','Reiss','Religion','Replay','Republic','Rip Curl','River Island','Rock Republic','Rohan','Ronit Zilkha','Rosetti','Rowallan','Roxy','Runway','Russell & Bromley','Russell Athletic','Sara Berman','Schott','Scorah Patullo','Seven','Signature','Sisley','Skechers','Sofia Cashmere','Spanx','Sprayway','Star by Julian McDonald','Stella McCartney adidas','Stephanie Kelian','Steve Madden','Strellson','Stussy','Superdry','Swarovski','Sweaty Betty','Ted Baker','Teddy Smith','The Vestry','The White Company','Thomas Nash(Debenhams)','Thomas Pink','Timberland','TM Lewin','TNT clothing','Toast','Tom Wolfe','Toms','Topman','Topshop','Trader (Debenhams)','True Religion','TSE','Tulah a la Mode','Ugg','Uniqlo','Unique (Topshop)','Urban outfitters','Urban Stone Apparel','Van Daal','Vans','Vera Moda','Vera Pelle','Vilbrequin mens swimwear','Virginia','Von Dutch','Wallis','Wayne Cooper','Weird Fish','Whistles','White Stuff','Windsmoor','Wolford','Wolsey','Wrangler','Zakee Shariff','Zara woman','Z-Brand'),
            '£30 - £70' => array('Acne Studios','Adam Lippes','Alberton Makali','Alexander Wang','Alice & Olivia','All Saints','Allegra Hicks','Almost Famous','Anna Sui','Anya Hindmarch','Aquascutum','Armani Jeans','Artigiano','Attico','Ba & Sh ','Badgeley Mischka','Barbour','Basia Zarzycka','BCBGmaxzaria','Beaufille','Bella Freud','Ben de Lisi','Biba','Boss','Boyd','Brora','Burfitt','Butler and Wilson','Cacharel','Calvin Klein','Caramel Childrens','Caravana','Caroline Charles','Caroline Herrera','Carven','Cefinn','Charles Tyrwhitt','Charles Jourdan','Charlotte Olympia','Christian Lacroix','Christian Louboutin','Churc\'s shoes','Citizens of Humanity maternity','Claudie Pilot','Coco da Mer','Cole Haan','Corneliani','Cushnie et Ochs','Damaris','Daniel Fiesoli','Desigual (Spanish)','Diane Von Furstenburg','DKNY','Dodo Bar Of','Donna Karan','Dosa','Earl','Eberjey ','Eileen Fisher','Elie Tahari','Ellery','Elizabeth & James','Elspeth Gibson','Emma Hope','Emma Purnell','Emporio Armani','Erdem','Erikson Beamon','Ermenegildo Zegna','Escada Sport','Extart &Panno','Favourbrook','Ferretti','Furla','Gabriela Ligenza Hats, shoes, accessories','Ganni','George Rech','Gerry Weber','Gigi','Guy Laroche','Halston Heritage','Harrods','Hatch maternity','Helly Hansan outdoor wear','Hogan shoes','Hugo Boss','J Brand','Jamin Puech','Jean Muir','Jil Sander','Joseph','JW Anderson','Kalita','Kal Kaur Rai','Kate Spade','Katherine Hamnett','Kenneth Cole','Kennth Cole Reaction','La Perla','La Tigre','Liberty','Loeffler Randall','Longchamps','Louise Rosewald','Lulu Guinness','Maggie Marilyn','Maje','Malone Souliers','Mandarina Duck','Manoush','Marc Jacobs','Matthew Williamson','McQ','Michael Kors','Moschino','Nina Ricci','Noa Noa','Norma Kamati','Not your daughters jeans','Orla Kiely','Orvis','Ossie Clarke','Oswald Boateng','Oushka','Paige ','Paul and Joe','Paul and Shark','Paul Costelloe','Paul Smith','Paule Ka','Peruvian Connection','Peter Jensen','Philosophy','Pippa Small jewellery','Pleats Please (Issey Miyake)','Polo Ralph Lauren','Pretty Ballerina','Pringle','Rag and Bone','Ralph Lauren','Rayne','Red Valentino','Repetto','Rive Gauche Yves Saint Laurent','Rixo London','Rocha John Rocha','Roger Vivier','Saks Fifth Avenue','Salomon','Sass & Bide','Self Portrait','Shanghai Tang','Shirin Guild','Sigerson Morrison shoes and boots','Snow & Rock','Solace London','Solange Azagury-Partridge jewellery','Sonia Rykel','Sophia Webster','Sportmax by Max Mara','Stuart Weitzman','T by Alexander Wang','Tata Naka','The Jacksons','The Kooples','Theory','Thierry Mugler','Tibi','Tiger of Sweden','Todds','Tommy Hilfiger','Tory Burch','Ulla Johnson','Vanessa Bruno','Vera Wang','Vetements','Vince','Vivetta','Vivienne Westwood','Weekend Max Mara','Zandra Rhodes','Zimmermann'),
            '£70+' => array('3.1 Philip Lim','Alaia','Alberta Feretti','Alexander McQueen','Alessandra Rich','Altuzarra','Ann Demeulemeester','Acquazurra','Aristocrat childrens','Armani','Aurelie Bidermann','Balenciaga','Balmain','Bottega Veneta','Brioni','Bruno Cucinelli','Bucherer','Bulgari','Burberry','Canada Goose','Cartier','Casadel','Celine','Chanel','Cheney','Chloe','Christian Laurier Paris','Christopher Kane','Cloe Cassandro','Danile Wellington','Dior','Dolce & Gabbana','Dries Van Noten','Duke and Dexter','Elsa Schaparelli','Emilia Wickstead','Emilio Pucci','Escada','Etro','Faberge','Fendi','Fernando Jorge jewellery','Giambattista Valli','Gianvito Rossi shoes','Givenchy','Gucci','Guiseppe Zanotti shoes','Helmut Lang','Hermes','Herve Leger','Isabel Marant','Issey Miyake','James Perse','Jason Wu','Jean Paul Gaultier','Jenny Packham','Jimmy Choo','Johanna Ortiz','John Galliano','Jonathan Simkhai','Judith Leiber bags','Karl Lagerfeld','Kenzo','Khaite','Lanvin','Loewe','Loro Piana','Louis Vuitton','Magda Butrym','Manolo Blahnik','Marchesa','Marni','Mary Katrantzou','Max Mara','Mira Miketi','Missoni','Miu Miu','Moncler','Mulberry','Oscar de la Renta','Peter Pilotto','Philip Treacey','Piazza Sempione','Prada','Preen by Thornton Bregazzi','Proenza Schouler','Roberto Cavalli','Roksanda','Roland Mouret','Rosetta Getty','Saint Laurent','See by Chloe','Simone Rocha','Stella Mc Cartney','Stone Island','Thom Brown','Tom Ford','Ungaro','Valentino','Versace','Victoria Beckham','Yohji Yamamoto','Zac Posen')
        ); 

        return view('clothing_brands', $data);
    }

    public function warehouseWorkflow()
    {
        $data['page']['title'] = 'Warehouse Workflow';

        return view('warehouse_workflow', $data);
    }

    public function refreshTokens(){

    }

    public function categories(){
          $profile = 'production';
        $path = app_path('credentials.ini');

        $provider = CredentialsProvider::ini($profile, $path);

        $service = new Services\TradingService([
            'credentials' => $provider,
            'apiVersion' => '1065',
            'globalId'   => 'EBAY-GB',
            'siteId' => 3,
            'authorization' => BRC::getAuth()
        ]);

        $request = new Types\GetSuggestedCategoriesRequestType();
        $request->Query = 'bong';

        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = BRC::getAuth();
        
        $response = $service->GetSuggestedCategories($request);

        $results = array();
        
        foreach($response->SuggestedCategoryArray->SuggestedCategory as $category):
            $r = array(
                'id' => $category->Category->CategoryID,
                'name' => $category->Category->CategoryName                
            );
            array_push($results, $r);
        endforeach;


        $data['page']['title'] = 'Search for Categories';
        $data['results'] = $results;

        return view('category_search', $data);










    }

     public function customInfo(){

        $profile = 'production';
        $path = app_path('credentials.ini');
        
        $provider = CredentialsProvider::ini($profile, $path);

        $service = new Services\TradingService([
            'credentials' => $provider,
            'apiVersion' => '1065',
            'globalId'   => 'EBAY-GB',
            'siteId' => 3,
            'authorization' => BRC::getAuth()
        ]);

        $request = new Types\GetCategorySpecificsRequestType();

        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $config['oAuth'];

        
        $request->CategoryID = array('181076');
        $response = $service->getCategorySpecifics($request);


        $data['page']['title'] = 'Custom Info';
        $data['results'] = $response->Recommendations;

        return view('custom_info', $data);
    }


    public function index(){
        $data['page']['title'] = 'Guides';

        return view('guides', $data);
    }

    public function size(){
        $data['page']['title'] = 'Size Guide';
        return view('size_guide', $data);
    }

    public function bulkEstValue(){
        $data['page']['title'] = 'Bulk Estimate Value';

        return view('bulk_est_value', $data);
    }

    

    public function doBulkEstValue(Request $request){

        $profile = 'production';
        $path = app_path('credentials.ini');


        $provider = CredentialsProvider::ini($profile, $path);
        $provider = CredentialsProvider::memoize($provider);


           $service = new FindingService([
                'credentials' => $provider,
                'apiVersion' => '1.13.0',
                'globalId'   => 'EBAY-GB',
            ]);
            
            $fields = preg_split('$\n$', $request->get('items'));
            $results = array();
            foreach($fields as $field):
                $request = new FindingTypes\FindCompletedItemsRequest();
                $request->keywords = $field;
                $request->paginationInput = new FindingTypes\PaginationInput();
                $request->paginationInput->entriesPerPage = 10;
                $response = $service->findCompletedItems($request);
                // Output the response from the API.
                if ($response->ack !== 'Success') {
                    foreach ($response->errorMessage->error as $error) {
                        printf("Error: %s\n", $error->message);
                    }
                } else {                    
                    $count = 0;
                    $value = 0;
                    $rawData = array();
                    foreach ($response->searchResult->item as $item) {
                        $count++;
                        $rawData[] = array(
                            'title' => $item->title,
                            'value' => sprintf('%0.2f',$item->sellingStatus->currentPrice->value),
                            'other' => $item,
                            'image' => $item->galleryURL
                        );
                        $value = $value + $item->sellingStatus->currentPrice->value;
                    }
                    if($count > 0):
                        $average = '£'.round($value / $count);
                    else:
                        $average = 'NF';
                    endif;
                    $results[] = ['name' => $field, 'average' => $average, 'rawData' => $rawData];
                }
            endforeach;

            $data['page']['title'] = 'Bulk Estimate Value';
            $data['results'] = $results;

            return view('bulk_est_value', $data);
    

    }

}