<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['areas'] = Area::with('shop')->orderBy('id')->get();        
        $data['page']['title'] = 'Areas';

        return view('areas', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page']['title'] = 'Create Area';
        return view('create_area', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:250',
            'area_manager' => 'required|max:250',
            'area_manager_email' => 'required|max:250|email',
            'cluster_manager' => 'required|max:250',
            'cluster_manager_email' => 'required|max:250|email',
            'notes' => 'required|max:5000',
            'color' => 'required|size:7|regex:^#(?:[0-9a-fA-F]{3}){1,2}^'
        ]);

        $record = new Area;
        $record->name = request('name');
        $record->area_manager = request('area_manager');
        $record->area_manager_email = request('area_manager_email');
        $record->cluster_manager = request('cluster_manager');
        $record->cluster_manager_email = request('cluster_manager_email');
        $record->notes = request('notes');
        $record->color = request('color');
        $record->save();

        return redirect('areas')->with('message', 'Area added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['area'] = Area::with('shop')->where('id', '=', $id)->firstOrFail();
        $data['page']['title'] = 'Area - '.$data['area']->name;

        return view('single_area', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['area'] = Area::find($id);
        $data['page']['title'] = 'Edit Area - '.$data['area']->name;
        return view('edit_area', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:250',
            'area_manager' => 'required|max:250',
            'area_manager_email' => 'required|max:250|email',
            'cluster_manager' => 'required|max:250',
            'cluster_manager_email' => 'required|max:250|email',
            'notes' => 'required|max:5000',
            'color' => 'required|size:7|regex:^#(?:[0-9a-fA-F]{3}){1,2}^'
        ]);

        $record = Area::find($id);
        $record->name = request('name');
        $record->area_manager = request('area_manager');
        $record->area_manager_email = request('area_manager_email');
        $record->cluster_manager = request('cluster_manager');
        $record->cluster_manager_email = request('cluster_manager_email');
        $record->notes = request('notes');
        $record->color = request('color');
        $record->update();

        return redirect('areas')->with('message', 'Area updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //NOT USED
    }
}
