<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InventoryStatus;
use App\API;

class AdminController extends Controller
{
	public function index()
	{
		$data['page']['title'] = 'Admin Dashboard';
		return view('admin/admin_dashboard', $data);
	}

	// Inventory
	public function inventoryStatusSettings()
	{	
		$data['page']['title'] = 'Inventory Status Settings';
		$data['statusList'] = InventoryStatus::all();
		return view('admin/inventory_status_settings', $data);
	}

	public function inventoryDoAddStatus(Request $request)
	{
		echo '<pre>';
		var_dump($request->all());
		echo '</pre>';
	}

	public function api()
	{
		$data['page']['title'] = 'API Key';
		return view('admin/api', $data);
	}

	public function setKey(Request $request){

		$set = new API;
		$set->key = $request->get('key');
		$set->save();

		return redirect()->route('admin')->with('message', 'API Key Updated');

	}
}
