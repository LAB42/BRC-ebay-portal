<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;
use App\Warehouse;
use App\WarehouseStaff;
use App\InventoryStatus;
use Auth;

class ListingController extends Controller
{

	public function mine()
	{
		$items = Inventory::where('location', '=', 'listing')->with('shop')->get();
		$data = array(array('Name'));

		$data = array();

		array_push($data, array(
			'Name',
			'SKU',
			'Area',
			'Shop Code',
			'Shop Name',
			'Location',
			'Name',
			'G/A',
			'Value',
			'Weight',
			'Dimensions',
			'Condition'
		));

		foreach($items as $item):
			array_push($data, array(
				$item->item_name,
				$item->shop->code .','. $item->gift_aid .',W'.date('W', time()).',NH',
				$item->shop->area->name,
				$item->shop->code,
				$item->shop->name,
				$item->location,
				$item->item_name,
				$item->gift_aid,
				$item->price,
				$item->weight_int.$item->weight_unit,
				$item->size_text,
				$item->condition
			));
		endforeach;

		return response()->csv($data);

	}

	public function index($id = NULL)
	{
		$data['page']['title'] = 'Listings';

		if($id !== NULL):
			$data['filters'] = Warehouse::where('type', '=', 'process')->orderBy('id', 'ASC')->get();
			$data['currentFilter'] = Warehouse::where('id', '=', $id)->firstOrFail();
			$data['items'] = Inventory::where('location', '=', $data['currentFilter']->section)->with('shop')->get();
		else:
			$data['filters'] = Warehouse::where('type', '=', 'process')->orderBy('id', 'ASC')->get();

			$filters = array();
			foreach($data['filters'] as $f):
				array_push($filters, $f->section);
			endforeach;

			$data['items'] = Inventory::whereIn('location', $filters)->get();
		endif;

		$data['currentTask'] = WarehouseStaff::where('user', '=', Auth::user()->id)->firstOrFail()->current_task;

		return view('listings', $data);
	}

	public function createListing($id = NULL)
	{
		$data['page']['title'] = 'Create Listing';

		if($id != NULL):
			$data['template'] = Inventory::where('id', '=', $id)->firstOrFail();

			if($data['template']->gift_aid === ','):
				$data['template']->gift_aid = '';
			endif;
		else:
			$data['template'] = NULL;
		endif;

		return view('create_listing', $data);
	}

	public function doCreateListing(Request $request)
	{
		$data['page']['title'] = 'Listed Created';

		$data['output'] = $request->all();

		//TODO - REDO
		$record = Inventory::find(request('itemId'));
		$record->status = InventoryStatus::where('name', 'ILIKE', '%LISTED%')->firstOrFail()->id;
		$record->update();

		//delete this blade file
		return view('created_listing', $data);

		//return redirect()->route('item', ['id' => request('itemId')])->with('message', 'Listing created');
	}
}
