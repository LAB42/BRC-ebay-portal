<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AccountController extends Controller
{
	public function __construct()
    {
    //    $this->middleware('auth');
    }

		public function getCurrentUserId()
		{
			return response()->json((object) array('user' => Auth::user()->id));
		}

	public function dashboard()
	{
		//$data['page']['title'] = 'Manage your account';
		return view('dashboard');
	}

	public function manageAccount()
	{
		$data['page']['title'] = 'Manage Account';
		return view('manage_account', $data);
	}
}
