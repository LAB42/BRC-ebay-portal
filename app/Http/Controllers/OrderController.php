<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App;
use DTS\eBaySDK\Credentials\CredentialsProvider;
use DTS\eBaySDK\Finding\Services\FindingService;
use DTS\eBaySDK\Sdk;
use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;

use App\BRC;

class OrderController extends Controller
{

    public function index()
    {
    	$data['page']['title'] = 'Orders';

    	$data['orders'] = Order::all();
        $data['orderIDs'] = array();
        foreach($data['orders'] as $order):
            $data['orderIDs'][] = $order->id;
        endforeach;

    	return view('orders', $data);
    }

    public function orderSheet()
    {
        $page['title'] = 'Order Sheet';
        $data['orders'] = Order::whereHas('orderStatus', function($query){
            $query->where('status', '=', 'waiting');
        })->get();

        return view('order_sheet', $data);
    }

    public function printInvoices(Request $request)
    {
        $ids = unserialize($request->input('ids'));

        $pdf = App::make('dompdf.wrapper');
        $invoices = Order::whereIn('id', $ids)->get();

            $data = '
                        <style>
                        .page-break {
                            page-break-after: always;
                        }
                        </style>
                    ';

            foreach($invoices as $invoice):
                if($invoice->fd_state_or_province === 'false' || $invoice->fd_state_or_province === null):
                    $invoice->fd_state_or_province = '';
                else:
                    $invoice->fd_state_or_province = $invoice->fd_state_or_province.'<br>';
                endif;
                if($invoice->ship_state_or_province !== 'false' || $invoice->ship_state_or_province === null):
                    $invoice->ship_state_or_province = '';
                else:
                    $invoice->ship_state_or_province = $invoice->ship_state_or_province.'<br>';
                endif;
                $data .= '
                        <img src="'.asset('img/brc_logo.png').'" alt="" style="width: 200px;">
                        <h1 style="text-align: center; margin-bottom: 0;">Invoice</h1>
                        <h2 style="text-align: center; margin-top: 0; font-weight: normal;">Order #'.$invoice->id.'</h2>

                        <div style="width: 50%; float: left;">
                            <h4 style="margin-bottom: 0;">Payment Address</h4>
                            <p style="margin-top: 0;">
                                '.$invoice->fd_addressLine1.'<br>
                                '.$invoice->fd_addressLine2.'<br>
                                '.$invoice->fd_city.'<br>
                                '.$invoice->fd_state_or_province.'
                                '.$invoice->fd_postal_code.'<br>
                                '.$invoice->fd_country.' ('.$invoice->ship_country_code.')<br>
                            </p>
                        </div>

                        <div style="width: 50%; float: right; text-align: right;">
                            <h4 style="margin-bottom: 0;">Delivery Address</h4>
                            <p style="margin-top: 0;">
                                '.$invoice->ship_addressLine1.'<br>
                                '.$invoice->ship_addressLine2.'<br>
                                '.$invoice->ship_city.'<br>
                                '.$invoice->ship_state_or_province.'
                                '.$invoice->ship_postal_code.'<br>
                                '.$invoice->ship_country.' ('.$invoice->ship_country_code.')<br>
                            </p>
                        </div>

                        <div style="clear: both;">
                            <hr style="border: none; border-top: solid 1px #ee2a24;">
                            <h4 style="margin-bottom: 5px;">Items</h4>
                            <table style="width: 100%; text-align: center; font-size: 0.8em;">
                                <thead style="background: #eee;">
                                    <tr>
                                        <td style="width: 10%; padding: 5px 0; font-weight: bold;">ID #</td>
                                        <td style="width: 50%; padding: 5px 0; font-weight: bold;">Item Name</td>
                                        <td style="width: 10%; padding: 5px 0; font-weight: bold;">Qty</td>
                                        <td style="width: 15%; padding: 5px 0; font-weight: bold;">Unit Cost</td>
                                        <td style="width: 15%; padding: 5px 0; font-weight: bold;">Total Cost</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Test Item</td>
                                        <td>2</td>
                                        <td>&pound;25.00</td>
                                        <td>&pound;50.00</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Test Item 2</td>
                                        <td>1</td>
                                        <td>&pound;35.00</td>
                                        <td>&pound;35.00</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Test Item 3</td>
                                        <td>1</td>
                                        <td>&pound;75.00</td>
                                        <td>&pound;75.00</td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr style="border: none; border-top: solid 1px #ee2a24;">


                            <div style="text-align: center; margin-top: 175px;">
                                <img src="'.asset('img/invoice_banner.jpg').'" alt="">
                            </div>

                            <p style="font-size: 0.7em; text-align: center; margin-top: 25px;">
                            Praesent at lorem ex. Sed scelerisque mi lacus, in suscipit elit ultrices sit amet. Nullam sagittis erat metus, sit amet cursus odio dapibus a. Pellentesque eu lacus vulputate, congue metus eget, pulvinar dolor. Etiam libero lectus, ornare vitae luctus eget, porttitor eget diam. Sed orci nisl, semper quis leo accumsan, tincidunt consequat dui. Donec sollicitudin rhoncus dolor id elementum. Nullam egestas accumsan auctor. Aliquam id lacus at metus eleifend mollis in non ex. Donec feugiat porta ipsum, ut feugiat diam pulvinar ac. Sed non purus pellentesque, bibendum erat in, tincidunt lacus. Morbi faucibus nunc a euismod malesuada. Aliquam quis sapien eget est eleifend mattis.
                            </p>
                            <p style="font-size: 0.5em; colour: #666; margin-top: 25px;">
                                <em>RM48 / '.date('d-m-Y', time()).'</em>
                            </p>
                        </div>
                        <div class="page-break"></div>
                    ';
                endforeach;


        $pdf->loadHTML($data);
        return $pdf->stream();
    }

    public function printLabels(Request $request)
    {
        $ids = unserialize($request->input('ids'));

        $pdf = App::make('dompdf.wrapper');
        $invoices = Order::whereIn('id', $ids)->get();

            $data = '
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <style>
                    *{
                        margin: 0;
                        padding: 0;
                    }
                    p{
                        font-size: 0.9em;
                    }
                    .page-break {
                        page-break-after: always;
                    }
                </style>
            </head>
            <body>

                    ';

            foreach($invoices as $invoice):
                if($invoice->fd_state_or_province === 'false' || $invoice->fd_state_or_province === null):
                    $invoice->fd_state_or_province = '';
                else:
                    $invoice->fd_state_or_province = $invoice->fd_state_or_province.'<br>';
                endif;
                if($invoice->ship_state_or_province !== 'false' || $invoice->ship_state_or_province === null):
                    $invoice->ship_state_or_province = '';
                else:
                    $invoice->ship_state_or_province = $invoice->ship_state_or_province.'<br>';
                endif;
                $data .= '
                        <div style="margin: 0px;">
                            <img src="'.asset('img/label_header.png').'" alt="" style="width: 250px; margin: 10px; float: right;">
                            <p style="float: left; margin: 15px; font-weight: bold;">#'.$invoice->id.'</p>
                            <p style="margin: 10px; padding-top: 5px; clear: both; text-align: center;">
                                '.$invoice->ship_addressLine1.'<br>
                                '.$invoice->ship_addressLine2.'<br>
                                '.$invoice->ship_city.'<br>
                                '.$invoice->ship_state_or_province.'
                                '.$invoice->ship_postal_code.'<br>
                                '.$invoice->ship_country.' ('.$invoice->ship_country_code.')<br>
                            </p>
                        </div>

                    ';
                endforeach;

                $data .= '
                    </head>
                    <body>
                ';

        $pdf->setPaper([0, 0, 175, 360], 'landscape');
        $pdf->loadHTML($data);
        return $pdf->stream();
    }

    public function singleOrder($id)
    {
        // $status = array(
        //     'name' => 'In Progress',
        //     'background' => '#1a3351'
        // );




        // $order = array(
        //     'username' => 'testUser',
        //     'checkoutNotes' => 'These are my shipping demands.',
        //     'orderDate' => '28/08/2018',
        //     'buyerTimezone' => 'GMT',
        //     'addressLine1' => 'Invisible House',
        //     'addressLine2' => '123 Fake Street',
        //     'city' => 'Nowhereington',
        //     'countryCode' => 'GB',
        //     'country' => 'United Kingdom',
        //     'postalCode' => 'SW4 2AD',
        //     'stateOrProvince' => '',
        //     'fullfillmentInstuctions' => 'shipping',
        //     'maxEstimatedDeliveryDate' => '01/09/2018',
        //     'minEstimatedDeliveryDate' => '29/08/2018',
        //     'shippingCarrierCode' => 'Royal Mail',
        //     'shippingServiceCode' => 'Standard 48',
        //     'companyName' => 'Some Company',
        //     'contactAddressLine1' => 'Invisible House',
        //     'contactAddressLine2' => '123 Fake Street',
        //     'contactCity' => 'Nowhereington',
        //     'contactCountryCode' => 'GB',
        //     'contactCountry' => 'United Kingdom',
        //     'contactPostalCode' => 'SW4 2AD',
        //     'contactStateOrProvince' => '',
        //     'contactEmail' => 'testUser@test.com',
        //     'contactFullName' => 'Test User',
        //     'contactPhoneNumber' => '555-555-555',
        //     'shipToReferenceId' => '05848455464215',
        //     'lastModifiedDate' => '28/08/2018',
        // );

        // $items = array(
        //     (object) array(
        //         'id' => 123,
        //         'name' => 'test item',
        //         'location' => 'WK31DJ',
        //         'qty' => 1,
        //         'price' => 34.25
        //     )
        //     );

        // switch($order['fullfillmentInstuctions']):
        //     case 'digital':
        //         $shippingType = 'digital';
        //     break;

        //     case 'pickup':
        //         $shippingType = 'collection';
        //     break;

        //     case 'shipping':
        //         $shippingType = 'shipping';
        //     break;

        //     case 'seller_defined':
        //         $shippingType = 'seller defined';
        //     break;

        //     default:
        //         $shippingType = 'unknown';
        // endswitch;

        // $data['page']['title'] = 'Order #'.$id;
        // $data['order'] = (object)$order;
        // $data['items'] = (object)$items;
        // $data['status'] = (object)($status);
        // $data['shippingType'] = ucfirst($shippingType);
        // $data['total'] = 33.25;
        // $data['shippingPaid'] = 3.25;
        // $data['shippingActual'] = 3.25;

        // ////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////
        //



        $profile = 'production';
        $path = app_path('credentials.ini');

        $provider = CredentialsProvider::ini($profile, $path);

        $service = new Services\TradingService([
            'credentials' => $provider,
            'apiVersion' => '1065',
            'globalId'   => 'EBAY-GB',
            'siteId' => 3,
            'authorization' => BRC::getAuth()
        ]);

        $request = new Types\GetCategorySpecificsRequestType();

        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = BRC::getAuth();


        $request->CategoryID = array('181076');
        $response = $service->getCategorySpecifics($request);



        $data['results'] = $response->Recommendations;



$status = array(
            'name' => 'In Progress',
            'background' => '#1a3351'
        );


        $data['order'] = Order::find($id);

        $data['page']['title'] = 'Order #'.$id;

        $data['status'] = (object)($status);
        $data['shippingType'] = 'shipping';
        $data['total'] = 33.25;
        $data['shippingPaid'] = 3.25;
        $data['shippingActual'] = 3.25;


        return view('single_order', $data);
    }





































}
