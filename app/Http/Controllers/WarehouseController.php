<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Mail\WarehouseOverview;
use Illuminate\Support\Facades\Mail;

use App\Warehouse;
use App\WarehouseStaff;
use App\Inventory;

class WarehouseController extends Controller
{
    public function sendEmailReport()
    {
        Mail::to('nhallworth@redcross.org.uk')->send(new WarehouseOverview());
        return redirect()->route('warehouse')->with('message', 'Email report sent');
    }

	function showStatus($low, $med, $high, $current){
	    $limit = 700;
            $high = round($limit);
            $medium = round($limit / 2);
            $low = round($limit / 4);

	    if($current >= $high):
	        return 'high';
	    endif;
	    if($current <= $medium):
	        return 'low';
	    endif;
	    if($current >= $medium && $current <= $high):
	        return 'medium';
	    endif;
	}

    public function index(){
    	$data['page']['title'] = 'Warehouse';

    	$data['staff'] = WarehouseStaff::all();

    	$processes = Warehouse::where('type', '=', 'process')->get();
  		foreach($processes as $process):
			$process->current = Inventory::where('location', '=', $process->section)->count();
    		$process->status = $this->showStatus($process->low, $process->medium, $process->high, $process->current);
    	endforeach;
    	$data['processes'] = $processes;

    	$storage = Warehouse::where('type', '=', 'storage')->get();
    	foreach($storage as $space):
    		$space->current = Inventory::where('location', '=', $space->section)->count();
    		$space->status = $this->showStatus($space->low, $space->medium, $space->high, $space->current);
    	endforeach;
  		$data['storage'] = $storage;

      $warehouseUser = WarehouseStaff::where('user', '=', Auth::user()->id)->firstOrFail();
      $data['userInitials'] = $warehouseUser->initials;
      $data['userCurrentTask'] = $warehouseUser->current_task;


    	return view('warehouse', $data);
    }

    public function doCheckIn(Request $request){

        $processId = $request->get('process');

        $user = array(
                    'id' => Auth::user()->id,
                    'name' => Auth::user()->name
                );

        if($request->get('process') === 'out'):
            $process['name'] = 'out';
        elseif($request->get('process') === 'in'):
            $process['name'] = 'break';
        else:
            $process['id'] = $request->get('process');
            $process['name'] = Warehouse::find($process['id'])->section;
        endif;

        $checkIn = WarehouseStaff::where('user', '=', $user['id'])->firstOrFail();

        if($process['name'] === 'out'):
            $checkIn->current_task = 'out-of-office';
            $checkIn->working = false;
        elseif($process['name'] === 'break'):
            $checkIn->current_task = 'break';
            $checkIn->working = false;
        else:
            $checkIn->current_task = $process['name'];
            $checkIn->working = true;
        endif;

        $checkIn->save();

        if($process['name'] === 'out'):
            return redirect()->route('warehouse')->with('message', 'Out - '.$user['name']);
        elseif($process['name'] === 'break'):
            return redirect()->route('warehouse')->with('message', 'In -  '.$user['name']);
        else:
            if($request->has('redirect')):
                return redirect()->back()->with('message', 'Checked in for '. $process['name'] .' as '. $user['name']);
            else:
                return redirect()->route('warehouse')->with('message', 'Checked in for '. $process['name'] .' as '. $user['name'].'<br><a href="'.route('listings-filter', ['id' => $process['id']]).'">View Items</a>');
            endif;
            //return redirect()->route('listings-filter', ['id' => $process['id']])->with('message', 'Checked in for '. $process['name'] .' as '. $user['name']);
        endif;
    }
}
