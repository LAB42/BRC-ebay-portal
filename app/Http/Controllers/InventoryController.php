<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ItemFolder;
use App\PackingType;
use App\ShippingType;
use App\InventoryStatus;
use App\Inventory;
use App\Area;
use App\Shop;
use App\BRC;
use App\ItemLog;
use App\ItemPhotos;
use DB;


class InventoryController extends BaseController
{

    /**
     * Default method for this controller
     *
     * Returns all items in inventory with the option to filter by month and year
     *
     * @category Controller method
     * @param  string $month  Month for filter
     * @param  string $year   Year for filter
     * @return void         Blade
     */
    public function all(){

    	$data['page']['title'] = 'Inventory';

        //Get list of all areas
        $data['areas'] = Area::orderBy('id')->get();

        //Default page state
        $data['items'] = Inventory::orderBy('date_received', 'ASC')->orderBy('id', 'ASC')->with('inventoryStatus')->with('shop')->get();

        $data['currentStatusFilter'] = NULL;
        $data['currentDateFilter'] = NULL;
        $data['currentAreaFilter'] = NULL;
        $data['statusList'] = InventoryStatus::orderBy('color', 'ASC')->orderBy('id', 'ASC')->get();
        $data['filteredResults'] = FALSE;

    	return view('inventory', $data);
    }

    public function filter(Request $request){

        $query = Inventory::query();

        //Status filter
        $query->when(request('status'), function($q){
            return $q->where('status', '=', request('status'));
        });

        //Date filter
        $query->when(request('date'), function($q){
            $sortByDate = BRC::getDateRange(request('date')['month'], request('date')['year']);
            $q->where('date_received', '>=', $sortByDate['lower']);
            $q->where('date_received', '<=', $sortByDate['upper']);
            return $q;
        });

        //Area filter
        $query->when(request('area'), function($q){
            return $q->whereHas('shop', function ($q) {
                $q->where('areaId', '=', request('area'));
            });
        });

        //Shop code filter
        $query->when(request('shopCode'), function($q){
            return $q->whereHas('shop', function ($q) {
                $q->where('code', '=', request('shopCode'));
            });
        });

        //Get relationships
        $query->with('inventoryStatus');
        $query->with('shop');

        //Order results
        $query->orderBy('date_received', 'ASC');
        $query->orderBy('id', 'ASC');

        //Get data
        $data['items'] = $query->get();

        //Pass filter information
        if($request->has('status')):
            $data['currentStatusFilter'] = array(
                'id' => request('status'),
                'name' => InventoryStatus::where('id', '=', request('status'))->firstOrFail()->name
            );
        else:
            $data['currentStatusFilter'] = NULL;
        endif;

        if($request->has('date.month') && $request->has('date.year')):
            $data['currentDateFilter'] = array(
                'month' => request('date')['month'],
                'year' => request('date')['year']
            );
        else:
            $data['currentDateFilter'] = NULL;
        endif;

        if($request->has('area')):
            $data['currentAreaFilter'] = array(
                'id' => request('area'),
                'name' => Area::where('id', '=', request('area'))->firstOrFail()->name
            );
        else:
            $data['currentAreaFilter'] = NULL;
        endif;

        if($request->has('shopCode')):
            $data['currentShopFilter'] = array(
                'shopCode' => request('shopCode')
            );
        else:
            $data['currentShopFilter'] = NULL;
        endif;


        $data['statusList'] = InventoryStatus::orderBy('color', 'ASC')->orderBy('id', 'ASC')->get();
        $data['areas'] = Area::orderBy('id')->get();
        $data['page']['title'] = 'Inventory - Filtered';

        $data['filteredResults'] = TRUE;

        return view('inventory', $data);
    }

    public function item($id, $next = NULL){
    	$data['page']['title'] = 'Item #'.$id;
      $data['inventoryStatuses'] = InventoryStatus::orderBy('color', 'ASC')->orderBy('id', 'ASC')->get();
      $data['itemFolders'] = ItemFolder::all();
      $data['packingTypes'] = PackingType::all();
      $data['shippingTypes'] = ShippingType::all();
    	$data['item'] = Inventory::where('id', '=', $id)->with('inventoryStatus')->with('shop')->with('photos')->firstOrFail();

      $actions = ItemLog::where('item_id', '=', $id)->orderBy('created_at', 'DESC')->get();
      $data['actions'] = BRC::unique_multidim_array($actions, 'action');

      if($next):
          $data['nextItem'] = $next;
      endif;

    	if($data['item'] == NULL):
    		echo 'Item not found';
    		exit();
    	endif;

    	return view('item', $data);
    }

    // public function search(Request $request){

    //     $query = $request->get('itemSearchQuery');


    //     $shopCode = $request->get('shopCodeQuery');


    //     $data['areaList'] = array();
    //     foreach(Area::all() as $area):
    //         $data['areaList'][] = $area->name;
    //     endforeach;

    //     $data['page']['title'] = 'Inventory';
    //     $data['areasList'] = Area::all();

    //     $items = Inventory::query();

    //     $data['currentStatusFilter'] = NULL;
    //     //Default page state

    //     //Just name
    //     if($request->has('itemSearchQuery') && $request->get('shopCodeQuery') === NULL):
    //         $items = $items->where('item_name', 'ILIKE', '%'.$query.'%');
    //         $data['itemSearchQuery'] = $query;
    //         $data['shopCodeQuery'] = NULL;

    //     //Just shop code
    //     elseif($request->has('shopCodeQuery') && $request->get('itemSearchQuery') === NULL):
    //         $items = $items->whereHas('shop', function($code) use ($shopCode){
    //             $code->where('code', '=', $shopCode);
    //         });
    //         $data['itemSearchQuery'] = NULL;
    //         $data['shopCodeQuery'] = $shopCode;

    //     //Both
    //     elseif($request->has('shopCodeQuery') && $request->has('itemSearchQuery')):
    //         $items = $items->where('item_name', 'ILIKE', '%'.$query.'%')->whereHas('shop', function($code) use ($shopCode){
    //             $code->where('code', '=', $shopCode);
    //         });
    //         $data['itemSearchQuery'] = $query;
    //         $data['shopCodeQuery'] = $shopCode;
    //     endif;

    //     $items = $items->orderBy('date_received', 'ASC')->with('inventoryStatus')->with('shop');

    //     $data['items'] = $items->get();

    //     //Set date filter info for conditional formatting
    //     $data['currentDateFilter'] = array(
    //         'month' => NULL,
    //         'year' => NULL
    //     );

    //     return view('inventory', $data);
    // }

    // public function inventoryByArea($area = NULL)
    // {
    //     $data['areasList'] = Area::all();

    //     if($area != NULL):
    //         $area = Area::find($area);
    //         $data['page']['areaSet'] = TRUE;
    //         $data['page']['area'] = $area;
    //         $data['page']['areaName'] = $area->name;
    //         $name = $data['page']['areaName'];
    //         $data['page']['areaId'] = $area;

    //         $shopsInArea = Shop::where('area', 'ILIKE', $area->name)->get();
    //         foreach($shopsInArea as $sia):
    //             $shopIds[] = $sia->id;
    //         endforeach;
    //         $data['items'] = Inventory::where('date_received', '>=', '2018-01-01')->whereIn('shopId', $shopIds)->with('shop')->with('inventoryStatus')->orderBy('date_received', 'ASC')->get();
    //     else:
    //         $area = 'All';
    //             $data['page']['areaSet'] = TRUE;
    //         $data['page']['areaName'] = 'All areas';
    //         $name = $data['page']['areaName'];
    //         $data['page']['areaId'] = $area;
    //         $data['items'] = Inventory::where('date_received', '>=', '2018-01-01')->orderBy('date_received', 'ASC')->get();
    //     endif;

    //         return view('inventory_by_area', $data);

    // }

    // public function searchItemId(Request $request)
    // {
    //     $itemId = $request->get('itemIdQuery');
    //     $action = $request->get('action');
    //     $item = Inventory::find($itemId);

    //     if(count($item) === 1):
    //         if($action === 'view'):
    //             return redirect()->route('item', ['id' => $itemId]);
    //         elseif($action === 'move'):
    //             return 'moving page';
    //         endif;

    //     else:
    //         return redirect()->route('inventory')->with('message', 'No results for '. $itemId);
    //     endif;
    // }

    public function search(Request $request)
    {
        $query = $request->get('query');
        $items = new Inventory;

        $items = $items->with('shop');
        $items = $items->with('inventoryStatus');


        $items->fuzziness = true;
        if($query):
            $constraints = $items;
            $items = Inventory::search($query)->constrain($constraints);
        endif;

        $data['items'] = $items->get();

        //Pass filter information
        if($request->has('status')):
            $data['currentStatusFilter'] = array(
                'id' => request('status'),
                'name' => InventoryStatus::find(request('status'))->name
            );
        else:
            $data['currentStatusFilter'] = NULL;
        endif;

        if($request->has('date.month') && $request->has('date.year')):
            $data['currentDateFilter'] = array(
                'month' => request('date')['month'],
                'year' => request('date')['year']
            );
        else:
            $data['currentDateFilter'] = NULL;
        endif;

        if($request->has('area')):
            $data['currentAreaFilter'] = array(
                'id' => request('area'),
                'name' => Area::find(request('area'))->name
            );
        else:
            $data['currentAreaFilter'] = NULL;
        endif;

        if($request->has('shopCode')):
            $data['currentShopFilter'] = array(
                'shopCode' => request('shopCode')
            );
        else:
            $data['currentShopFilter'] = NULL;
        endif;


        $data['statusList'] = InventoryStatus::orderBy('color', 'ASC')->orderBy('id', 'ASC')->get();
        $data['areas'] = Area::orderBy('id')->get();
        $data['page']['title'] = 'Inventory Search';
        $data['searchQuery'] = request('query');
        $data['filteredResults'] = TRUE;

        return view('inventory', $data);




    }
}
