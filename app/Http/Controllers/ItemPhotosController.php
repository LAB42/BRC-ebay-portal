<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Image;
use Intervention\Image\ImageManager;
use App\Inventory;
use App\ItemPhoto;

use Auth;
use App\BRC;


class ItemPhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['photos'] = ItemPhoto::all();

        $data['page']['title'] = 'Item photos';

        return view('photos', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
      $data['item'] = Inventory::find($id)->with('photos')->firstOrFail();

      $data['page']['title'] = 'Add photos';

      return view('add_item_photos', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'files' => 'required'
      ]);

      $item = Inventory::find(request('itemId'));

      foreach(request('files') as $file):
        echo 'file://'.preg_replace('^\\\\^', '/', $file);
        echo '<br />';
      endforeach;

      $now = time();
      $folder = 'item_photos';
      $filename = array(
        'item' => 'item_'.$item->id,
        'week_prefix' => '_wk',
        'week' => strtolower(date('W', $now)),
        'day_prefix' => '_',
        'day' => strtolower(date('D', $now)),
        'time' => date('_h_m_s', $now),
        'ext' => '.jpg'
      );

      //'item_photos/item_'.$item->id.'_wk'.strtolower(date('W_D_h_m', time())).'.jpg'
      $image = Image::make(request('files')[0])->resize(900,900)->save($folder.'/'.implode($filename));

      $record = new ItemPhoto;
      $record->item_id = $item->id;
      $record->filename = implode($filename);
      $record->added_by = Auth::user()->id;
      $record->approved = false;
      $record->notes = 'N/A';
      $record->mime = $image->mime();
      $record->file_size = $image->filesize();
      $record->dimensions = $image->width().'_'.$image->height();
      $record->save();

      $message = (count(request('files')) > 1) ? 'Photos uploaded' : 'Photo uploaded';

      BRC::logItemAction($item->id, $message.' - '.implode($filename));

      return redirect()->route('photo-show', ['id' => $item->id])->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data['item'] = Inventory::where('id', '=', $id)->with('photos')->firstOrFail();

      $data['page']['title'] = 'Photos for item #'.$data['item']->id;

      return view('item_photos', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
