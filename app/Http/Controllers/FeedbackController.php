<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;

class FeedbackController extends Controller
{

    public function updateNotes(Request $request)
    {
        $request->validate([
            'notes' => 'required'
        ]);

        $record = Feedback::find(request('messageId'));
        $record->notes = request('notes');
        $record->update();

        return redirect()->route('feedback')->with('message', 'Message notes updated.');
    }

    public function markAsRead(Request $request)
    {        
        $request->validate([
            'read' => 'required'
        ]);

        $record = Feedback::find(request('messageId'));
        $record->read = TRUE;
        $record->update();

        return redirect()->route('feedback')->with('message', 'Marked as read');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['messages'] = Feedback::all();

        $data['page']['title'] = 'Feedback';
        return view('feedback', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page']['title'] = 'Submit feedback, question or error message';

        return view('create_feedback', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'submitted_by' => 'required',
            'subject' => 'required',
            'title' => 'required',
            'message' => 'required',
        ]);

        $record = new Feedback;
        $record->submitted_by = request('submitted_by');        
        $record->title = request('title');
        $record->subject = request('subject');
        $record->message = request('message');
        $record->notes = 'Pass this message onto NH';
        $record->read = FALSE;
        $record->save();

        switch(request('subject')):
            case 'support':
                $validationMessage = 'Your support request has been submited. We suggest turning in off and on again.';
            break;

            case 'general':
                $validationMessage = 'Thank you for your feedback. We hopes it\'s good. If not, we\'ll ignore it for 2 to 3 working days.';
            break;

            case 'suggestion':
                $validationMessage = 'We like ideas! Thank you for submitting one. We\'ll be in touch soon.';
            break;

            case 'error':
                $validationMessage = 'Sorry for the shoddy coding. The code monkey responsible has been given their P45. We\'ll let you know when the problem is fixed.';
            break;

            default:
                $validationMessage = 'Message received. We\'ll get back to you soon.';
            
        endswitch;

        return redirect()->route('feedback-create')->with('message', $validationMessage);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['message'] = Feedback::find($id);

        $data['page']['title'] = 'Feedback Message';
        return view('single_feedback', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Feedback::find($id);
        $record->delete();

        return redirect()->route('feedback')->with('message', 'Message deleted');
    }
}
