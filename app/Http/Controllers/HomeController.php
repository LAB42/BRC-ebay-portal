<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WarehouseStaff;
use App\Warehouse;
use App\Inventory;
use App\InventoryStatus;


use DTS\eBaySDK\Credentials\CredentialsProvider;
use DTS\eBaySDK\Finding\Services\FindingService;
use DTS\eBaySDK\Finding\Types as FindingTypes;
use DTS\eBaySDK\Sdk;
use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;

use App\BRC;

class HomeController extends Controller
{
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function contact()
    {
      $data['page']['title'] = 'Contact the Ebay Team';
      return view('contact', $data);
    }

    public function forecast()
    {
      $data['page']['title'] = 'Weather Forecast';

      $weatherURL = 'http://api.openweathermap.org/data/2.5/forecast?id=2633810&appid=82993e95f31b7789b51e800333ed2a99';
      $data['weather'] = (object) json_decode(file_get_contents($weatherURL), true);

      return view('forecast', $data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // $profile = 'production';
      // $path = app_path('credentials.ini');
      //
      // $provider = CredentialsProvider::ini($profile, $path);
      //
      // $service = new Services\TradingService([
      //     'credentials' => $provider,
      //     'apiVersion' => '1065',
      //     'globalId'   => 'EBAY-GB',
      //     'siteId' => 3,
      //     'authorization' => BRC::getAuth()
      // ]);
      //
      // $request = new Types\GetFeedbackRequestType();
      //
      // $request->DetailLevel = ['ReturnAll'];
      // $request->UserID = 'BritishRedCross';
      //TODO - ADD PAGINATION
      //
      // $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
      // $request->RequesterCredentials->eBayAuthToken = BRC::getAuth();
      //
      // $response = $service->GetFeedback($request);
      //
      // $results = array();
      // foreach($response->FeedbackDetailArray->FeedbackDetail as $feedback):
      //     $r = (object) array(
      //         'type' => $feedback->CommentType,
      //         'text' => $feedback->CommentText,
      //     );
      //     array_push($results, $r);
      // endforeach;
      // $data['feedback'] = $results;

      $weatherURL = 'http://api.openweathermap.org/data/2.5/weather?id=2633810&appid=82993e95f31b7789b51e800333ed2a99';
      $data['weather'] = (object) json_decode(file_get_contents($weatherURL), true);
      
      $data['warehouseStaff'] = WarehouseStaff::all();

      $data['todo'] = array();

      $warehouseProcesses = Warehouse::where('type', '=', 'process')->get();

      foreach($warehouseProcesses as $process):
        array_push($data['todo'], (object) array(
          'type' => 'warehouse',
          'id' => $process->id,
          'name' => $process->section,
          'current' => Inventory::where('location', '=', $process->section)->count()
        ));
      endforeach;

      $data['customerService'] = array();

      $customerService = (object) array(
        (object) array(
          'name' => 'Messages',
          'count' => '18'
        ),
        (object) array(
          'name' => 'Orders',
          'count' => '42'
        ),
        (object) array(
          'name' => 'Return Requests',
          'count' => '3'
        )
      );

      $data['customerService'] = $customerService;


      $data['inventoryStats'] = array();

      $statuses = array('Pending', 'Listed', 'Missing', 'RTS - Low Value', 'Dangerous');

      foreach($statuses as $status):
        $sn = InventoryStatus::where('name', '=', $status)->firstOrFail();
        array_push($data['inventoryStats'], (object) array(
          'name' => $status,
          'count' => Inventory::where('status', '=', $sn->id)->count()
        ));
      endforeach;

      return view('home', $data);
    }

    public function invoice()
    {
        $data['page']['title'] = 'Invoice';

        return view('invoice', $data);
    }

    public function p2gQuote(Request $request)
    {
        //Add form validation
        //
        $input = array(
                   'weight'  => $request->input('weight'),
                   'length'  => $request->input('length'),
                   'width'  => $request->input('width'),
                   'height'  => $request->input('height'),
                   'collectionCountry' => $request->input('collectionCountry'),
                   'destinationCountry' => $request->input('destinationCountry'),

                );

        return redirect('https://www.parcel2go.com/quotes?col='.$input['collectionCountry'].'&dest='.$input['destinationCountry'].'&mdd=0&p=1~'.$input['weight'].'|'.$input['length'].'|'.$input['width'].'|'.$input['height'].'&quotetype=Default#/results/collection');
    }

}
