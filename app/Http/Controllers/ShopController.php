<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\Areas;
use App\Inventory;

class ShopController extends BaseController
{
	public function index()
	{
		$data['page']['title'] = 'Shops';
		$data['shops'] = Shop::with('area')->orderBy('name', 'ASC')->get();
		
		$data['areas'] = Areas::orderBy('id')->get();

		$data['areaList'] = array();
		foreach($data['areas'] as $area):
			array_push($data['areaList'], $area->name);
		endforeach;
	
		return view('shops', $data);
	}


	public function doFindShop(Request $request)
	{		
		$q = $request->input('query');		
		$results = Shop::where('code', 'ILIKE', '%'.$q.'%')->orWhere('name', 'ILIKE', '%'.$q.'%')->orderBy('name', 'ASC')->get();

		if(count($results) == 0):
			return redirect()->route('shops')->with('messageWarning', 'No results');
		elseif(count($results) == 1):
			return redirect('shops/'.$results[0]->id);
		else:			
			$resultsData['page']['title'] = 'Shop Search Results';
			$resultsData['results'] = $results;			
			return view('shop_search_results', $resultsData);
		endif;		
	}

	public function singleShop($id)
	{	
		$data['page']['title'] = 'Shop Info';
		$data['shop'] = Shop::find($id);

		$stats = Inventory::where('shopId', '=', $id)->with('inventoryStatus')->get();
		$data['stats'] = array(
			'total' => count($stats),
			'listed' => 0,
			'returns' => 0,			
			'sold' => 0
		);

		foreach($stats as $stat):
			switch (strtolower($stat->inventoryStatus->name)):
				case 'listed':
					$data['stats']['listed']++;
					break;
				case (strpos(strtolower($stat->inventoryStatus->name), 'rts') !== FALSE):
					$data['stats']['returns']++;
					break;				
				case 'sold':
					$data['stats']['sold']++;				
					break;
			endswitch;
		endforeach;

		return view('single_shop', $data);		
	}

	public function singleArea($id)
	{		
		$data['page']['title'] = 'Shops';
		$data['shops'] = Shop::where('areaId', '=', $id)->with('area')->get();		
		
		$data['areas'] = Areas::orderBy('id')->get();

		$data['areaList'] = array();
		foreach($data['areas'] as $area):
			$data['areaList'][] = $area->name;
		endforeach;

	
		return view('shops', $data);
	}
}
