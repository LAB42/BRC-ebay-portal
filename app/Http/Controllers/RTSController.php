<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App;

class RTSController extends Controller
{
    public function index()
    {
    	$data['page']['title'] = 'Return to Shop';
    	return view('rts_dashboard', $data);
    }

    public function generateRTSLabel()
    {
    	$data['page']['title'] = 'Generate RTS Label';
    	return view('generate_rts_label', $data);
    }

    public function doGenerateRTSLabel(Request $request)
    {
    	// $formData = array(
    	// 		'code' => $request->input('code'),
    	// 		'qty' => $request->input('qty')
    	// 	);
    	
    	// $label = array(
    	// 	'shop' => $shop = Shop::where('code', '=', $formData['code'])->firstOrFail(),
    	// 	'crates' => $formData['qty']
    	// );

    	// return redirect()->route('generate-rts-label')->with('label', $label);
        
        $pdf = App::make('dompdf.wrapper');
        
    
            $data = '
                        <style>
                        *{
                            font-weight: normal;
                            line-height: 2em;
                        }
                        .page-break {
                            page-break-after: always;
                        }
                        </style>
                    ';

            $shop = Shop::where('code', '=', $request->input('code'))->firstOrFail();
            
            for($i=1; $i <= $request->input('qty'); $i++):              
                $data .= '
                <div style="text-align: center;">
                    <div style="border: dashed 2px #333; padding: 25px;">
                        <h2 style="font-size: 2em; font-weight: bold;">EBAY RETURNS</h2>
                        <h2 style="font-size: 3em; font-weight: bold; margin: 50px;">'.$shop->area->name.'</h2>
                        <hr>
                        <h1 style="font-size: 3em; margin: 50px;">'.$shop->name.'</h1>
                        <h3 style="font-size: 3em; margin: 50px;">'.$shop->code.'</h3>                        
                        <h2 style="font-size: 4em;">'.$i.'/'.$request->input('qty').'</h2>
                    </div>
                </div>
                <div class="page-break"></div>
                ';
            endfor;

            $pdf->setPaper('a4', 'landscape');
            $pdf->loadHTML($data);
            return $pdf->stream();
    }
}
