<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class UserRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($userID)
    {        
        $data['user'] = User::find($userID);
        $data['roles'] = Role::all();
        $data['currentRoles'] = array();

        foreach($data['user']->roles as $role):
            array_push($data['currentRoles'], $role->id);
        endforeach;

        $data['page']['title'] = 'Add Role to User - '.$data['user']->name;

        return view('admin/create_user_role', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        var_dump($request->all());

        $request->validate([
            'userID' => 'required',
            'roleID' => 'required'
        ]);

        $record = User::find($request->get('userID'));
        $record->attachRole($request->get('roleID'));

        return redirect()->route('admin-user-role-edit', ['id' => $request->get('userID')])->with('message', 'Role added to user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::find($id);    
        $data['page']['title'] = 'Manage User Roles - '.$data['user']->name;

        return view('admin/edit_user_role', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user, $role)
    {
        $user = User::find($user);

        $user->detachRole($role);

        return redirect()->route('admin-user-role-edit', ['id' => $user])->with('message', 'Removed role from user');
    }
}
