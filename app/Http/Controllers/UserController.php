<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page']['title'] = 'Manage Users';
        $data['users'] = User::orderBy('id', 'ASC')->get();

        return view('admin/manage_users', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page']['title'] = 'Create User';

        return view('admin/create_user', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:users|max:255',
            'email' => 'required|unique:users|max:255',
            'password' => 'required|confirmed',
            'color' => 'required|size:7|regex:^#(?:[0-9a-fA-F]{3}){1,2}^'
        ]);

        $record = new User();
        $record->name = $request->get('name');
        $record->email = $request->get('email');
        $record->password = Hash::make($request->get('password'));
        $record->color = $request->get('color');
        $record->save();

        return redirect()->route('admin-users')->with('message', 'New user added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::find($id);
        $data['page']['title'] = 'Edit User - '.$data['user']->name;

        return view('admin/edit_user', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => [
                    'required',
                    'max:255',
                    Rule::unique('users')->ignore($id),
                ],
            'email' => [
                    'required',
                    'max:255',
                    Rule::unique('users')->ignore($id),
                ],
            'color' => [
                    'required',
                    'size:7',
                    'regex:^#(?:[0-9a-fA-F]{3}){1,2}^'
            ]
        ]);
        
        $record = User::find($id);
        $record->name = $request->get('name');
        $record->email = $request->get('email');
        $record->color = $request->get('color');
        $record->update();

        return redirect()->route('admin-users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
