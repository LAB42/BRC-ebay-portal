<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logsheet extends Model
{
	protected $table = 'logsheets';	
	protected $primaryKey = 'id';

	public function shop()
    {
        return $this->belongsTo('App\Shop');
    }
}
