<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
	use Searchable;
	
	protected $table = 'shops';
	protected $primaryKey = 'id';

	public function logsheets()
    {
        return $this->hasMany('App\Logbook');
    }


	public function inventory(){
		return $this->belongsTo('App\Inventory');
	}

	public function area()
	{
		return $this->hasOne('App\Area', 'id', 'areaId');
	}
   
}
