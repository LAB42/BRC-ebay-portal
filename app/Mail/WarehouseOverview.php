<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Inventory;
use App\InventoryStatus;

class WarehouseOverview extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {        
        $this->total = Inventory::all()->count();

        //FETCH LIST OF STATUSES FROM DB AND LOOP FOR ARRAY OUTPUT
        

        $statuses = InventoryStatus::all();

        $breakdownCounts = array();
        foreach($statuses as $status):            
            array_push($breakdownCounts, array(
                'id' => $status->id,
                'name' => $status->name,
                'count' => Inventory::where('status', '=', $status->id)->count()
            ));
        endforeach;
        $this->breakdownCounts = $breakdownCounts;

        return $this->from('ebay@test.com')
                ->view('email/warehouse_overview')
                ->with([
                    'total' => $this->total,
                    'breakdownCounts' => $this->breakdownCounts
                ]);
    }
}
