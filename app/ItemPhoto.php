<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemPhoto extends Model
{
    protected $table = 'item_photos';
    protected $primaryId = 'id';

    public function inventory(){
      return $this->hasOne('App\Inventory', 'id', 'item_id');
    }
}
