<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackingType extends Model
{
    protected $table = 'packing_types';
    protected $primaryId = 'id';    
}
