import React from 'react';
import { render } from 'react-dom';

import {ShowCategories, CustomInfo} from './components/createListing';

const CategoryElement = document.querySelector('#categories');
if(CategoryElement){
	render(<ShowCategories />, CategoryElement);
}

const CustomInfoElement = document.querySelector('#customInfo');
if(CustomInfoElement){
	render(<CustomInfo />, CustomInfoElement);
}