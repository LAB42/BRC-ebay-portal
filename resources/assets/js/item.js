import React from 'react';
import { render } from 'react-dom';

import {Name, Desp, ValueIndicator, EstValue, Price, GiftAid, ShopName, ShopCode, Category, Location, WeightInt, WeightUnit, WeightText, SizeText, Condition} from './components/item/Edit';

const NameElement = document.querySelector('#itemName');
if(NameElement){
	render(<Name />, NameElement);
}

const DespElement = document.querySelector('#itemDesp');
if(DespElement){
	render(<Desp />, DespElement);
}

const ValueIndicatorElement = document.querySelector('#valueIndicator');
if(ValueIndicatorElement){
	render(<ValueIndicator />, ValueIndicatorElement);
}

const EstValueElement = document.querySelector('#estValue');
if(EstValueElement){
	render(<EstValue />, EstValueElement);
}

const PriceElement = document.querySelector('#price');
if(PriceElement){
	render(<Price />, PriceElement);
}

const GiftAidElement = document.querySelector('#giftAid');
if(GiftAidElement){
	render(<GiftAid />, GiftAidElement);
}

const ShopNameElement = document.querySelector('#shopName');
if(ShopNameElement){
	render(<ShopName />, ShopNameElement);
}

const ShopCodeElement = document.querySelector('#shopCode');
if(ShopCodeElement){
	render(<ShopCode />, ShopCodeElement)
}

const CategoryElement = document.querySelector('#category');
if(CategoryElement){
	render(<Category />, CategoryElement);
}

const LocationElement = document.querySelector('#location');
if(LocationElement){
	render(<Location />, LocationElement);
}

const WeightIntElement = document.querySelector('#weightInt');
if(WeightIntElement){
	render(<WeightInt />, WeightIntElement);
}

const WeightUnitElement = document.querySelector('#weightUnit');
if(WeightUnitElement){
	render(<WeightUnit />, WeightUnitElement);
}

const WeightTextElement = document.querySelector('#weightText');
if(WeightTextElement){
	render(<WeightText />, WeightTextElement);
}

const SizeTextElement = document.querySelector('#sizeText');
if(SizeTextElement){
	render(<SizeText />, SizeTextElement);
}

const ConditionElement = document.querySelector('#condition');
if(ConditionElement){
	render(<Condition />, ConditionElement);
}
