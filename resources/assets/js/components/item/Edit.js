import React, { Component } from 'react';
import axios from 'axios';
import ContentEditable from "react-sane-contenteditable";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';

const editableStyle = {
  background: '#f6f6f6',
  borderRadius: 3,
  padding: '10px 20px',
  display: 'inline-block',
  border: 'solid 1px #ccc',
  fontSize: '1.4em',
  width: '100%'
};

const editableStyleTextarea = {
  background: '#f6f6f6',
  borderRadius: 3,
  padding: '10px 20px',
  minHeight: '150px',
  width: '100%',
  display: 'inline-block',
  border: 'solid 1px #ccc',
  fontSize: '1.4em'
};

const editableStyleTitle = {
  background: '#f6f6f6',
  borderRadius: 3,
  padding: '10px 20px',
  display: 'inline-block',
  border: 'solid 1px #ccc',
  fontSize: '2em',
  width: '100%',
};


var getUrl = window.location;

if(getUrl.hostname.match('localhost')){
  var currentURL = 'http://' + getUrl.hostname + "/" + getUrl.pathname.split('/')[1];
  currentURL = currentURL + '/public';
}
else{
  var currentURL = 'http://' + getUrl.hostname;
  currentURL = currentURL;
}

const url = currentURL;

export class WeightInt extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      item: '',
      weightInt: '',
      user: ''
    }
  }

   handleChange = (ev, value) => {
    this.setState({
      weightInt: value,
    });
  };

  saveChange = (ev, value) => {
    console.log(url);
    axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      weight_int: this.state.weightInt
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });

    axios.post(url+'/api/items/logItemAction/'+this.state.item.id, {
      item: this.state.item.id,
      user: this.state.user,
      action: 'weight_int'
    })
    .catch(function (error) {
      console.log(error);
    });

  };


   componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);

    self = this;
    axios.get(url+'/account/current')
    .then(function (response) {
      self.setState({user: response.data.user});
    })
    .catch(function (error) {
      console.log(error);
    });


    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
      this.setState({'item' : response.data});

      var weightInt = response.data.weight_int;
      if(weightInt === null || weightInt === undefined){
        this.setState({'weightInt' : 'N/A'})
      }
      else{
        this.setState({'weightInt' : weightInt})
      }
    });
  }

  render() {
    return (
        <ContentEditable
          tagName="h4"
          className="my-class"
          content={this.state.weightInt}
          editable={true}
          maxLength={9}
          multiLine={false}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyle}
        />

    );
  }
}

export class WeightUnit extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      item: '',
      weightUnit: ''
    }
  }

   handleChange = (ev, value) => {
    this.setState({
      weightUnit: value
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      weight_unit: this.state.weightUnit
  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });
      axios.post(url+'/api/items/logItemAction/'+this.state.item.id, {
      item: this.state.item.id,
      user: 1,
      action: 'weight_unit'
    })
    .catch(function (error) {
      console.log(error);
    });
  };


   componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
      this.setState({'item' : response.data});

      var weightUnit = response.data.weight_unit;
      if(weightUnit === null || weightUnit === undefined){
        this.setState({'weightUnit' : 'N/A'})
      }
      else{
        this.setState({'weightUnit' : weightUnit})
      }
    });
  }

  render() {
    return (
        <ContentEditable
          tagName="h4"
          className="my-class"
          content={this.state.weightUnit}
          editable={true}
          maxLength={100}
          multiLine={false}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyle}
        />

    );
  }
}

export class WeightText extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      item: '',
      weightText: ''
    }
  }

   handleChange = (ev, value) => {
    this.setState({
      weightText: value
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      weight_text: this.state.weightText
  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });
  axios.post(url+'/api/items/logItemAction/'+this.state.item.id, {
      item: this.state.item.id,
      user: 1,
      action: 'weight_text'
    })
    .catch(function (error) {
      console.log(error);
    });
  };


   componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
      this.setState({'item' : response.data});

      var weightText = response.data.weight_text;
      if(weightText === null || weightText === undefined){
        this.setState({'weightText' : 'N/A'})
      }
      else{
        this.setState({'weightText' : weightText})
      }
    });
  }

  render() {
    return (
        <ContentEditable
          tagName="h4"
          className="my-class"
          content={this.state.weightText}
          editable={true}
          maxLength={100}
          multiLine={false}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyle}
        />

    );
  }
}

export class SizeText extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      item: '',
      sizeText: ''
    }
  }

   handleChange = (ev, value) => {
    this.setState({
      sizeText: value
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      size_text: this.state.sizeText
  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });
      axios.post(url+'/api/items/logItemAction/'+this.state.item.id, {
      item: this.state.item.id,
      user: 1,
      action: 'weight_int'
    })
    .catch(function (error) {
      console.log(error);
    });
  };


   componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
      this.setState({'item' : response.data});

      var sizeText = response.data.size_text;
      if(sizeText === null || sizeText === undefined){
        this.setState({'sizeText' : 'N/A'})
      }
      else{
        this.setState({'sizeText' : sizeText})
      }
    });
  }

  render() {
    return (
        <ContentEditable
          tagName="h4"
          className="my-class"
          content={this.state.sizeText}
          editable={true}
          maxLength={1000}
          multiLine={false}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyleTextarea}
        />

    );
  }
}

export class Condition extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      item: '',
      condition: ''
    }
  }

   handleChange = (ev, value) => {
    this.setState({
      condition: value
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      condition: this.state.condition
  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });
      axios.post(url+'/api/items/logItemAction/'+this.state.item.id, {
      item: this.state.item.id,
      user: 1,
      action: 'condition'
    })
    .catch(function (error) {
      console.log(error);
    });
  };


   componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
      this.setState({'item' : response.data});

      var condition = response.data.condition;
      if(condition === null || condition === undefined){
        this.setState({'condition' : 'N/A'})
      }
      else{
        this.setState({'condition' : condition})
      }
    });
  }

  render() {
    return (
        <ContentEditable
          tagName="h4"
          className="my-class"
          content={this.state.condition}
          editable={true}
          maxLength={1000}
          multiLine={true}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyleTextarea}
        />

    );
  }
}

export class Location extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      item: '',
      location: ''
    }
  }

   handleChange = (ev, value) => {
    this.setState({
      location: value
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      location: this.state.location
  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });

      axios.post(url+'/api/items/logItemAction/'+this.state.item.id, {
      item: this.state.item.id,
      user: 1,
      action: 'location'
    })
    .catch(function (error) {
      console.log(error);
    });
  };


   componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
      this.setState({'item' : response.data});

      var location = response.data.location;
      if(location === null || location === undefined){
        this.setState({'location' : 'N/A'})
      }
      else{
        this.setState({'location' : location})
      }
    });
  }

  render() {
    return (
        <ContentEditable
          tagName="h4"
          className="my-class"
          content={this.state.location}
          editable={true}
          maxLength={50}
          multiLine={false}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyle}
        />

    );
  }
}

export class Category extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      item: '',
      category: ''
    }
  }

   handleChange = (ev, value) => {
    this.setState({
      category: value
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      category: this.state.category
  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });

      axios.post(url+'/api/items/logItemAction/'+this.state.item.id, {
      item: this.state.item.id,
      user: 1,
      action: 'category'
    })
    .catch(function (error) {
      console.log(error);
    });
  };


   componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
      this.setState({'item' : response.data});

      var category = response.data.category;
      if(category === null || category === undefined){
        this.setState({'category' : 'N/A'})
      }
      else{
        this.setState({'category' : category})
      }
    });
  }

  render() {
    return (
        <ContentEditable
          tagName="h4"
          className="my-class"
          content={this.state.category}
          editable={true}
          maxLength={420}
          multiLine={false}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyle}
        />

    );
  }
}

export class ShopCode extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      item: '',
      shopCode: ''
    }
  }

   handleChange = (ev, value) => {
    this.setState({
      shopCode: value
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      shop_code: this.state.shop.code
  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });
  };


   componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
      this.setState({'item' : response.data});

      var shopCode = response.data.shop.code;
      if(shopCode === null || shopCode === undefined){
        this.setState({'shopCode' : 'N/A'})
      }
      else{
        this.setState({'shopCode' : shopCode})
      }
    });
  }

  render() {
    return (
        <ContentEditable
          tagName="h4"
          className="my-class"
          content={this.state.shopCode}
          editable={true}
          maxLength={10}
          multiLine={false}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyle}
        />

    );
  }
}

export class ShopName extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      item: '',
      shopName: ''
    }
  }

   handleChange = (ev, value) => {
    this.setState({
      shopName: value
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      shop_name: this.state.shop.name
  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });
  };


   componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
      this.setState({'item' : response.data});

      var shopName = response.data.shop.name;
      if(shopName === null || shopName === undefined){
        this.setState({'shopName' : 'N/A'})
      }
      else{
        this.setState({'shopName' : shopName})
      }
    });
  }

  render() {
    return (
        <ContentEditable
          tagName="h4"
          className="my-class"
          content={this.state.shopName}
          editable={true}
          maxLength={100}
          multiLine={false}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyle}
        />

    );
  }
}

export class GiftAid extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      item: '',
      giftAid: '',
      user: '1',
    }
  }

   handleChange = (ev, value) => {
    this.setState({
      giftAid: value,
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      gift_aid: this.state.giftAid
  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });

         axios.post(url+'/items/getUser/'+this.state.item.id, {

  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });




      axios.post(url+'/api/items/logItemAction/'+this.state.item.id, {
      item: this.state.item.id,
      user: 1,
      action: 'gift_aid'
    })
    .catch(function (error) {
      console.log(error);
    });

  };


   componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
      this.setState({'item' : response.data});

      var giftAid = response.data.gift_aid;
      if(giftAid === null || giftAid === undefined){
        this.setState({'giftAid' : 'N/A'})
      }
      else{
        this.setState({'giftAid' : giftAid})
      }
    });
  }

  render() {
    return (
        <ContentEditable
          tagName="h4"
          className="my-class"
          content={this.state.giftAid}
          editable={true}
          maxLength={15}
          multiLine={false}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyle}
        />

    );
  }
}

export class Price extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      item: '',
      price: ''
    }
  }

   handleChange = (ev, value) => {
    this.setState({
      price: value
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      price: this.state.price
  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });
      axios.post(url+'/api/items/logItemAction/'+this.state.item.id, {
      item: this.state.item.id,
      user: 1,
      action: 'price'
    })
    .catch(function (error) {
      console.log(error);
    });
  };


   componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
      this.setState({'item' : response.data});

      var price = response.data.price;

      if(price === null){
        this.setState({'price' : 'N/A'})
      }
      else{
        this.setState({'price' : price})
      }
    });
  }

  render() {
    return (
        <ContentEditable
          tagName="h4"
          className="my-class"
          content={this.state.price}
          editable={true}
          maxLength={9}
          multiLine={false}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyle}
        />

    );
  }
}

export class EstValue extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      item: '',
      estValue: ''
    }
  }

   handleChange = (ev, value) => {
    this.setState({
      estValue: value
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      est_value: this.state.estValue
  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });
      axios.post(url+'/api/items/logItemAction/'+this.state.item.id, {
      item: this.state.item.id,
      user: 1,
      action: 'est_value'
    })
    .catch(function (error) {
      console.log(error);
    });
  };


   componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
      this.setState({'item' : response.data});

      var estValue = response.data.est_value;
      if(estValue === null){
        this.setState({'estValue' : 'NEV'})
      }
      else{
        this.setState({'estValue' : estValue})
      }
    });
  }

  render() {
    return (
        <ContentEditable
          tagName="h4"
          className="my-class"
          content={this.state.estValue}
          editable={true}
          maxLength={40}
          multiLine={false}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyle}
        />

    );
  }
}


export class Name extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      charCount: ''
    };
  }
  handleChange = (ev, value) => {
    this.setState({
      title: value
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(url+'/api/items/save/'+this.state.item.id, {
      id: this.state.item.id,
      item_name: this.state.title
  })
  .then(function (response) {
    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });

      axios.post(url+'/api/items/logItemAction/'+this.state.item.id, {
      item: this.state.item.id,
      user: 1,
      action: 'name'
    })
    .catch(function (error) {
      console.log(error);
    });

  };

   componentDidMount() {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
  })
.then(response => {
     this.setState({'item' : response.data});
     this.setState({'title' : response.data.item_name});
     this.setState({'charCount' : response.data.item_name.length});

      //console.log(tableData);
});}

  render() {
    return (
        <ContentEditable
          tagName="h1"
          className="my-class"
          content={this.state.title}
          editable={true}
          maxLength={160}
          multiLine={false}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyleTitle}
        />

        // '<span style="float:right;"><small style="color: #f00;">' + (80 - this.state.charCount) +'</small>'
    );
  }
}

export class Desp extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      desp: ''
    };
  }
  handleChange = (ev, value) => {
    this.setState({
      desp: value
    }
    );
  };

  saveChange = (ev, value) => {
        axios.post(`http://localhost/brc/public/api/items/save/`+this.state.item.id, {
      id: this.state.item.id,
      desp: this.state.desp
  })
  .then(function (response) {

    console.log(response);

  })
  .catch(function (error) {
    console.log(error);
  });

      axios.post(url+'/api/items/logItemAction/'+this.state.item.id, {
      item: this.state.item.id,
      user: 1,
      action: 'description'
    })
    .catch(function (error) {
      console.log(error);
    });

  };

   componentDidMount() {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
  })
.then(response => {
     this.setState({'item' : response.data});
     this.setState({'desp' : (response.data.comments !== null) ? response.data.comments : 'No comment'});
});}

  render() {
    return (
        <ContentEditable
          tagName="p"
          className="my-class"
          content={this.state.desp}
          editable={true}
          maxLength={2500}
          multiLine={true}
          onChange={this.handleChange}
          onBlur={this.saveChange}
          style={editableStyleTextarea}
        />

    );
  }
}

export class ValueIndicator extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      valueIndicator: ''
    }
  }

  componentDidMount()
  {
    var itemId = document.location.href.substr(document.location.href.lastIndexOf('/') + 1);
    axios.get(url+'/api/items/' + itemId, {
      })
    .then(response => {
        var price = response.data.price;
        if(response.data.price === null){
          this.setState({'valueIndicator' : ''});
        }
        else if(price >= 60){
          this.setState({'valueIndicator' : '£££'});
        }
        else if(price < 60 && price >= 30){
          this.setState({'valueIndicator' : '££'});
        }
        else if(price < 30 && price >= 10){
          this.setState({'valueIndicator' : '£'});
        }
        else if(price < 10){
          this.setState({'valueIndicator': 'Return'})
        }
        else{
          this.setState({'valueIndicator': ''});
        }
    });
  }

  render() {
    return (
         <h3>{this.state.valueIndicator}</h3>
    );

  }

}
