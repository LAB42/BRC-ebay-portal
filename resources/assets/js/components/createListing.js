import React from 'react';
import axios from 'axios';

const inputStyle = {
  background: '#fff',    
  margin: '10px 10px 10px 0',
  padding: '10px 20px',  
  border: 'solid 1px #666', 
  width: '80%',   
};

const btnStyle = {
  background: '#ddd',
  borderRadius: 3,
  display: 'block',
  border: 'none',
  padding: '10px 20px',
  border: 'solid 1px #ccc',      
};

const thStyle = {
  textAlign: 'center'
}

const tdStyle = {
  padding: '0',
  textAlign: 'center'
}

var getUrl = window.location;
var currentURL = 'http://' + getUrl.host + "/" + getUrl.pathname.split('/')[1];

if (currentURL === 'http://localhost/brc'){
  var currentHost = currentURL+'/public';
}
else{
  var currentHost = currentURL;
}
const url = currentHost;


export class CustomInfo extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      fields: [],
      values: [],
      query: '',
      searched: false,
      result: ''
    };

    this.changeQuery = this.changeQuery.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.chooseCategory = this.chooseCategory.bind(this);
  }

  changeQuery(evt){
    this.setState({'value': evt.target.value});    
  }

  handleClick(evt) {

    evt.preventDefault();
    var query = this.state.value
    this.setState({'searched' : true})    
    axios.post(url+'/api/listing/getCustomInfo', {
      query: query
      })
    .then(response => {
      
      var fields = response.data;      
      var recs = [];
      for(var f in fields){        
        var values = Object.values(response.data[f].values);
        var rec = [fields[f].name,fields[f].values];
        recs.push(rec);
      }
      this.setState({'fields' : recs});
    });

  }

  chooseCategory(evt){
      evt.preventDefault();
      this.setState({'result' : evt.target.value});      

  }

  render() {

      if(this.state.searched !== true) {
      return (
        <div>
        <input id="customInfoNumber"        
       onChange={this.changeQuery} style={inputStyle} />       
      <button onClick={this.handleClick} style={btnStyle}>
         Find
      </button>
      
      </div>
      );
    } else {
      return (
        
       <div>
        <input id="customInfoNumber"        
       onChange={this.changeQuery} style={inputStyle} />       
      <button onClick={this.handleClick} style={btnStyle}>
         Find
      </button>
      
      
      <div className="row">
      {this.state.fields.map(field=>          
           <div className="col-sm-3">  
           <label>{field[0]}</label>

           <input list={'custom_'+field[0]} name={'custom_'+field[0]} />
            <datalist id={'custom_'+field[0]} autocomplete="false">
                {field[1].map((value) =>                  
                    <option value={value.value}>{value.value}</option>
                )}
            </datalist>
            </div>
         )
       }
        </div>
      
      </div>
      )}
    }
  }

export class ShowCategories extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      query: '',
      searched: false,
      result: ''
    };

    // This binding is necessary to make `this` work in the callback
    this.changeQuery = this.changeQuery.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.chooseCategory = this.chooseCategory.bind(this);
  }

  changeQuery(evt){
    this.setState({'value': evt.target.value});    
  }

  handleClick(evt) {

    evt.preventDefault();
    var query = this.state.value
    this.setState({'searched' : true})

    axios.post(url+'/api/listing/getCategory', {
      query: query
      })
    .then(response => {
      
      this.setState({'categories' : response.data});
      
      
    });

  }

  chooseCategory(evt){
      evt.preventDefault();
      this.setState({'result' : evt.target.value});      

  }

  render() {

      if(this.state.searched !== true) {
      return (
        <div>
        <input id="categoryNumber"        
       onChange={this.changeQuery} style={inputStyle} />       
      <button onClick={this.handleClick} style={btnStyle}>
         Find
      </button>
      
      </div>
      );
    } else {
      return (
       <div>
        <input id="category" name="category"       
       onChange={this.changeQuery} style={inputStyle} value={this.state.result}/>       
      <button onClick={this.handleClick} style={btnStyle}>
         Find
      </button>
      
       <table className="table">
            <tr>
              <th style={thStyle}>ID</th>
              <th style={thStyle}>Category</th>
              <th style={thStyle}>Select</th>
            </tr>
      { this.state.categories.map(category=> 
          
          
            <tr>
              <td style={tdStyle}>{category.id}</td>
              <td style={tdStyle}>{category.name}</td>
              <td style={tdStyle}>
                <button value={category.id} onClick={this.chooseCategory}>Choose</button>
                </td>
            </tr>
          

        )}
        </table>
        </div>
      )}
    }
  }