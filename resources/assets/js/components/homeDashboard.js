import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
};

export class Orders extends React.Component{

  render() {
  return (
    <div>
      <Card>
        <CardMedia          
          image=""
          title=""
        />
        <CardContent>
          <Typography gutterBottom variant="headline" component="h3">
            Awaiting Payment
          </Typography>
          <Typography component="h4">
            12
          </Typography>
          <hr />
          <Typography gutterBottom variant="headline" component="h3">
            Awaiting Dispatch
          </Typography>
          <Typography component="h4">
            12
          </Typography>          
          <hr />
          <Typography gutterBottom variant="headline" component="h3">
            Dispatched
          </Typography>
          <Typography component="h4">
            12
          </Typography>
        </CardContent>        
      </Card>
    </div>
  );
}

}

export class Progress extends React.Component{

  render() {
  return (
    <div>
      <Card>
        <CardMedia          
          image=""
          title=""
        />
        <CardContent>
          <Typography gutterBottom variant="headline" component="h3">
            Listed
          </Typography>
          <Typography component="h4">
            184
          </Typography>
          <hr />
          <Typography gutterBottom variant="headline" component="h3">
            Goods-in
          </Typography>
          <Typography component="h4">
            321
          </Typography>
          <hr />
          <Typography gutterBottom variant="headline" component="h3">
            Goods-out
          </Typography>
          <Typography component="h4">
            75
          </Typography>          
        </CardContent>
      </Card>
    </div>
  );
}

}

export class Value extends React.Component{

  render() {
  return (
    <div>
      <Card>
        <CardMedia          
          image=""
          title=""
        />
        <CardContent>
          <Typography gutterBottom variant="headline" component="h3">
            Weekly Listed
          </Typography>
          <Typography component="h4">
            &pound;4652
          </Typography>
          <hr />
          <Typography gutterBottom variant="headline" component="h3">
            Weekly Sold
          </Typography>
          <Typography component="h4">
            &pound;5264
          </Typography>
          <hr />
          <Typography gutterBottom variant="headline" component="h3">
            STR
          </Typography>
          <Typography component="h4">
            42%
          </Typography>
        </CardContent>
      </Card>      
    </div>
  );
}

}

export class Ebay extends React.Component{

  render() {
  return (
    <div>
      <Card>
        <CardMedia          
          image=""
          title=""
        />
        <CardContent>
          <Typography gutterBottom variant="headline" component="h3">
            Messages
          </Typography>
          <Typography component="h4">
            5
          </Typography>
          <hr />
          <Typography gutterBottom variant="headline" component="h3">
            Feedback
          </Typography>
          <Typography component="h4">
            4219
          </Typography>
          <hr />
          <Typography gutterBottom variant="headline" component="h3">
            Total Listed
          </Typography>
          <Typography component="h4">
            6542
          </Typography>
        </CardContent>
      </Card>      
    </div>
  );
}

}






