import React from 'react';
import { render } from 'react-dom';

import {Orders, Progress, Value, Ebay} from './components/homeDashboard';

const OrdersElement = document.querySelector('#orders');
if(OrdersElement){
  render(<Orders />, OrdersElement);
}

const ProgressElement = document.querySelector('#progress');
if(ProgressElement){
  render(<Progress />, ProgressElement);
}

const ValueElement = document.querySelector('#value');
if(ValueElement){
  render(<Value />, ValueElement);
}

const EbayElement = document.querySelector('#ebay');
if(EbayElement){
  render(<Ebay />, EbayElement);
}