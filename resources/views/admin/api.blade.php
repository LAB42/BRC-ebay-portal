@extends('layout.master')
@section('content')
<div class="row">
	<div class="col-sm-12">
		<form action="{{route('setKey')}}" method="POST">
			@csrf
			<label for="key" style="font-size: 1.5em;">Key</label>
			<textarea name="key" id="key" cols="100" rows="5" style="padding: 25px;"></textarea><br><br>
			<input type="submit" value="Save" class="posBtn" style="font-size: 1.2em; text-transform: capitalize;">
		</form>
	</div>
</div>
@endsection