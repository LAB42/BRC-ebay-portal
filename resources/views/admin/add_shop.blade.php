@extends('../layout.master')
@section('content')
<div class="row">
	<div class="col-sm-4">
		@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<form action="{{route('admin-shops-doAdd')}}" method="post">
			@csrf
			<label for="code">Code</label>
			<input type="text" name="code" id="code">

			<label for="name">Name</label>
			<input type="text" name="name" id="name">

			<label for="area">Area</label>
			<select name="areaId" id="areaId">
				@foreach($areas as $area)
				<option value="{{$area->id}}">{{$area->name}}</option>
				@endforeach
			</select>

			<label for="manage">Manager</label>
			<input type="text" name="manager" id="manager">

			<label for="assistant_manager">Assistant Manager</label>
			<input type="text" name="assistant_manager" id="assistant_manager">

			<p>
				<strong>Coming Soon...</strong>
				Other information
				Phone
				Address				
			</p>

			<input type="submit" value="Save" class="posBtn">
		</form>
	</div>
</div>
@endsection