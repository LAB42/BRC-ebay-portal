@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<form action="{{route('admin-packing-update', ['id' => $type->id])}}" method="POST">
			@method('PUT')
			@csrf
			<label for="name">Name</label>
			<input type="text" name="name" id="name" value="{{$type->name}}">

			<label for="length">Length (cm)</label>
			<input type="text" name="length" id="length" value="{{$type->length}}">

			<label for="width">Width (cm)</label>
			<input type="text" name="width" id="width" value="{{$type->width}}">			
			
			<label for="height">Height (cm)</label>
			<input type="text" name="height" id="height" value="{{$type->height}}">

			<label for="stock">Stock</label>
			<input type="text" name="stock" id="stock" value="{{$type->stock}}">
			<br><br><!--replace with css-->
			<input type="submit" value="Edit">
		</form>
	</div>
</div>
@endsection