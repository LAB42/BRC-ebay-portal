@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form action="{{route('admin-user-store')}}" method="POST">
			@csrf
			<label for="name">Name</label>
			<input type="text" name="name" id="name">

			<label for="email">Email</label>
			<input type="text" name="email" id="email">

			<label for="password">Password</label>
			<input type="password" name="password" id="password">

			<label for="password_confirmation">Confirm Password</label>
			<input type="password" name="password_confirmation" id="password_confirmation">

			<label for="color">Colour</label>
			<input type="color" name="color" id="color" style="padding: 0; height: 2.4em; width: 5%;">

			<br><br><!-- Fix in CSS -->
			<input type="submit" value="Add User">
		</form>
	</div>
</div>
@endsection