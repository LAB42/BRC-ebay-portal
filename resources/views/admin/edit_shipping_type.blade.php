@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<form action="{{route('admin-shipping-type-update', ['id' => $type->id])}}" method="POST">
			@method('PUT')
			@csrf
			<label for="name">Name</label>
			<input type="text" name="name" id="name" value="{{$type->name}}">
			<br><br><!-- replace with css-->
			<input type="submit" value="Update">
		</form>
	</div>
</div>
@endsection