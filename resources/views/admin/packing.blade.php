@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-4">
		@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<a href="{{route('admin-packing-create')}}" class="cta">Add packing type</a>
		@if(count($types) === 0)
			<h4>No packing types</h4>
		@else
		<table class="table text-center">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Length</th>
					<th>Width</th>
					<th>Height</th>
					<th>Stock</th>
					<th>Edit</th>
				</tr>			
			</thead>
			<tbody>
				@foreach($types as $type)
				<tr>
					<td>{{$type->id}}</td>
					<td><a href="{{route('admin-packing-show', ['id' => $type->id])}}">{{$type->name}}</a></td>
					<td>{{$type->length}}cm</td>
					<td>{{$type->width}}cm</td>
					<td>{{$type->height}}cm</td>
					<td>{{$type->stock}}</td>
					<td>
						<a href="{{route('admin-packing-edit', ['id' => $type->id])}}" class="filterBtn">Edit</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@endif
	</div>
</div>

@endsection