@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<a href="{{route('key')}}" class="cta">API Key</a>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<div class="card text-center">
			<div class="card-header">
				<h4>Logbook</h4>
			</div>
			<div class="card-body">
				<div class="list-group">
					<div class="list-group-item list-group-action"><a href="">Export logbook</a></div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-3">
		<div class="card text-center">
			<div class="card-header">
				<h4>Inventory</h4>
			</div>
			<div class="card-body">
				<div class="list-group">
					<div class="list-group-item list-group-action"><a href="{{url('admin/inventory/status-settings')}}">Status Settings</a></div>
					<div class="list-group-item list-group-action"><a href="{{route('admin-packing')}}">Manage Packing Types</a></div>
					<div class="list-group-item list-group-action"><a href="{{route('admin-shipping-types')}}">Manage Shipping Types</a></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="card text-center">
			<div class="card-header">
				<h4>Listings</h4>
			</div>
			<div class="card-body"></div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="card text-center">
			<div class="card-header">
				<h4>Orders</h4>
			</div>
			<div class="card-body"></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<div class="card text-center">
			<div class="card-header">
				<h4>Shops</h4>
			</div>
			<div class="card-body">
				<div class="list-group">
					<div class="list-group-item item-group-item-action"><a href="{{route('admin-add-shop')}}">Add Shop</a></div>
					<div class="list-group-item item-group-item-action"><a href="{{route('admin-edit-shops')}}">Edit Shops</a></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="card text-center">
			<div class="card-header">
				<h4>Guides</h4>
			</div>
			<div class="card-body"></div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="card text-center">
			<div class="card-header">
				<h4>Orders</h4>
			</div>
			<div class="card-body">
				<div class="list-group">
					<div class="list-group-item item-group-item-action"><a href="#">Auto-generated reports</a></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="card text-center">
			<div class="card-header">
				<h4>Accounts</h4>
			</div>
			<div class="card-body">
				<div class="list-group">
					<div class="list-group-item item-group-item-action"><a href="#">Ebay Team</a></div>
					<div class="list-group-item item-group-item-action"><a href="#">Shop Access</a></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<div class="card text-center">
			<div class="card-header">
				<h4>Orders</h4>
			</div>
			<div class="card-body">
				<div class="list-group">
					<div class="list-group-item item-group-item-action"><a href="{{route('admin-add-warehouse')}}">Add warehouse section/process</a></div>
					<div class="list-group-item item-group-item-action"><a href="{{route('admin-edit-warehouse')}}">Edit Warehouse</a></div>
					<div class="list-group-item item-group-item-action"><a href="#">Add staff</a></div>
					<div class="list-group-item item-group-item-action"><a href="#">Edit staff</a></div>
				</div>
			</div>
		</div>
	</div>
	@role('owner')
	<div class="col-sm-3">
		<div class="card text-center">
			<div class="card-header">
				<h4>Owner</h4>
			</div>
			<div class="card-body">
				<div class="list-group">
					<div class="list-group-item item-group-item-action"><a href="{{route('admin-users')}}">Users</a></div>
					<div class="list-group-item item-group-item-action"><a href="{{route('admin-roles')}}">Roles</a></div>
					<div class="list-group-item item-group-item-action"><a href="">Permissions</a></div>
					<div class="list-group-item item-group-item-action"><a href="">Abilities</a></div>
					<div class="list-group-item item-group-item-action"><a href="">Reports</a></div>
				</div>
			</div>
		</div>
	</div>
	@endrole
</div>
@endsection
