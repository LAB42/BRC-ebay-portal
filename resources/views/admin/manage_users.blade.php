@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<a href="{{route('admin-user-create')}}" class="cta">Add User</a>
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Color</th>
					<th>Name</th>
					<th>Email</th>
					<th>Roles</th>
					<th>Manage</th>
				</tr>		
			</thead>
			<tbody>
				@foreach($users as $user)
				<tr>
					<td>{{$user->id}}</td>
					<td style="background: {{$user->color}}; width: 5%;"></td>
					<td>{{$user->name}}</td>
					<td>{{$user->email}}</td>
					<td>
						@foreach($user->roles as $role)
							<p>
								<strong>{{$role->display_name}}</strong><br>
								{{$role->description}}
							</p>
						@endforeach
					</td>
					<td>
						<a href="{{route('admin-user-edit', ['id' => $user->id])}}" class="filterBtn">Edit</a>
						<a href="{{route('admin-user-role-edit', ['id' => $user->id])}}" class="filterBtn">Roles</a>						
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection