@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form action="{{route('admin-user-update', ['id' => $user->id])}}" method="POST">
			@method('put')
			@csrf
			<label for="name">Name</label>
			<input type="text" name="name" id="name" value="{{$user->name}}">

			<label for="email">Email</label>
			<input type="text" name="email" id="email" value="{{$user->email}}">

			<label for="color">Colour</label>
			<input type="color" name="color" id="color" value="{{$user->color}}" style="padding: 0; height: 2.4em; width: 5%;">

			<br><br><!-- Fix in CSS -->
			<input type="submit" value="Update">
		</form>
	</div>
</div>
@endsection