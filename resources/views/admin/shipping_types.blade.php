@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-4">
		@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
	<a href="{{route('admin-shipping-type-create')}}" class="cta">Add shipping type</a>
		@if(count($types) === 0)
			<h4>No shipping types</h4>
		@else
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Edit</th>
				</tr>				
			</thead>
			<tbody>
				@foreach($types as $type)
				<tr>
					<td>{{$type->id}}</td>
					<td><a href="{{route('admin-shipping-type-show', ['id' => $type->id])}}">{{$type->name}}</a></td>
					<td><a href="{{route('admin-shipping-type-edit', ['id' => $type->id])}}" class="filterBtn">Edit</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@endif
	</div>
</div>
@endsection