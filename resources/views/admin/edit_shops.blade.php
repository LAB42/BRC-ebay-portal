@extends('../layout.master')
@section('content')
<div class="row">
	<div class="col-sm-8">
		@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
		@endif
		<table class="table">
		<tr class="text-center">
			<th>Code</th>
			<th>Name</th>
			<th>Area</th>
			<th>Shop Manager</th>
			<th>Assistant Manager</th>
			<th style="width: 10%;">Save</th>
		</tr>
		@foreach($shops as $shop)
		<tr class="text-center">
			<form action="{{route('admin-shops-doEdit')}}" method="post">
				@csrf
				<input type="hidden" name="id" value="{{$shop->id}}">

				<td>{{$shop->code}}</td>

				<td spellcheck="false">
					<input type="text" style="text-align: center" name="name" id="name{{$shop->id}}" value="{{$shop->name}}">
				</td>

				<td>		

					<select name="area" id="area{{$shop->id}}">

					@foreach($areas as $area)
						@if($shop->areaId === $area->id):
							<option value="{{$area->id}}" selected>{{$area->name}}</option>
						@else
							<option value="{{$area->id}}">{{$area->name}}</option>
						@endif
					@endforeach
					</select>
				</td>
	
				<td spellcheck="false">
					<input type="text" style="text-align: center" name="sm" id="sm{{$shop->id}}" value="{{$shop->manager}}">
				</td>

				<td spellcheck="false">
					<input type="text" style="text-align: center" name="asm" id="asm{{$shop->id}}" value="{{$shop->assistant_manager}}">
				</td>

				<td>
					<input type="submit" value="Save" class="posBtn">
				</td>
			</form>
		</tr>
		@endforeach
	</table>	
	</div>
</div>
@endsection