@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<a href="{{route('admin-packing-edit', ['id' => $type->id])}}" class="cta">Edit</a>
		<form action="{{route('admin-packing-delete', ['id' => $type->id])}}" method="POST">
			@method('DELETE')
			@csrf
			<input type="submit" value="Delete" class="cta">
		</form>
		<dl>
			<dt>Length</dt>
			<dd>{{$type->length}}cm</dd>

			<dt>Width</dt>
			<dd>{{$type->width}}cm</dd>

			<dt>Height</dt>
			<dd>{{$type->height}}cm</dd>

			<dt>Stock</dt>
			<dd>{{$type->stock}}</dd>
		</dl>
	</div>
</div>
@endsection