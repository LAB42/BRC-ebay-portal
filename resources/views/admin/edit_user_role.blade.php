@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<hr>
		<a href="{{route('admin-user-role-create', ['user' => $user->id])}}" class="cta">Add Role</a>
		@if(count($user->roles) === 0)
		<h2>There are no role assigned to this user.</h2>
		@else
		<hr>
		<table class="table">
			<thead>
				<tr>
					<th>Role Name</th>
					<th>Description</th>
					<th>Remove</th>					
				</tr>
			</thead>
			<tbody>
				@foreach($user->roles as $role)
				<tr>
					<td>{{$role->display_name}}</td>
					<td>{{$role->description}}</td>
					<td>
						<a href="{{route('admin-user-role-delete', ['user' => $user->id, 'role' => $role->id])}}" class="filterBtn">Remove</a>						
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@endif
	</div>
</div>
@endsection