@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">		
		<a href="{{route('admin-role-create')}}" class="cta">Add Role</a>

		@if(count($roles) === 0)
		<h2>No roles to display</h2>
		@else
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Protected Name</th>
					<th>Display Name</th>
					<th>Description</th>
					<th>Manage</th>
				</tr>
				@foreach($roles as $role)
				<tr>
					<td>{{$role->id}}</td>
					<td>{{$role->name}}</td>
					<td>{{$role->display_name}}</td>
					<td>{{$role->description}}</td>
					<td><a href="{{route('admin-role-edit', ['id' => $role->id])}}" class="filterBtn">Edit</a></td>
				</tr>
				@endforeach
			</thead>
		</table>
		@endif
	</div>
</div>
@endsection