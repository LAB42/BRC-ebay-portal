@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-4">
		<form action="{{url('admin/inventory/status-settings/doAddStatus')}}" method="post">
			@csrf
			<label for="statusName">Status Name</label>
			<input type="text" name="statusName" id="statusName" size="25">
			<input type="submit" value="Add Status">
		</form>
		<hr>
		<table class="table">
			<tr>
				<th class="text-center">Status</th>
				<th class="text-center">Update</th>
			</tr>
			@foreach($statusList as $status)
			<tr>
				<td class="text-center">{{$status->name}}</td>
				<td class="text-center"><a href="#">Update</a></td>
			</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection