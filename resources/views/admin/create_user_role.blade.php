@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<hr>
		@if(count($user->roles) === 0)
			<h4>No Exisiting Roles</h4>
		@elseif(count($user->roles) === 1)
			<h4>Existing Role</h4>
		@else
			<h4>Existing Roles</h4>
		@endif		
		<ul style="padding: 0; list-style: none;">
			@foreach($user->roles as $userRole)
				<li><strong>{{$userRole->display_name}}</strong> ({{$userRole->description}})</li>
			@endforeach
		</ul>
		<hr>

		@if(count($roles) - count($currentRoles) === 0)
			<h2>This user has been assigned every role.</h2>
		@else
		<form action="{{route('admin-user-role-store')}}" method="POST">			
			@csrf

			<input type="hidden" name="userID" value="{{$user->id}}">

			<label for="roleID">Role</label>
			<select name="roleID" id="roleID">
				@foreach($roles as $role)					
					@if(!in_array($role->id, $currentRoles))
					<option value="{{$role->id}}">{{$role->display_name}}</option>					
					@endif
				@endforeach				
			</select>
			<br><br><!-- Fix in CSS -->
			<input type="submit" value="Add Role to User">
		</form>
		@endif
	</div>
</div>
@endsection