@extends('../layout.master')
@section('content')
<div class="row">
	<div class="col-sm-12">
		@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
		@endif
		<table class="table">
		<tr class="text-center">
			<th>Section</th>
			<th>Type</th>
			<th>Low</th>
			<th>Medium</th>
			<th>High</th>
			<th>Current</th>
			<th style="width: 20%;">Save</th>
		</tr>
		@foreach($warehouse as $warehouse)
		<tr class="text-center">
			<form action="{{route('admin-warehouse-doEdit')}}" method="post">
				@csrf
				<input type="hidden" name="id" value="{{$warehouse->id}}">			

				<td spellcheck="false">					
					<input type="text" style="text-align: center;"name="section" id="section" value="{{$warehouse->section}}">
				</td>

				<td>		

					<select name="type" id="type">
					
					@foreach($types as $type)
						@if($type === $warehouse->type):
							<option value="{{$type}}" selected>{{ucfirst($type)}}</option>
						@else
							<option value="{{$type}}">{{ucfirst($type)}}</option>
						@endif
					@endforeach
					</select>
				</td>
	
				<td spellcheck="false">
					<input type="text" style="text-align: center;" name="low" id="low" value="{{$warehouse->low}}">
				</td>

				<td spellcheck="false">
					<input type="text" style="text-align: center;" name="medium" id="medium" value="{{$warehouse->medium}}">
				</td>

				<td spellcheck="false">
					<input type="text" style="text-align: center;" name="high" id="high" value="{{$warehouse->high}}">
				</td>

				<td spellcheck="false">
					<input type="text" style="text-align: center;" name="current" id="current" value="{{$warehouse->current}}">
				</td>
	
				<td>
					<input type="submit" value="Save" class="posBtn">
					</form>
					<form action="{{route('admin-warehouse-doDelete')}}" method="POST" style="display: inline;">
						@csrf
						<input type="hidden" name="deleteId" value="{{$warehouse->id}}">
						<input type="submit" value="Delete" class="negBtn">
					</form>
				</td>			
		</tr>
		@endforeach
	</table>	
	</div>
</div>
@endsection