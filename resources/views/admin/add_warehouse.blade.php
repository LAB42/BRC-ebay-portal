@extends('../layout.master')
@section('content')
<div class="row">
	<div class="col-sm-4">
		@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<form action="{{route('admin-warehouse-doAdd')}}" method="post">
			@csrf
			
			<label for="section">Name</label>
			<input type="text" name="section" id="section">

			<label for="type">Type</label>
			<select name="type" id="type">
				<option value="storage">Storage</option>
				<option value="process">Process</option>
			</select>

			<label for="low">Low</label>
			<input type="text" name="low" id="low">

			<label for="medium">Medium</label>
			<input type="text" name="medium" id="medium">

			<label for="high">High</label>
			<input type="text" name="high" id="high">

			<label for="current">Current</label>
			<input type="text" name="current" id="current">

			<input type="submit" value="Save" class="posBtn">
		</form>
	</div>
</div>
@endsection