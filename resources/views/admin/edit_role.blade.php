@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form action="{{route('admin-role-update', ['id' => $role->id])}}" method="POST">			
			@method('put')
			@csrf
			<label for="name">Protected Name</label>
			<input type="text" name="name" id="name" value="{{$role->name}}" disabled><span style="margin-left: 15px; color: #651713;">This field can't be changed.</span>

			<label for="display_name">Display Name</label>
			<input type="text" name="display_name" id="display_name" value="{{$role->display_name}}">

			<label for="description">Description</label>
			<input type="text" name="description" id="description" value="{{$role->description}}">
			<br><br><!-- Fix in CSS -->
			<input type="submit" value="Update Role">
		</form>
	</div>
</div>
@endsection