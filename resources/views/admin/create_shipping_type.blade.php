@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<form action="{{route('admin-shipping-type-store')}}" method="POST">
			@csrf
			<label for="name">Name</label>
			<input type="text" name="name" id="name">
			<br><br><!-- replace with css-->
			<input type="submit" value="Add shipping type">
		</form>
	</div>
</div>
@endsection