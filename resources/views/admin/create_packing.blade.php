@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<form action="{{route('admin-packing-store')}}" method="POST">
			@csrf
			<label for="name">Name</label>
			<input type="text" name="name" id="name">

			<label for="length">Length</label>
			<input type="text" name="length" id="length">

			<label for="width">Width</label>
			<input type="text" name="width" id="width">

			<label for="height">Height</label>
			<input type="text" name="height" id="height">

			<label for="stock">Stock</label>
			<input type="text" name="stock" id="stock">
			<br><br><!-- replace with css -->
			<input type="submit" value="Add packing type">			
		</form>
	</div>
</div>
@endsection
