@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<form action="{{route('admin-shipping-type-delete', ['id' => $type->id])}}" method="POST">
			@method('delete')
			@csrf
			<input type="submit" value="Delete">
		</form>
		<p>No other information.</p>
	</div>
</div>
@endsection