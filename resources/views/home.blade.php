@extends('layout.master')

@section('content')

@role(array('ebaymanagement', 'ebay_staff'))
<div class="row">

	<!-- <div class="col-sm-3">
		<div class="card text-center">
					<div class="card-header">
						<h4>Notice Board</h4>
					</div>
					<div class="card-body">
						<form action="#" method="POST">
							<input type="text" name="officeMessage" id="officeMessage">
							<input type="submit" value="Add">
						</form>
						<hr>
						<ul style="list-style: none; padding:0; font-size: 1.1em;">
							<li>VM out of office Tuesday</li>
							<li>NM at RFP Tuesday to Thursday</li>
							<li></li>
						</ul>
					</div>
				</div>
	</div> -->

	<div class="col-sm-3">
		<div class="card text-center">
			<div class="card-header">
					<h4>Feedback</h4>
			</div>
			<div class="card-body">
				<ul style="padding: 0; list-style: none;">
					<?php $i = 0; ?>
					{{--@foreach($feedback as $fb)--}}
					{{--@if($i < 5)--}}
					<li style="padding-bottom: 15px;"><strong>{{--$fb->type--}}</strong> {{--$fb->text--}}</li>
					<?php $i++; ?>
					{{--@endif--}}
					{{--@endforeach--}}
				</ul>
				</dl>
			</div>
		</div>
	</div>

	<div class="col-sm-6 text-center">
		<div class="jumbotron jumbotron-fluid" style="padding: 15px;">
		  <div class="container">
				<img src="{{asset('img/bears.jpg')}}" alt="" style="border-radius: 60px; pacity: 0.8; width: 420px;">
				@if(Auth::user()->roles->contains('name', 'owner'))
		    	<h2>Get back to work!</h2>
				@else
					<h2>It's going to be ok.</h2>
				@endif
				<h5>Week {{date('W', time())}}</h5>
		  </div>
		</div>
	</div>

	<div class="col-sm-3">
		<div class="card">
			<div class="card-header">
				<h4>Office</h4>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-sm-6">
						<table class="table" style="margin-top: 0;">
							<tbody>
								@foreach($warehouseStaff as $person)
								<tr>
									<td style="border: none;"><img src="{{asset('img/user_icons/'.strtolower($person->initials).'.png')}}" alt="" style="width: 32px;"></td>
									<td style="border: none;">
										{{strtoupper($person->current_task)}}
										@if($person->user === Auth::user()->id)
											@if($person->current_task === 'out-of-office')
											<form action="{{route('warehouse-check-in')}}" method="post">
												@csrf
												<input type="hidden" name="process" value="in">
												<input type="submit" value="Check In" class="filterBtn" style="background: #666; padding: 5px 10px; border-radius: 360px; color: #FFF;">
											</form>
											@else
											<form action="{{route('warehouse-check-in')}}" method="post">
												@csrf
												<input type="hidden" name="process" value="out">
												<input type="submit" value="Check Out" class="filterBtn" style="background: #666; padding: 5px 10px; border-radius: 360px; color: #FFF;">
											</form>
											@endif
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="col-sm-6">
						<dl>
							<dt>Temp</dt>
							<dd>{{$weather->main['temp'] - 272.15}}&deg;C</dd>

							<dt>Humidity</dt>
							<dd>{{$weather->main['humidity']}}%</dd>

							<dt>Overview</dt>
							<dd>
								<img src="http://openweathermap.org/img/w/{{$weather->weather[0]['icon']}}.png" alt="" style="width: 55px; padding: 5px; background: #eee; border-radius: 360px; border: solid 1px #ccc;">
								{{ucwords($weather->weather[0]['description'])}}
							</dd>
						</dl>
						<a href="{{route('forecast')}}" class="cta">Forecast</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="row">

	<div class="col-sm-3" style="margin-bottom: 15px;">
		<div class="card text-center">
			<div class="card-header">
				<h4>Bookmarks</h4>
			</div>
			<div class="card-body"></div>

				<div class="row" style="margin-bottom: 25px;">
					<div class="col-sm-4">
						<a href="#"><img src="{{asset('img/ebay_logo.png')}}" alt="Ebay" style="width: 120px;"></a>
					</div>
					<div class="col-sm-4">
						<a href="#"><img src="{{asset('img/abe_books_logo.png')}}" alt="Ebay" style="width: 64px;"></a>
					</div>
					<div class="col-sm-4">
						<a href="#"><img src="{{asset('img/amazon_logo.png')}}" alt="Ebay" style="width: 64px;"></a>
					</div>
				</div>

				<div class="row" style="margin-bottom: 25px;">
					<div class="col-sm-4">
						<a href="#"><img src="{{asset('img/icons/google_sheets.svg')}}" alt="Ebay" style="width: 64px;"></a>
					</div>
					<div class="col-sm-4">
						<a href="#"><img src="{{asset('img/icons/google_docs.svg')}}" alt="Ebay" style="width: 64px;"></a>
					</div>
					<div class="col-sm-4">
						<a href="#"><img src="{{asset('img/icons/facebook.svg')}}" alt="Ebay" style="width: 64px;"></a>
					</div>
				</div>

				<div class="list-group">
					<div class="list-group-item list-group-item-action"><a href="{{route('shops')}}">Find a shop</a></div>
					<div class="list-group-item list-group-item-action"><a href="{{route('logbook')}}">Process goods-in</a></div>
					<div class="list-group-item list-group-item-action"><a href="{{route('inventory-filter', ['date' => ['year' => date('Y', time()), 'month' => date('m', time())]])}}">This months inventory</a></div>
					<div class="list-group-item list-group-item-action"><a href="{{route('listings')}}">Items being processed</a></div>
					<div class="list-group-item list-group-item-action"><a href="#"><strike>Print postage labels</strike></a></div>
					<div class="list-group-item list-group-item-action"><a href="#"><strike>Print invoices (Royal Mail 48)</strike></a></div>
					<div class="list-group-item list-group-item-action"><a href="#"><strike>Print invoices (High-value / Courier)</strike></a></div>
					<div class="list-group-item list-group-item-action"><a href="https://www.royalmail.com/discounts-payment/credit-account/online-business-account" target="_blank">Submit OBA order</a></div>
					<div class="list-group-item list-group-item-action"><a href="https://www.parcel2go.com/" target='_blank'>Order P2G postage</a></div>
				</div>
		</div>
	</div>

	<div class="col-sm-3" style="margin-bottom: 15px">
			<div class="card text-center">
				<div class="card-header">
					<h4>Customer Service</h4>
				</div>
				<div class="card-body">
					@foreach($customerService as $cs)
						<h5>{{$cs->name}}</h5>
						<span style="font-size: 1.8em">{{$cs->count}}</span>
						@if(!$loop->last)
						<hr>
						@endif
					@endforeach
				</div>
			</div>
	</div>

	<div class="col-sm-3" style="margin-bottom: 15px">
		<div class="card text-center">
			<div class="card-header">
				<h4>Inventory</h4>
			</div>
			<div class="card-body">
				@foreach($inventoryStats as $stats)
				<h5>{{$stats->name}}</h5>
				<span style="font-size: 1.8em;">{{$stats->count}}</span>
				@if(!$loop->last)
				<hr>
				@endif
				@endforeach
			</div>
		</div>
	</div>

	<div class="col-sm-3" style="margin-bottom: 15px;">
		<div class="card text-center">
			<div class="card-header">
				<h4>Warehouse</h4>
			</div>
			<div class="card-body">
					@foreach($todo as $action)
					<h5>{{strtoupper($action->name)}}</h5>


					<span style="font-size: 1.8em; display: inline-block; padding-left: 125px;">
							{{$action->current}}
					</span>
					<div style="margin-left: 42px; display: inline-block;">
						<form action="{{route('warehouse-check-in')}}" method="post">
							@csrf
							<input type="hidden" name="process" value="{{$action->id}}">
							<input type="submit" value="Check in" class="filterBtn" style="padding: 5px 10px;">
						</form>
					</div>
					@if(!$loop->last)
					<hr>
					@endif
					@endforeach
			</div>
		</div>
	</div>

</div>
@endrole


@role('shop')
<div class="row">
	<div class="col-sm-10 text-center" style="margin: 0 auto;">
		<div class="jumbotron jumbotron-fluid" style="padding: 15px;">
		  <div class="container">
				<img src="{{asset('img/ebay_logo.png')}}" alt="">
		    <h1>British Red Cross - Ebay Warehouse</h1>
		    <p class="lead">
		    	Welcome to the new ebay application.
		    </p>
				<div class="row">
					<div class="col-sm-4" style="margin: 0 auto;">
						<div class="list-group-flush">
							<a href="{{route('create-logsheet')}}" class="list-group-item list-group-item-action" style="margin-bottom: 10px;">Submit a logsheet</a>
							<a href="{{route('contact')}}" class="list-group-item list-group-item-action" style="margin-bottom: 10px;">How to contact us?</a>
							<a href="{{route('guides')}}" class="list-group-item list-group-item-action" style="margin-bottom: 10px;">How to use this app</a>
							<a href="{{route('feedback-create')}}" class="list-group-item list-group-item-action" style="margin-bottom: 10px;">App feedback, question or error?</a>
						</div>
					</div>
				</div>
		  </div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6 text-center" style="margin: 0 auto; margin-bottom: 25px;">
		<div class="card">
			<div class="card-header">
				<h4>How to send items</h4>
			</div>
			<div class="card-body">
				<div class="col-sm-12 text-center">
					<div class="row">
						<div class="col-sm-3">
							<img src="{{asset('img/shop.jpg')}}" alt="" style="width: 180px; height: 180px;" class="img-fuild rounded-circle">
						</div>
						<div class="col-sm-3">
							<img src="{{asset('img/ebay_page.png')}}" alt="" style="width: 180px; height: 180px;" class="img-fuild rounded-circle">
						</div>
						<div class="col-sm-3">
								<img src="{{asset('img/aid_work.jpg')}}" alt="" style="width: 180px; height: 180px;" class="img-fuild rounded-circle">
						</div>
						<div class="col-sm-3">
								<img src="{{asset('img/aid_work.jpg')}}" alt="" style="width: 180px; height: 180px;" class="img-fuild rounded-circle">
						</div>
					</div>

					<p style="font-size: 1.6em; margin: 25px 0;">
						Select item
						<span style="color: #43a92c; font-weight: bold;">&gt;</span>
						Research info &amp; value
						<span style="color: #43a92c; font-weight: bold;">&gt;</span>
						One logsheet per box
						<span style="color: #43a92c; font-weight: bold;">&gt;</span>
						Listed
						<span style="color: #43a92c; font-weight: bold;">&gt;</span>
						Sold
						<span style="color: #43a92c; font-weight: bold;">&gt;</span>
						Sale linked to shop
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row text-center">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-header">
				<h4>Team Ebay</h4>
			</div>
			<div class="card-body">
				<div class="row">
						@foreach($warehouseStaff as $person)
						<div class="col-sm-3">
							<img src="{{asset('img/user_icons/'.strtolower($person->initials).'.png')}}" alt="" width="100px;">
							<h3>{{$person->initials}}</h3>
							<p>{{$person->bio}}</p>
						</div>
						@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endrole

<!-- <div class="row">
	<div class="col-sm-12 text-center">

	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-3">
		<div id="value"></div>
	</div>
	<div class="col-sm-3">
		<div id="progress"></div>
	</div>
	<div class="col-sm-3">
		<div id="orders"></div>
	</div>

	<div class="col-sm-3">
		<div id="ebay"></div>
	</div>

</div> -->
<div class="row">
	<div class="col-sm-3">
		<!-- <a href="{{route('create-listing')}}" class="cta">Create Listing</a> -->
	</div>
	<div class="col-sm-3">
		<!-- <a href="{{route('logbook')}}" class="cta">Process goods-in</a>		 -->

	</div>
	<div class="col-sm-3">
		<div class="row">
			<!-- <div class="col-sm-3 text-center">
				<a href="#">
					<img src="{{asset('img/icons/p2g.png')}}" style="height: 32px" alt="Parcel2Go">
					<h5>Parcel2Go</h5>
				</a>
			</div>
			<div class="col-sm-3 text-center">
				<a href="https://www.myhermes.co.uk/">
					<img src="{{asset('img/icons/myhermes.png')}}" style="height: 32px" alt="myHermes">
					<h5>myHermes</h5>
				</a>
			</div>
			<div class="col-sm-3 text-center">
				<a href="#">
					<img src="{{asset('img/icons/royal_mail.png')}}" style="height: 32px" alt="Royal Mail">
					<h5>Quote</h5>
				</a>
			</div>

			<div class="col-sm-3 text-center">
				<a href="#">
					<img src="{{asset('img/icons/royal_mail.png')}}" style="height: 32px" alt="Royal Mail">
					<h5>OBA</h5>
				</a>
			</div> -->
		</div>
	</div>
	<div class="col-sm-3 text-center">
		<!-- <a href="https://ebay.co.uk"><img src="{{asset('img/ebay_logo.png')}}" alt="Ebay"></a> -->
		<!-- <div class="row">
			<div class="col-sm-4">
				<ul style="list-style: none; padding: 0;">
					<li><a href="#">Scheduled</a></li>
					<li><a href="#">Active</a></li>
					<li><a href="#">Sold</a></li>
				</ul>
			</div>

			<div class="col-sm-4">
				<ul style="list-style: none; padding: 0;">
					<li><a href="#">Awaiting Payment</a></li>
					<li><a href="#">Unsold</a></li>
					<li><a href="#">Returns</a></li>
				</ul>
			</div>

			<div class="col-sm-4">
				<ul style="list-style: none; padding: 0;">
					<li><a href="#">Feedback</a></li>
				</ul>
			</div>
		</div> -->
	</div>

</div>
<!--
			<div class="row">
				<div class="col-sm-4">
					<form action="{{route('p2g-quote')}}" method="POST">
						@csrf
						<input type="hidden" name="collectionCountry" value="219">
						<input type="hidden" name="destinationCountry" value="219">
						<label for="weight">Weight</label>
						<input type="text" name="weight" id="weight">

						<label for="length">Length</label>
						<input type="text" name="length" id="length">

						<label for="width">Width</label>
						<input type="text" name="width" id="width">

						<label for="height">Height</label>
						<input type="text" name="height" id="height">

						<br><br>
						<input type="submit" value="Get Quote">
					</form>
				</div>
				<div class="col-sm-4">
					<h5 class="sectionHeader">Awaiting Collection</h5>
					<ul>
						<li>ParcelForce &ndash; 7</li>
						<li>myHermes &ndash; 3</li>
						<li>DPD &ndash; 2</li>
					</ul>
				</div>
				<div class="col-sm-4"></div>
			</div>
 -->

<!-- <div class="row">

	<div class="col-sm-8">
		<img src="{{asset('img/icons/google_docs.svg')}}" style="height: 64px" alt="Google Doc">
		<h5><a href="">Docs</a></h4>

		<img src="{{asset('img/icons/google_sheets.svg')}}" style="height: 64px" alt="Google Sheets">
		<h5><a href="">Sheets</a></h4>

		<a href="#" class="cta">Goods-in</a>
		<a href="#" class="cta">Listing</a>
		<a href="#" class="cta">Shop Information</a>
		<a href="#" class="cta">Find Product</a>
	</div>
</div>
 -->


<!--
<div class="row">
	<div class="col-sm-4">
		<h3 class="sectionHeader">Todo</h3>
		<table class="table">
			<tr>
				<th>Name</th>
				<th width="50">C</th>
				<th width="50">A</th>
			</tr>
			<tr>
				<td>Todo item title</td>
				<td>VM</td>
				<td>DJ</td>
			</tr>
			<tr>
				<td>Todo item title</td>
				<td>VM</td>
				<td>KF</td>
			</tr>
			<tr>
				<td>Todo item title</td>
				<td>VM</td>
				<td>NH</td>
			</tr>
		</table>
		<a href="#" class="cta">View Todo</a>
	</div>

	<div class="col-sm-4">
		<h3 class="sectionHeader">Timetable</h3>

		<table class="table">
			<tr>
				<th>Name</th>
				<th>Type</th>
			</tr>
			<tr>
				<td>Event Name</td>
				<td>Team meeting</td>
			</tr>
			<tr>
				<td>VIP</td>
				<td>Visit</td>
			</tr>
			<tr>
				<td>Driver for S9</td>
				<td>Delivery</td>
			</tr>
		</table>

		<a href="#" class="cta">View Timetable</a>
	</div>

	<div class="col-sm-4">
		<h3 class="sectionHeader">Trending</h3>
			<table class="table">
				<tr>
					<th>Item</th>
					<th width="100">Up / Down</th>
				</tr>
				<tr>
					<td><a href="#">Item Name</a></td>
					<td>UP</td>
				</tr>
				<tr>
					<td><a href="#">Item Name</a></td>
					<td>DOWN</td>
				</tr>
				<tr>
					<td><a href="#">Item Name</a></td>
					<td>UP</td>
				</tr>
			</table>
		<a href="#" class="cta">View Trending</a>
	</div>
</div>
	 -->



<script src="{{ asset('js/homeDashboard.js') }}" defer></script>
<script src="{{ asset('js/item.js') }}" defer></script>
@endsection
