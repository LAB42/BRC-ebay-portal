@extends('layout.master')

@section('content')
<form action="{{url('list/doCreateListing')}}" method="post">
	@csrf
	<input type="hidden" name="itemId" value="{{$template['id']}}">

	<label for="location">Location</label>
	<input type="text" name="location" id="location">

	<label for="sku">SKU</label>
	<input type="text" name="sku" id="sku" size="30"
	value="@if($template != NULL) {{$template['shop']['code'].','.$template['gift_aid'].',W'.date('W', time()).',NH'}} @endif">

	<hr>

	<label for="item_name">Item Name</label>
	<input type="text" name="item_name" id="itemName" size="80" value="@if($template != NULL) {{$template['item_name']}} @endif">



    <label for="price">Price</label>
    <input type="text" name="price" id="price" size="10" value="@if($template != NULL) {{'£'.$template['price']}} @endif">

    <div class="row">
        <div class="col-sm-6">
            <label for="category">Category</label>
            <div id="categories"></div>
        </div>
    </div>

    <label for="customInfo">Custom Item Specifics</label>
    <div id="customInfo"></div>

	<label for="description">Description</label>
    <div contenteditable="true" style="background: #fff; border:solid 1px #666; padding: 15px;">
        @if($template != NULL)
        {{$template['comments']}}<br><br>
        Weight: {{$template['weight_int'].$template['weight_unit']}}<br><br>
        Size: {{$template['size_text']}}<br><br>
        Condition: {{$template['condition']}}<br><br>
        @endif
    </div>
    <br>
    <h5 class="sectionHeader">Shipping</h5>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus dolore, nihil corporis qui culpa molestiae, officiis quis ex doloribus dolorum, fugiat, ut nesciunt dolorem voluptatibus accusamus illo tenetur? Ea, excepturi?</p>

    <div class="row">
        <div class="col-sm-3">
            <label for="shippingMethod">Shipping Method</label>
            <select name="shippingMethod" id="shipping">
                <option value="royal_mail_48">Royal Mail 48</option>
                <option value="other_3_day">Other 3 Day</option>
                <option value="free_shipping">Free Shipping</option>
            </select>

            <label for="shippingCost">Shipping Cost</label>
            <input type="text" name="shippingCost" id="shippingCost">
        </div>


        <div class="col-sm-6" style="margin-top: 10px; border: solid 1px #ddd; padding: 10px 20px;">
            <h6>Shipping Quote</h6>
            <a href="https://www.parcel2go.com/quick-quote" target="_blank" class="cta" style="background: #ddd; color: #333;">Parcel2Go</a>
            <a href="https://www.royalmail.com/price-finder/" target="_blank" class="cta" style="background: #ddd; color: #333;">Royal Mail</a>
            <a href="https://www.myhermes.co.uk/send-a-parcel.html#/price" target="_blank" class="cta" style="background: #ddd; color: #333;">myHermes</a>

            <div class="row">
                <div class="col-sm-4">
                    <p>Weight: {{($template['weight_int'] !== NULL && $template['weight_unit'] !== NULL) ? $template['weight_int'].$template['weight_unit'] : 'N/A'}}</p>
                </div>
                <div class="col-sm-6">
                    <p>Size info: {{($template['size_text'] !== NULL) ? $template['size_text'] : 'N/A'}}</p>
                </div>
            </div>

        </div>
    </div>



    <br><br>
    <h5 class="sectionHeader">Notes <small>(not made public)</small></h5>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste quas, sunt fuga magnam ipsam, alias accusantium enim id totam, dolores nihil, explicabo eaque illo voluptate tenetur deleniti. Ipsam, dolor, at!</p>

    <input type="submit" value="Create Listing">

</form>
<script src="{{ asset('js/createListing.js') }}" defer></script>
@endsection
