@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>	
		@endif
		<form action="{{url('logsheet/doGenerateLogsheet')}}" method="post">
			@csrf
			<div class="row">
				<div class="col-sm-2">
					<label for="shopCode">Shop Code</label>
					<input type="text" name="shopCode" id="shopCode" size="15">
					<br><br><!-- replace with CSS -->
					<a href="{{route('shops')}}" target="_blank" class="cta">Find shop code</a>
				</div>
				<div class="col-sm-2">					
					<label for="deliveryDate">Due for delivery</label>
					<input type="date" name="deliveryDate" id="deliveryDate">				
				</div>
			</div>
			<hr>
			<input type="hidden" name="numberOfItems" value="10">
			<table class="table">
				<tr>
					<th class="text-center">#</th>
					<th>Name</th>
					<th>Info</th>
					<th>Gift Aid</th>
					<th class="text-center">Value</th>					
				</tr>
			
			<?php				
			for ($i=1; $i <= 10; $i++) { 
				?>
				<tr>
					<td style="border-top: none;">
						<input type="text" name="id{{$i}}" id="id{{$i}}" size="3" style="text-align: center;" value="{{$i}}" disabled>
					</td>	
					<td style="border-top: none;">
						<input type="text" name="name{{$i}}" id="name{{$i}}" size="50">						
					</td>
					<td style="border-top: none;">
						<input type="text" name="info{{$i}}" id="info{{$i}}" size="50">						
					</td>
					<td style="border-top: none;">
						<input type="text" name="giftAid{{$i}}" id="giftAid{{$i}}" size="6">						
					</td>
					<td style="border-top: none;">
						<span style="font-weight: bold; font-size: 1.4em; display: inline;">&pound;</span>
						<input type="text" name="value{{$i}}" id="value{{$i}}" size="5" style="text-align: center;">
					</td>
				</tr>
			<?php
			}
			?>
			</table>
			<hr>
			<input type="submit" class="btn" value="Add Logsheet">
		</form>
	</div>
</div>
@endsection