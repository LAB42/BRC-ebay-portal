@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		
		@if(count($areas) === 0)
		<h2>No areas to display</h2>
		@else
		<table class="table text-center">
			<thead>
				<tr>
					<th>Color</th>
					<th>Name</th>															
					<th>Number of Shops</th>
					<th>ARM</th>
					<th>Manage</th>
				</tr>
			</thead>

			<tbody>
				@foreach($areas as $area)
				<tr>	
					<td style="width: 2%; background: {{$area->color}};"></td>				
					<td style="width: 15%;">
						<a href="{{route('area-show', ['id' => $area->id])}}">{{$area->name}}</a>					
					</td>					
					<td style="width: 10%;">
						<p>{{count($area->shop)}}</p>						
					</td>
					<td>
						<p style="margin: 0;">
							@if($area->area_manager)
							{{$area->area_manager}}
							@else
							No ARM information
							@endif

							@if($area->area_manager_email)							
							<br>
							<a href="#">
								<img src="{{asset('img/icons/envelope.svg')}}" alt="" style="width: 32px;">								
							</a>
							@endif
						</p>
					</td>					
					<td>
						<a href="{{route('area-edit', ['id' => $area->id])}}" class="filterBtn">Update</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@endif



	</div>
</div>
@endsection