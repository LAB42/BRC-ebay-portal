@extends('layout/master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<h4 class="sectionHeader">Goods-in</h4>
		<ol style="font-size: 1.2em;">
			<li><a href="{{url('logsheet/generate')}}">Generate logsheet</a> - (includes finding value for each item)</li>
				<ol type="a">
					<li>Create return card</li>
					<li>Create BIN card</li>
				</ol>
		</ol>
		<hr>
		<h4 class="sectionHeader">Weight / Size / Condition <small>(W/S/C)</small></h4>
		<p>Add the following information for each item:</p>
		<ul>
			<li>Weight</li>
			<li>Size</li>
			<li>Condition</li>
		</ul>		
		<hr>
		<h4 class="sectionHeader">Photos</h4>
	</div>
</div>
@endsection