@extends('layout.master')

@section('content')
<hr>
<div class="row">
  <div class="col-sm-3">
    <div class="card card-body">
        <h4 class="sectionHeader">Warehouse</h4>
        <form action="#" method="POST">
          @csrf
          <label for="status">Status</label>
          <select name="status" id="status">
            @foreach($statuses as $status)
            <option value="{{$status->id}}">{{$status->name}}</option>
            @endforeach
          </select>
          <br><br><!-- FIX IN CSS -->
          <input type="submit" class="btn" value="Generate report">
        </form>
      </div>
    </div>

    <div class="col-sm-3">
      <div class="card card-body">
        <h4 class="sectionHeader">Shops</h4>

        <div class="row">
          <div class="col-sm-6">
            <form action="{{route('generate-shop-report')}}" method="POST">
              @csrf
              <label for="code">Code</label>
              <input type="text" name="code" id="code">

              <label for="status">Status</label>
              <select name="status" id="status">
                @foreach($statuses as $status)
                <option value="{{$status->id}}">{{$status->name}}</option>
                @endforeach
              </select>
              <br><br><!-- FIX IN CSS -->
              <input type="submit" class="btn" value="Generate report">
            </form>
          </div>
          <div class="col-sm-6">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A non quis vero fuga libero tempora quod dolores quae est, amet quam reiciendis tenetur voluptate et, odit ipsa, provident soluta nulla.</p>
          </div>
        </div>        
      </div>
  </div>

  <div class="col-sm-3">
      <div class="card card-body">      
        <h4 class="sectionHeader">Area</h4>
        <form action="#" method="POST">
          <label for="areaId">Name</label>
          <select name="areaId" id="areaId">
            @foreach($areas as $area)
            <option value="{{$area->id}}">{{$area->name}}</option>
            @endforeach
          </select>

          <label for="type">Status</label>
          <select name="type" id="type">
            <option value="pending">Pending</option>
            <option value="sold">Unsold</option>
            <option value="sold">Sold</option>
          </select>
          <br><br><!-- FIX IN CSS -->
          <input type="submit" class="btn" value="Generate report">
        </form>        
      </div>
  </div>

  <div class="col-sm-3 card card-body">
    <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
          Modal
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum vel explicabo, ea, vitae ab laudantium suscipit repudiandae nihil maiores molestias et vero consequatur maxime minus dolorum, ad aliquid corporis rerum!</span><span>Eveniet voluptates provident quasi recusandae neque magnam, doloremque dolor illum id cumque odit repudiandae sunt ratione, sapiente porro impedit sequi, molestias excepturi. Eum obcaecati commodi veniam voluptatem consectetur fugiat enim?</span></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
          </div>
        </div>
  </div>
</div>
@endsection
