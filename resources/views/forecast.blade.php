@extends('layout.master')

@section('content')
<div class="row">
  <div class="col-sm-5">
    <table class="table text-center" style="font-size: 1.2em;">
      <tbody>
        @foreach($weather->list as $row)
        @if(date('l', $row['dt']) !== 'Saturday' && date('l', $row['dt']) !== 'Sunday')
        @if(date('gA', $row['dt']) === '9AM' || date('gA', $row['dt']) === '12PM' || date('gA', $row['dt']) === '3PM' || date('gA', $row['dt']) === '6PM')
        @if(date('gA', $row['dt']) === '9AM')
        <tr>
          <td style="border-top: none; display: block; margin-top: 25px; background: #ddd; border-bottom: solid 1px #666;">
            <strong>{{date('l dS', $row['dt'])}}</strong>
          </td>
          <td style="border-top: none; border-bottom: solid 1px #666;"></td>
          <td style="border-top: none; border-bottom: solid 1px #666;"></td>
          <td style="border-top: none; border-bottom: solid 1px #666;"></td>
          <td style="border-top: none; border-bottom: solid 1px #666;"></td>
        </tr>
        @endif
        <tr
        style="
        {!!(date('j', $row['dt']) == date('j', time())) ? 'background: #e6e6e6;' : NULL!!}
        ">
          <td style="width: 10%;">{{date('gA', $row['dt'])}}</td>
          <td style="width: 10%;">
            <img src="http://openweathermap.org/img/w/{{$row['weather'][0]['icon']}}.png" alt="" style="width: 84px; padding: 10px; background: #eee; border-radius: 360px; border: solid 1px #ccc;">
          </td>
          <td style="width:20%;">
            {{ucwords($row['weather'][0]['description'])}}
          </td>
          <td style="width: 10%;">{{round($row['main']['temp'] - 272.15)}}&deg;C</td>
          <td style="width: 10%;">{{$row['main']['humidity']}}%</td>
        </tr>
        @endif
        @endif
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
