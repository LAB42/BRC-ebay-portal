@extends('layout.master')
@section('content')
<div class="row">
	<div class="col-sm-12">
		<a href="{{url('logbooks')}}" class="cta" style="font-weight: bold;">Logbooks</a>
		<hr>						
		@foreach($areasList as $area)
			<ul style="list-style: none; padding: 0; display: inline-block;">
				<li><a href="{{url('/inventory/inventoryByArea/'.$area->id)}}" class="filterBtn" style="font-weight: bold; color: #FFF; background: {{$colors[strtolower(str_replace(' ', '', $area->name))]}};">
					<?php
					$a = str_replace('south', 'south ',$area->name);
					$b = str_replace('north', 'north ',$a);
					echo strtoupper($b);
					?>
				</a></li>
			</ul>
		@endforeach				
		<h4>{{(isset($items) ? 'Results - '. count($items) : 'No results')}}</h4>
		<hr>
		@if(count($items) > 0)
		<table class="table" style="text-align: center;">
			<tr>
				<th>Date</th>
				<th>Area</th>
				<th>Shop Code</th>
				<th>Shop Name</th>	
				<th>Name</th>
				<th>Status</th>
			</tr>			
			@foreach($items as $item)		
			<tr>	
				<td>{{date('d/m/y', strtotime($item->date_received))}}</td>
				<?php 
				$area = strtolower($item->shop->area);
				$ac = preg_replace('$ $','', $area);								
				?>
				@if(!in_array($item->shop->area, $areas))
				<td style="background: {{$colors[$ac]}}; color: #fff; font-weight: bold" width="85">
					{{$item->shop->area}}
				@else
				<td>
					N/A
				@endif
				</td>

				<td>{{$item->shop->code}}</td>
				<td>{{$item->shop->name}}</td>
				<td><a href="{{url('/item/'.$item->id)}}">{{$item->item_name}}</a></td>
				<td
				@if($item->status == 'LISTED')
				style="background: {{$colors['listed']}}; color: #fff;
				@elseif($item->status == 'NIS')
				style="background: {{$colors['nis']}}; color: #fff;				
				@elseif($item->status == 'A')
				style="background: {{$colors['a']}}; color: #fff;
				@elseif($item->status == 'PAT')
				style="background: {{$colors['pat']}}; color: #fff;			
				@elseif($item->status == 'PENDING')
        		style="background: {{$colors['pending']}}; color: #fff;     
        		@endif
				vertical-align: middle; font-weight: bold;">
				{{$item->inventoryStatus->name}}</td>				
			</tr>
			@endforeach		
			
		</table>
		@endif
	</div>
</div>

@endsection