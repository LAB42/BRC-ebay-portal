@extends('layout.master')
@section('printStyle')
<link rel="stylesheet" href="{{asset('css/binCard.css')}}" media="print">
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<a href="{{route('pdfBinCard', ['id' => $logsheet->id])}}" class="cta">Print</a>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<?php $formattedArea = preg_replace('$ $', '',strtolower($logsheet->shop->area->name)); ?>
		<div class="area" style="display: block; padding: 25px 0; font-size: 2.4em; font-weight: normal; background: {{$colors[$formattedArea]}}; color: #FFF;">
			@if(!empty($logsheet->shop->area->name))
				{{$logsheet->shop->area->name}}
			@else
				No Area
			@endif			
		</div>
	</div>
	<div class="col-sm-6" style="padding-top: 35px; text-align: center;">
		<h4 style="font-size: 2em;">{{$logsheet->shop->name}}</h4>

		<img src="data:image/png;base64,{{DNS2D::getBarcodePNG($logsheet->id, "QRCODE")}}" alt="barcode" />		
	</div>
	<div class="col-sm-3" style="padding-top: 35px;">		
		<h4 style="font-size: 2em; text-align: center;">{{$logsheet->shop->code}}</h4><br>
	</div>
</div>
<div class="row">
	<div class="col-sm-12" style="text-align: center;">
		<h3 style="font-weight: bold;">{{$logsheet->id}}-NH-200418-{{rand(1000,4999)}}</h3>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">		
		<hr>
		<table class="table">
			<tr>
				<th class="text-center">#</th>
				<th class="text-center">Name</th>
				<th class="text-center">Info</th>
				<th class="text-center">Value</th>			
			</tr>
			@foreach(json_decode($logsheet->items) as $item)
			@if($item->value >= 10)
			<tr>
				<td class="text-center">{{$item->id}}</td>
				<td class="text-center">{{$item->name}}</td>
				<td class="text-center">{{$item->info}}</td>
				<td class="text-center">&pound;{{$item->value}}</td>
			</tr>
			@endif
			@endforeach
		</table>
	</div>
</div>
@endsection