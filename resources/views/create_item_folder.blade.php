@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		
		<form action="{{route('item-folder-store')}}" method="POST">
			@csrf
			
			<label for="name">Name</label>
			<input type="text" name="name" id="name" size="25">

			<label for="description">Description</label>
			<input type="text" name="description" id="description" size="50">

			<label for="color">Colour</label>
			<input type="color" name="color" id="color" style="padding: 0; height: 2.4em; width: 5%;">
			
			<br><br><!-- Fix in CSS -->
			<input type="submit" value="Create Folder">

		</form>
	</div>
</div>
@endsection