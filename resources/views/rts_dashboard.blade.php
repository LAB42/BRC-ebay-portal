@extends('layout/master')

@section('content')

<div class="row">
	<div class="col-sm-6">
		<a href="{{route('generate-rts-label')}}" class="cta">Generate RTS Label</a>
		<a href="#" class="cta">Add Consignment</a>
		<a href="#" class="cta">Filter by Area</a>
		<a href="#" class="cta">Filter by Shop</a>
	</div>	
</div>
<div class="row">
	<div class="col-sm-12">
		<table class="table text-center">
			<thead>
				<tr>
					<th>ID</th>
					<th>Area</th>
					<th>Shop</th>
					<th>Status</th>
					<th><!-- Blank --></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>##</td>
					<td>South 3</td>
					<td>Ammanford</td>
					<td>Waiting</td>
					<td><a href="#" class="filterBtn">Dispatched</a></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
@endsection