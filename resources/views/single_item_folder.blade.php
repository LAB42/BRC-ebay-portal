@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-8">
		@if(count($items) === 0)
			<h2>No items to display</h2>
		@else
		<table class="table text-center">
			<thead>
				<tr>
					<th>ID</th>
					<th>Shop Code</th>
					<th>Shop Name</th>
					<th>Item Name</a></th>
					<th>Status</th>
				</tr>

				@foreach($items as $item)
					<tr>
						<td style="width: 5%;">{{$item->id}}</td>
						<td style="width: 10%;"><a href="{{route('single-shop', ['id' => $item->shop->id])}}">{{$item->shop->code}}</a></td>
						<td style="width: 20%;"><a href="{{route('single-shop', ['id' => $item->shop->id])}}">{{$item->shop->name}}</a></td>
						<td style="width: 50%;"><a href="{{route('item', ['id' => $item->id])}}">{{$item->item_name}}</a></td>
						<td style="width: 15%;">{{$item->inventoryStatus->name}}</td>
					</tr>
				@endforeach
			</thead>
		</table>
		@endif
	</div>
</div>
@endsection