@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<form action="#" method="post">

			
			<label for="crate">Crate</label>
			<input type="text" name="crate" id="crate" size="40">

			<hr>
			
			<h4 class="sectionHeader">Shop</h4>
			<div class="row">
				<div class="col-sm-6">
					<label for="shopName">Shop Name</label>
					<input type="text" name="shopName" id="shopName" size="60">		
				</div>
				<div class="col-sm-3">
					<label for="shopCode">Shop Code</label>
					<input type="text" name="shopCode" id="shopCode" size="20">		
				</div>
				<div class="col-sm-3">							
					<label for="shopArea">Shop Area</label>
					<input type="text" name="shopArea" id="shopArea" size="20">					
				</div>
			</div>
			
			


			<hr>

			<h4 class="sectionHeader">Item</h4>
			<label for="itemName">Item Name</label>
			<input type="text" name="itemName" id="itemName" size="95">

			<label for="giftAid">Gift Aid</label>
			<input type="text" name="giftAid" id="giftAid" size="15">

			<div class="row">
				<div class="col-sm-2">
					<label for="estValue">Estimated Value</label>
					<input type="text" name="estValue" id="estValue" size="15">		
				</div>
				<div class="col-sm-3">
					<label for="valueInt">Value Int</label>
					<input type="text" name="valueInt" id="valueInt" size="10">		
				</div>
			</div>
			
			<label for="comments">Description / Comments</label>
			<textarea name="comments" id="comments" cols="45" rows="3"></textarea>
			
			<hr>

			<h5 class="sectionHeader">Weight / Size / Condition</h5>
			<label for="weightText">Weight Text</label>
			<textarea name="weightText" id="weightText" cols="50" rows="2"></textarea>

			<label for="weightInt">Weight Number</label>
			<input type="text" name="weightInt" id="weightInt" size="10">

			<label for="weightUnit">Weight Unit</label>
			<select name="weightUnit" id="weightUnit">
				<option value="g">g</option>
				<option value="kg">kg</option>
			</select>

			<label for="sizeText">Size</label>
			<textarea name="sizeText" id="sizeText" cols="50" rows="3"></textarea>
			
			<label for="condition">Condition</label>
			<textarea name="condition" id="condition" cols="50" rows="3"></textarea>
	
			<hr>
			<h5 class="sectionHeader">Photos</h5>
			<label for="photos">Upload</label>
			<input type="file" name="photos" id="photos">


		</form>
	</div>
</div>
@endsection