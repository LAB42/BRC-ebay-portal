@extends('layout.master')

@section('content')
<div class="row">
  <div class="col-sm-12">
    <h4>{{$item->item_name}}</h4>
    <p>{{($item->condition !== NULL) ? $item->condition : 'No condition information available.'}}</p>
    <hr>
    <h1>{{count($item->photos)}} item photos</h1>
  </div>
</div>
<div class="row">
  @foreach($item->photos as $photo)
    <div class="col-sm-2">
      <div class="card">
        <div class="card-header" style="padding: 15px;">
          <p style="margin-bottom: 0;">
            <span style="background: #5a98c0; color: #fff; padding: 5px 11px; margin-right: 25px; border-radius: 360px; font-weight: bold; font-size: 1.2em;">{{$loop->iteration}}</span>
            {{$photo->filename}}
          </p>
        </div>
        <div class="card-body">
          <img src="{{asset('item_photos/'.$photo->filename)}}" alt="Image not found" style="width: 100%;">
        </div>
        <div class="card-footer text-center" style="padding: 10px 15px;">
          <p style="margin-bottom: 0;">
            <small>Modified by {{Auth::user($photo->added_by)->name}} on {{date('D d/m/y \a\t h:m', strtotime($photo->updated_at))}}</small>
          </p>
        </div>
      </div>
    </div>
  @endforeach
</div>
@endsection
