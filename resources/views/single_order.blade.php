@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-4">		
		<div style="
		cursor: pointer; color: #FFF; vertical-align: middle; text-align: center; font-weight: bold; padding: 5px; width: 30%;background: {{$status->background}};">
			{{$status->name}}
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-12">
				<h1>&pound;{{$total}}</h1>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-6">
				<h3>{{ucfirst($shippingType)}}</h3>
				<h4>
					{{$order->carrier_code}} 
					|
					{{$order->service_code}}
				</h4>				
				<div class="row">
					<div class="col-sm-6">
						<p>
							<strong>Min Delivery</strong><br>
							{{$order->min_estimated_delivery_date}}
						</p>
						<p>
							<strong>Paid</strong><br>
							<!-- &pound;{{$shippingPaid}} -->
							&pound;2.45<br>
							<strong>Difference</strong><br>
							&pound;{{sprintf("%0.2f", $shippingActual - 2.45)}}						
						</p>
					</div>
					<div class="col-sm-6">
						<p>
							<strong>Max Delivery</strong><br>
							{{$order->max_estimated_delivery_date}}
						</p>
						<p>
							<strong>Actual</strong><br>							
						</p>
						<form action="#" method="post">
								<input type="text" name="actualPostage" id="actualPostage" value="&pound;{{$shippingActual}}" style="width: 100%; margin-top: -10px;" >
								<input type="submit" value="Update" style="width: 100%; background: #eee; color: #333; cursor: pointer;">
						</form>						
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<a href="#" class="cta" style="font-size: 0.8em;">Print Label</a><br>
				<a href="#" class="cta" style="font-size: 0.8em;">Book Courier</a><br>
				<form action="{{route('p2g-quote')}}" method="POST">
					@csrf
					<input type="hidden" name="collectionCountry" value="219">
					<input type="hidden" name="destinationCountry" value="219">
					<label for="weight">Weight</label>
					<input type="text" name="weight" id="weight" size="10">

					<label for="length">Length</label>
					<input type="text" name="length" id="length" size="10">

					<label for="width">Width</label>
					<input type="text" name="width" id="width" size="10">

					<label for="height">Height</label>
					<input type="text" name="height" id="height" size="10">
					
					<br><br>
					<input type="submit" value="Get Quote">
				</form>
			</div>
		</div>		
		<hr>		
	</div>
	<div class="col-sm-4">		
		<dl>
			<dt>Username</dt>
			<dd>{{$order->buyer}}</dd>

			<dt>Order date</dt>
			<dd>{{$order->creation_date}}</dd>

			<dt>Name</dt>
			<dd>{{$order->full_name}}</dd>

			<dt>Email</dt>
			<dd>{{$order->buyer_email}}</dd>

			<dt>Phone</dt>
			<dd>{{$order->buyer_phone}}</dd>
		</dl>
	</div>	
	<div class="col-sm-4">
		<h4 class="sectionHeader">Payment Address</h4>
		<p>
			{{$order->ship_addressLine1}}<br>
			{{$order->ship_addressLine2}}<br>
			{{$order->ship_city}}<br>
			{{($order->ship_state_or_province !== false) ? $order->ship_state_or_province : ''}}<br>
			{{$order->ship_postal_code}}<br>
			{{$order->ship_country}} ({{$order->ship_country_code}})
		</p>

		<h4 class="sectionHeader">Shipping Address</h4>
		<p>
			{{$order->ship_addressLine1}}<br>
			{{$order->ship_addressLine2}}<br>
			{{$order->ship_city}}<br>
			{{($order->ship_state_or_province !== false) ? $order->ship_state_or_province : ''}}<br>
			{{$order->ship_postal_code}}<br>
			{{$order->ship_country}} ({{$order->ship_country_code}})
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<table class="table">
			<tr>
				<th class="text-center">ID</th>
				<th class="text-center">Name</th>
				<th class="text-center">Location</th>
				<th class="text-center">QTY</th>			
				<th class="text-center">Price</th>
				<th class="text-center">Picture</th>
			</tr>
		</table>	
	</div>
</div>
@endsection