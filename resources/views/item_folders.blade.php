@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-6">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		<a href="{{route('item-folder-create')}}" class="cta">Add Folder</a>

		@if(count($folders) === 0)
			<h2>No folders</h2>
		@else
		<table class="table text-center">
			<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Colour</th>					
					<th>Manage</th>
				</tr>
			</thead>
			<tbody>
				@foreach($folders as $folder)
				<tr>
					<td><a href="{{route('item-folder-show', ['id' => $folder->id])}}">{{$folder->name}}</a></td>
					<td>{{$folder->description}}</td>
					<td style="background: {{$folder->color}}; color:#FFF;">{{strtoupper($folder->color)}}</td>
					<td>
						<div class="row text-center">
							<div class="col-sm-6">
								<a href="{{route('item-folder-edit', ['id' => $folder->id])}}" class="filterBtn">Edit</a>
							</div>
							<div class="col-sm-6">
								<form action="{{route('item-folder-delete', ['id' => $folder->id])}}" method="POST">
									@method('DELETE')
									@csrf
									<input type="submit" value="Delete">
								</form>
								
							</div>
						</div>						
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@endif
	</div>
</div>
@endsection