@extends('layout.master')
@section('message')
<!-- <h4 class="pageMessage">
This is a test user message
<span style="padding-left: 50px; cursor: pointer;">x</span>
</h4> -->
@endsection

@section('content')
<style>
.card-header{
	background: {{Auth::user()->color}};
	color: #FFF;
	font-weight: bold;
	font-size: 1.2em;
}
</style>
<div class="row">
	<div class="col-sm-2 text-center">
		<figure class="figure">
		  <img src="{{asset('img/placeholder.png')}}" class="figure-img img-fluid rounded" alt="User profile picture">
		  <figcaption class="figure-caption text-center">
				<span style="font-size: 2em; line-height: 1.5em;">{{Auth::user()->name}}</span>
				<span style="font-size: 1.2em; line-height: 1em; display: block;">{{Auth::user()->email}}</span>
			</figcaption>
		</figure>

		<div class="list-group">
			<li class="list-group-item"><a href="{{route('manage-account')}}">Manage Account</a></li>
			@role(['owner', 'ebaymanagement'])
			<li class="list-group-item"><a href="{{route('admin')}}">Admin Dashboard</a></li>
			@endrole
		</div>
	</div>

	<div class="col-sm-4">
		<div class="card" style="margin-bottom: 15px;">
			<div class="card-header">
				Information
			</div>
			<div class="card-body">
				<dl>
					<dt>Email</dt>
					<dd>{{Auth::user()->email}}</dd>
				</dl>
			</div>
		</div>

		<div class="card">
			<div class="card-header">
				Role(s)
			</div>
			<div class="card-body">
				<div class="row">
					@foreach(Auth::user()->roles as $role)
					<div class="col-sm-6">
						<dl>
							<dt>{{$role->display_name}}</dt>
							<dd>{{$role->description}}</dd>
						</dl>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
<hr>
<!-- <div class="row">
	<div class="col-sm-12">
		<a href="{{route('picklist')}}" class="cta">Generate Picklist</a>
	</div>
</div>
<hr> -->
<!-- <div class="row">
	<div class="col-sm-12">
		<h3 class="sectionHeader">Performance</h3>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<h4>Week</h4>
		<dl>
			<dt style="color: #333;">Listings</dt>
			<dd>[###]</dd>

			<dt style="color: #333;">Value</dt>
			<dd>[###]</dd>

			<dt style="color: #333;">ATV</dt>
			<dd>[###]</dd>
		</dl>
	</div>
	<div class="col-sm-4">
		<h4>Month</h4>
		<dl>
			<dt style="color: #333;">Listings</dt>
			<dd>[###]</dd>

			<dt style="color: #333;">Value</dt>
			<dd>[###]</dd>

			<dt style="color: #333;">ATV</dt>
			<dd>[###]</dd>
		</dl>
	</div>
	<div class="col-sm-4">
		<h4>Year</h4>
		<dl>
			<dt style="color: #333;">Listings</dt>
			<dd>[###]</dd>

			<dt style="color: #333;">Value</dt>
			<dd>[###]</dd>

			<dt style="color: #333;">ATV</dt>
			<dd>[###]</dd>
		</dl>
	</div>
</div> -->
@endsection
