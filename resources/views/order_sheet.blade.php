@extends('layout.master')
@section('printStyle')
<link rel="stylesheet" href="{{asset('css/orderSheet.css')}}" media="print">
@endsection
@section('content')
<table class="table">	
	<tr class="text-center">
		<th>ID</th>
		<th>Customer</th>
		<th>Items</th>		
		<th>Ship to</th>
	</tr>
	
	@foreach($orders as $order)
	<tr class="text-center">
		<td>{{$order->id}}</td>
		<td>{{$order->full_name}}</td>
		<td>
			<h5>Item Name <small>[Location] [Qty]</small></h5>
			<h5>Item Name 2 <small>[Location] [Qty]</small></h5>
		</td>
		<td>
			<strong>{{$order->full_name}}</strong><br>
			{{$order->ship_addressLine1}}<br>
			{{$order->ship_addressLine2}}<br>
			{{$order->ship_city}}<br>
			{{$order->ship_state_or_province}}<br>
			{{$order->ship_postal_code}}<br>
			{{$order->ship_country}}({{$order->ship_country_code}})
		</td>				
	</tr>
	@endforeach
</table>
@endsection