@extends('layout/master')
@section('content')
<style>
	#pageTitle{
		display: none;
	}
	.pathLink{
		text-align: center;
		vertical-align: middle;
		font-size: 1.8em;
	}
</style>
<div class="row">
	<div class="col-sm-3 pathLink">

	</div>
	<div class="col-sm-6 text-center">
		<h1>How to Select Items</h1>
	</div>
	<div class="col-sm-3 pathLink">
		<a href="{{route('researchingItems')}}">Next Page<img src="{{asset('img/next.png')}}" alt=""></a>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">
		<p style="font-size: 1.2em;">
			We receive hundreds of items each week and aim to sell them as fast as possible.
			<br>
			A large amount of time is spent returning low value or unsuitable items.
			<br>
			Please check your items against the lists below before sending.
		</p>
		<p style="font-size: 1.2em; margin: 0;">
			<strong>Items with a value under &pound;30 will be returned.</strong><br>
			<a href="{{route('findValue')}}" class="cta" style="margin-top: 10px;">Find values</a>
		</p>
	</div>
</div>
<hr>
<div class="row text-center">
	<div class="col-sm-4">
		<div class="card card-body">
			<img src="{{asset('img/tick.png')}}" alt="" style="width: 64px; margin: 0 auto;">
			<h2>Send</h2>
			<p>
				These items are high value and sell quickly.<br>
				They often worth more online, than in a shop.
			</p>
			<ul>
				<li>[Item Name]</li>
				<li>[Item Name]</li>
				<li>[Item Name]</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card card-body">
			<img src="{{asset('img/info.png')}}" alt="" style="width: 64px; margin: 0 auto;">
			<h2>Ask Us</h2>
			<p>The value and chance of selling varies. <br> Please <a href="#">ask us</a> before sending.</p>
			<ul>
				<li>[Item Name]</li>
				<li>[Item Name]</li>
				<li>[Item Name]</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card card-body">
			<img src="{{asset('img/cross.png')}}" alt="" style="width: 64px; margin: 0 auto;">
			<h2>Don't Send</h2>
			<p>
				These are common and low value items.<br>
				There is a poor chance of them selling.
			</p>
			<ul>
				<li>[Item Name]</li>
				<li>[Item Name]</li>
				<li>[Item Name]</li>
			</ul>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">
		<h4>If in doubt...</h4>
		<h5>Email: [address@domain.tld]</h5>
		<h5>Phone: [00000 000000]</h5>
	</div>
</div>
@endsection
