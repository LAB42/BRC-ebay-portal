<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>DENIED</title>
</head>
	<style>
		#message{
			font-family: Segoe UI;
			margin-top: 150px;
			text-align: center;
		}
		h1{
			color: #ee2a24;
			font-size: 4.2em;
			margin: 0;
			padding: 0;
		}
		ul{
			font-size: 2.4em;
			margin: 0;
			padding: 0;
			list-style: none;
		}
	</style>
<body>
	<div id="message">
		<h1>DENIED</h1>
		<ul>
			<li>No roles</li>
			<li>No permissions</li>
			<li>No service</li>
		</ul>
	</div>
</body>
</html>