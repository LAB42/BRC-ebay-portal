@extends('layout.master')
@section('headScripts')
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['sankey']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {


        var data = new google.visualization.DataTable();
        data.addColumn('string', 'From');
        data.addColumn('string', 'To');
        data.addColumn('number', 'Weight');
        data.addRows([
          [ 'Stock', 'NIS', 125 ],
          [ 'NIS', 'Low Value', 50 ],
          [ 'NIS', 'Unsuitable', 37 ],
          [ 'NIS', 'Fake', 12 ],
          [ 'Stock', 'Listed', 650 ],
          [ 'Listed', 'Sold', 420 ],
          [ 'Sold', 'Dispatched', 350 ],
          [ 'Dispatched', 'Collection', 14],
          [ 'Dispatched', 'Returns', 15],
          [ 'Dispatched', '48', 175],
          [ 'Dispatched', 'Courier', 37],
        ]);

        // Sets chart options.
        var options = {
          sankey: { node: { width: 20 } },
        };

        // Instantiates and draws our chart, passing in some options.
        var chart = new google.visualization.Sankey(document.getElementById('sankey_basic'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Type', 'Qty'],
      ['Listed',   420],
      // ['NIS',      nis],
      ['Pending',  500],
    ]);

    var options = {
      title: 'Current Stock'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
  }
</script>

    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Week', 'Listings', 'Sales'],
          ['30',  289,      75],
          ['31',  320,      129],
          ['32',  600,       150],
          ['33',  541,      420]
        ]);

        var options = {
          title: 'Listings',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>

@endsection
@section('content')
<style>
	.whSection{
		border: solid 1px #c5c5c5; padding: 10px 0;
		border-radius: 10px;
		margin: 10px;
	}
	.sectionNumber{
		background: #DDD;
		display: inline-block;
		padding: 10px 20px;
		border-radius: 360px;
	}
	.high{
		background: #7d1c23;
		color: #fff;
	}

	.medium{
		background: #f1b13b;
		color: #fff;
	}

	.low{
		background: #04923e;
		color: #fff;
	}
	h2,h3,h4{ line-height: 0.8em; }
	h2{ font-size: 1.3em; }
	h3{ font-size: 1.2em; }
	h4{ font-size: 1.1em; }


</style>
<div class="row">
	<div class="col-sm-12">
		<a href="{{route('email-warehouse-report')}}" class="cta">Email report</a>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12">
		<dl style="font-size: 1.4em; text-align: center;">
			<dt style="display: inline;">Messages</dt>
			<dd style="display: inline; margin-right: 25px;">5</dd>
			<dt style="display: inline;">Due In</dt>
			<dd style="display: inline; margin-right: 25px;">North 18 | South 9</dd>
			<dt style="display: inline;">New Orders</dt>
			<dd style="display: inline; margin-right: 25px;">19</dd>
		</dl>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-4">
		<div id="sankey_basic" style="width: 100%; height: 420px; margin: 0 auto; background: #FFF; border: solid 1px #666; padding: 15px;"></div>
	</div>
	<div class="col-sm-4">
		<div id="piechart" style="width: 100%; height: 420px; margin: 0 auto; background: #FFF; border: solid 1px #666;"></div>
	</div>
	<div class="col-sm-4">
		<div id="curve_chart" style="width: 100%; height: 420px; margin: 0 auto; background: #FFF; border: solid 1px #666;"></div>
	</div>
</div>
<br><br>
<hr>
<div class="row" style="margin-top: 15px;">
	@foreach($staff as $person)
	<div class="col-sm-3 text-center">
		<img src="{{strtolower(asset('img/user_icons/'.$person->initials.'.png'))}}" alt="placeholder" style="width: 60px; height: 60px; border-radius: 360px; margin-bottom: -35px; z-index: 2000; position: relative;"><br><br>
		<h4 style="border-radius: 360px; display: inline-block; padding: 20px; color: #FFF; font-weight: bold;
			@if($person->working === true)
				background: #04923e;
			@elseif($person->working === false && $person->current_task === 'break')
				background: #d0011b;
			@elseif($person->working === false)
				background: #eee;
			@else
				background: #eee;
			@endif
		">{{$person->initials}}</h4>
		<h5>
			@if($person->current_task !== 'out-of-office')
				{{strtoupper($person->current_task)}}
			@endif
		</h5>
		@if($person->user === Auth::user()->id)
			@if($userCurrentTask === 'out-of-office')
			<form action="{{route('warehouse-check-in')}}" method="post">
				@csrf
				<input type="hidden" name="process" value="in">
				<input type="submit" value="Check In" class="filterBtn" style="background: #666; padding: 5px 10px; border-radius: 360px; color: #FFF;">
			</form>
			@else
			<form action="{{route('warehouse-check-in')}}" method="post">
				@csrf
				<input type="hidden" name="process" value="out">
				<input type="submit" value="Check Out" class="filterBtn" style="background: #666; padding: 5px 10px; border-radius: 360px; color: #FFF;">
			</form>
			@endif
		@endif
	</div>
	@endforeach
</div>
<hr>
<br>
<div class="row">
	<div class="col-sm-12 text-center">
		<h2 class="sectionHeader" style="font-size: 2em;">Processes</h2>
	</div>
</div>
<div class="row">
	@foreach($processes as $process)
	<div class="col-sm-3 text-center">
		<div class="whSection">
			<div class="row">
				<div class="col-sm-3">
					<h6 style="font-size: 1.8em; border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-right: solid 1px #ccc; background: #eee; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">P</h6>
				</div>
				<div class="col-sm-6" style="margin-left: 3px;">
					<h2 class="sectionNumber {{$process->status}}">{{strtoupper($process->section)}}</h2>
				</div>
			</div>
			<h3>{{$process->current}} / {{$process->high}}</h3>
			<h4>[Person]</h4>
			@if($process->type === 'process' && $userCurrentTask === $process->section)			
			<h4 style="border-radius: 360px; display: inline-block; padding: 16px; margin-top: 5px; color: #FFF; font-weight: bold; background: #ccc;">
				{{$userInitials}}
			</h4>
			@elseif($process->current !== 0)
			<form action="{{route('warehouse-check-in')}}" method="post">
				@csrf
				<input type="hidden" name="process" value="{{$process->id}}">
				<input type="image" src="{{asset('img/icons/check_in.svg')}}" style="width: 64px;">
			</form>
			@endif
		</div>
	</div>
	@endforeach
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">
		<h2 class="sectionHeader" style="font-size: 2em;">Storage</h2>
	</div>
</div>
<div class="row">
	@foreach($storage as $space)
	<div class="col-sm-4 text-center">
		<div class="whSection">
			<div class="row">
				<div class="col-sm-2">
					<h6 style="font-size: 1.8em; border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-right: solid 1px #ccc; background: #eee; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">S</h6>
				</div>
				<div class="col-sm-8" style="margin-left: 3px;">
					<h2 class="sectionNumber {{$space->status}}">{{strtoupper($space->section)}}</h2>
				</div>
			</div>
			<h3>{{$space->current}} / {{$space->high}}</h3>
			@foreach($staff as $person)
			@if($space->section === $person->current_task)
			<h4 style="border-radius: 360px; display: inline-block; padding: 20px; color: #FFF; font-weight: bold; background: #CCC;">
			{{$person->initials}}</h4>

			@endif
		@endforeach
	</div></div>
	@endforeach
</div>

@endsection
