@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div style="display: inline-block; min-width: 200px; min-height: 42px; background: {{$area->color}};"></div>
		<hr>

		<dl>	
			<dt>Area Manager</dt>
			@if($area->area_manager)
				<dd>{{$area->area_manager}}</dd>				
			@else
				<dd>No area manager information</dd>
			@endif			
		</dl>

		<hr>
		<p>{{$area->notes}}</p>
		<hr>
		
		<h4 class="sectionHeader">Shops</h4>

		@if(count($area->shops) === 0)
			<h4>There are no shops in this area.</h4>
		@else
		<ul>
			@foreach($area->shop as $shop)
			<li><a href="{{route('single-shop', ['id' => $shop->id])}}">{{$shop->name}}</a></li>
			@endforeach
		</ul>
		@endif

	</div>
</div>
@endsection