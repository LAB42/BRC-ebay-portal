@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<form action="#" method="post">
			<label for="name">Name</label>
			<input type="text" name="name" id="name" value="{{Auth::user()->name}}">

			<label for="email">Email</label>
			<input type="text" name="email" id="email" value="{{Auth::user()->email}}">

			<label for="picture">Picture</label>
			<div style="border: solid 1px #444; padding: 10px 25px 50px; width: 420px;">
				<input type="file" name="picture" id="picture" style="padding: 0; text-align: center; width: 420px;">
				<p style="text-align: center; margin-top: 50px; font-weight: bold;">or<br>drop file<br>here</p>
			</div>			

		</form>
	</div>
</div>
@endsection