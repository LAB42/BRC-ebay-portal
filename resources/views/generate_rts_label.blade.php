@extends('layout.master')
@section('printStyle')
<link rel="stylesheet" href="{{asset('css/rtsLabel.css')}}" media="print">
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<form action="{{route('do-generate-rts-label')}}" method="POST">
			@csrf
			<label for="code">Shop Code</label>			
			<input type="text" name="code" id="code">

			<label for="qty">Number of Crates</label>
			<input type="text" name="qty" id="qty">
			<br><br>
			<input type="submit" value="Generate">
		</form>
	</div>
</div>
@endsection