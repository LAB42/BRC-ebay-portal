@extends('layout.master')

@section('content')
<div class="row">
	@foreach($brandValues as $k=>$v)
	<div class="col-sm-3">		
		<h4 class="sectionHeader">{{$k}}</h4>
		<ul>	
		@foreach($v as $bv)
			<li>{{$bv}}</li>
		@endforeach
		</ul>
	</div>
	@endforeach	
</div>

@endsection