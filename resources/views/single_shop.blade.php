@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">		
		<form action="{{route('doFindShop')}}" method="post">
			@csrf
			<label for="query" style="display: none;">Shop Code / Name</label>
			<input type="text" name="query" id="query" placeholder="Shop Code/Name" autofocus>

			<input type="submit" value="Search" style="margin-left: -5px;">
		</form>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-4">
		<h2>{{$shop->name}}</h2>		
		<?php $formattedArea = preg_replace('$ $', '',strtolower($shop->area->name)); ?>
		<div class="area" style="background: {{$colors[$formattedArea]}}; color: #FFF;">
			{{$shop->area->name}}
		</div>
		<h4 style="display: inline; margin: 0 15px;">{{$shop->code}}</h4>
	</div>
	<div class="col-sm-2">
		<dl>
			<dt>Total Sent</dt>
			<dd>{{$stats['total']}}</dd>

			<dt>Current Listed</dt>
			<dd>{{$stats['listed']}}</dd>
		</dl>
	</div>
	<div class="col-sm-2">
		<dl>
			<dt>Returned</dt>
			<dd>{{$stats['returns']}}</dd>

			<dt>Sold</dt>
			<dd>{{$stats['sold']}}</dd>
		</dl>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-4">		
		<div class="card card-body">
			<h3 class="sectionHeader">Other Information</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae unde error similique, praesentium neque molestiae atque deserunt ad aperiam ut odio. Laudantium velit maiores, ratione aliquam aliquid, ut mollitia molestiae.</p>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur quis beatae, enim ad officiis fugit voluptatibus, facere eum quasi atque quisquam quidem, aperiam eveniet eaque, eius pariatur iusto doloribus quaerat!</p>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste tempora accusamus omnis qui asperiores voluptatum, nihil odit consectetur, eaque ea ipsa quae ut rem. Molestiae velit facilis quia dolorum impedit.</p>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card card-body">
			<h3 class="sectionHeader">Contact</h3>
			<div class="row">
				<div class="col-sm-6">
					@if($shop->manager !== NULL)
					<dl>									
						<dt>Shop Manager <small><a href="#">Message</a></small></dt>
						<dd>{{$shop->manager}}</dd>
					</dl>
					@else
					<p><strong class="warningText">No shop manager information</strong></p>
					@endif

					@if($shop->assistant_manager !== NULL)
					<dl>
						<dt>Assistant Shop Manager <small><a href="#">Message</a></small></dt>
						<dd>{{$shop->assistant_manager}}</dd>
					</dl>
					@else
						<p><strong class="warningText">No assistant shop manager information</strong></p>					
					@endif
				</div>
				<div class="col-sm-6">
					<h4>0118 999 881 999 119 7253</h4>				

					<p>
						[Address Line 1]<br>
						[Address Line 2]<br>
						[Town]<br>
						[County]<br>
						[Post Code]
					</p>	
				</div>
			</div>			
		</div>
		 <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 420px;
      }
    </style>
<div id="map"></div>
<script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 57.1497170, lng: -2.0942780},
          zoom: 12
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWkjVnPto3s8jiahXRj46wtlvWBMBT4o0&callback=initMap"
    async defer></script>
	</div>
	<div class="col-sm-4">		
		<div class="card card-body">
			<h3 class="sectionHeader">Awards</h3><br>
			<table class="table">
				<tr style="display: none;">				
					<td>Badge</td>
					<td>Reason</td>
					<td>Date</td>
				</tr>
				<tr>				
					<td><img src="{{asset('img/award.png')}}" alt="" width="50"></td>
					<td>Awarded for detailed paperwork</td>
					<td>{{date('M y', time())}}</td>
				</tr>
				<tr>				
					<td><img src="{{asset('img/green_award.png')}}" alt="" width="50"></td>
					<td>Awarded for only sending high-value items</td>
					<td>{{date('M y', time())}}</td>
				</tr>
				<tr>				
					<td><img src="{{asset('img/pink_award.png')}}" alt="" width="50"></td>
					<td>Awarded for researching items before sending</td>
					<td>{{date('M y', time())}}</td>
				</tr>
			</table>
		</div>
	</div>
</div>
@endsection