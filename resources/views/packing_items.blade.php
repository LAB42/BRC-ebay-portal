@extends('layout/master')
@section('content')
<style>
	#pageTitle{
		display: none;
	}
	.pathLink{
		text-align: center;
		vertical-align: middle;
		font-size: 1.8em;		
	}
</style>
<div class="row">
	<div class="col-sm-3 pathLink">
		<a href="{{route('researchingItems')}}"><img src="{{asset('img/back.png')}}" alt=""></a>
	</div>
	<div class="col-sm-6 text-center">
		<h1>Packing Items</h1>		
	</div>
	<div class="col-sm-3 pathLink">
		
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">
		<p style="font-size: 1.2em;">
			We receive hundreds of items each week and aim to sell them as fast as possible.
			<br>
			A large amount of time is spent returning low value or unsuitable items.
			<br>
			Please chek your items against the lists below before sending.
		</p>
	</div>	
</div>
<hr>
<div class="row text-center">
	<div class="col-sm-4">
		<img src="{{asset('img/box.png')}}" alt="">		
		<h2>Boxes & Bags</h2>
		<p>
		Most items can be packed into suitable boxes or crates.<br>
		Your driver will be give similar containers in return.
		<hr>
		If possible, place clothing &amp; shoes into bags.
		<hr>
		Keep fragile items seperate from heavier ones. Try to use bubblewrap, not newspaper.
		Mark the box as fragile.
		</p>
	</div>
	<div class="col-sm-4">
		<img src="{{asset('img/paperwork.png')}}" alt="">
		<h2>Label</h2>
		<p>			
			<strong>Label each box with:</strong>
		</p>
		<ul style="list-style: none; padding: 0; margin-top: -15px;">
			<li>Shop Name</li>
			<li>Shop Code</li>
			<li>Area Code</li>
			<li>Box/Bag Number</li>
		</ul>
		<a href="#" class="cta">Generate labels</a>
	</div>
	<div class="col-sm-4">
		<img src="{{asset('img/checklist.png')}}" alt="">
		<h2>Logsheet</h2>
		<p>			
			<strong>For each item:</strong>
			<ul style="list-style: none; padding: 0; margin-top: -15px;">
				<li><span style="padding: 0 10px; color: #04923e; font-size: 1.8em; vertical-align: middle;">&#10004</span>Item Name</li>
				<li><span style="padding: 0 10px; color: #04923e; font-size: 1.8em; vertical-align: middle;">&#10004</span>Value</li>
				<li><span style="padding: 0 10px; color: #04923e; font-size: 1.8em; vertical-align: middle;">&#10004</span>GA Status</li>
				<li><span style="padding: 0 10px; color: #04923e; font-size: 1.8em; vertical-align: middle;">&#10004</span>Comments</li>
			</ul>
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">		
		<h4>If in doubt...</h4>
		<h5>Email: [address@domain.tld]</h5>
		<h5>Phone: [00000 000000]</h5>		
	</div>
</div>
@endsection