@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<h4 class="sectionHeader">Generate Picklist</h4><br>

		<form action="{{url('picklist/assign')}}" method="post">
			@csrf
			<input type="hidden" name="items" value="{{json_encode($items)}}">
			<input type="submit" value="Assign">
		</form>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-3">
		<h2><a href="{{url('picklist')}}">All</a></h2>
		<p>All areas, all prices</p>
	</div>
	<div class="col-sm-3">
		<h2><a href="{{url('picklist/north')}}">North</a></h2>
		<p>All items above &pound30</p>
	</div>

	<div class="col-sm-3">
		<h2><a href="{{url('picklist/south')}}">South</a></h2>
		<p>All items above &pound30</p>
	</div>

	<div class="col-sm-3">
		<h2><a href="{{url('picklist/clearance')}}">Clearance</a></h2>
		<p>All items under &pound30</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12">
		<div style="font-size: 1.2em;">
			<dl class="row">
				<div class="col-sm-2">
					<dt>Items</dt>
					<dd>{{count($pick)}}</dd>
				</div>
				
				<div class="col-sm-2">
					<dt>Area(s)</dt>
					<dd>{{$area}}</dd>
				</div>
				@if(strtolower($area) != 'clearance')
				<div class="col-sm-4">
					<strong>Sort By</strong>
					<form action="{{url('picklist/'.strtolower($area).'/sortBy')}}" method="post">
						@csrf
						<?php
						function selectValue($value, $selected){
							return ($value === $selected) ? 'selected' : '';
						}
						?>
						<select name="sortPrice" id="sortPrice" autocomplete="off">
							<option value="under_30" {{selectValue('under_30', old('sortPrice'))}}>Under &pound;30</option>
							<option value="30_50" {{selectValue('30_50', old('sortPrice'))}}>&pound;30 &ndash; &pound;50</option>
							<option value="50_100" {{selectValue('50_100', old('sortPrice'))}}>&pound;50 &ndash; &pound;100</option>
							<option value="over_100" {{selectValue('over_100', old('sortPrice'))}}>&pound;100+</option>
						</select>
						<input type="submit" value="Sort">
					</form>					
				</div>
				@endif
				<div class="col-sm-4"></div>
			</dl>
		</div>		
		<table class="table" style="text-align: center;">
			<tr>
				<th>ID</th>
				<th>Area</th>
				<th>Crate</th>
				<th>#</th>
				<th>Name</th>
				<th>Value</th>
				<th>Picked?</th>
			</tr>
			@foreach($pick as $p)
			<tr>
				<td>&num;{{$p->id}}</td>
				<td>{{$p->area}}</td>
				<td>{{$p->location}}</td>
				<td>{{$p->crate_item_number}}</td>
				<td><a href="{{url('item/'.$p->id)}}">{{$p->item_name}}</a></td>				
				<td>&pound;{{$p->price}}</td>
				<td></td>
			</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection