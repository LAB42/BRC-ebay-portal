@extends('layout.master')

@section('content')
<style>
	.guideList{
		list-style: none;
		padding: 0;
		font-size: 1.3em;
		line-height: 2em;
	}
</style>
<div class="row">
	<div class="col-sm-12">
		<!-- <a href="{{route('warehouseWorkflow')}}" class="cta">Warehouse Workflow</a> -->
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12">
		<h4 class="sectionHeader">Filter</h4>
		<p>
			<a href="#" class="filterBtn">Shops</a>
			<a href="#" class="filterBtn">Volunteers</a>
			<a href="#" class="filterBtn">Ebay Warehouse</a>
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-4">
		<div class="card card-body">
			<h3 class="sectionHeader">Sending Items</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus iusto reiciendis nesciunt nemo sint voluptatem id libero, autem sunt tenetur esse excepturi ratione cupiditate dolorum blanditiis odit incidunt a repellat!</p>

			<ul class="guideList list-group-flush">
				<li class="list-group-item"><a href="{{route('selectingItems')}}">Selecting Items</a></li>
				<li class="list-group-item"><a href="{{route('findValue')}}">Find value</a></li>
				<li class="list-group-item"><a href="#">Logsheets</a></li>

			</ul>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card card-body">
			<h3 class="sectionHeader">Clothing</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus iusto reiciendis nesciunt nemo sint voluptatem id libero, autem sunt tenetur esse excepturi ratione cupiditate dolorum blanditiis odit incidunt a repellat!</p>
			<ul class="guideList list-group-flush">
				<li class="list-group-item"><a href="{{route('sizeGuide')}}">Sizes</a></li>
				<li class="list-group-item"><a href="{{route('clothingBrands')}}">Brands</a></li>
			</ul>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card card-body">
			<h3 class="sectionHeader">Authentication</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus iusto reiciendis nesciunt nemo sint voluptatem id libero, autem sunt tenetur esse excepturi ratione cupiditate dolorum blanditiis odit incidunt a repellat!</p>
			<ul class="guideList list-group-flush">
				<li class="list-group-item"><a href="#">Clothes</a></li>
				<li class="list-group-item"><a href="#">Shoes</a></li>
				<li class="list-group-item"><a href="#">Handbags</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm 12">

		<!--
		remove from controller and blade
		<a href="{{url('guides/categories')}}" class="cta">Categories</a><br>
		<a href="{{url('guides/custominfo')}}" class="cta">Custom Info</a> -->
	</div>
</div>
<hr><!--
<div class="row">
	<div class="col-sm-12">
		<h4>Course</h4>
		<h5>Goods-in</h5>
		<ol>
			<li>Completing a Logsheet</li>
			<ol>
				<li>Paper</li>
				<li>Digital</li>
			</ol>
			<li>Finding Value</li>
			<ol>
				<li>Research Links</li>
				<li>Product Authentication</li>
				<li>Item Title</li>
				<li>Create BIN Card</li>
			</ol>
			<li>Returning Items</li>
			<ol>
				<li>Create Return Card</li>
				<li>Packing Items</li>
			</ol>
		</ol>

		<h5>Create Listing</h5>
		<ol>
			<li>W/S/C</li>
			<ol>
				<li>Weight</li>
				<ol>
					<li>Jewellery</li>
					<li>Clothing</li>
					<li>Other Items</li>
				</ol>
				<li>Size</li>
				<ol>
					<li>Guides</li>
				</ol>
				<li>Condition</li>
				<ol>
					<li>Describing the Condition</li>
				</ol>
				<li>Shipping</li>

			</ol>
			<li>Photos</li>
			<ol>
				<li>Small / Medium</li>
				<li>Large</li>
				<li>Clothing</li>
			</ol>
			<li>Listing</li>
		</ol>

		<h5>Confirm Listing</h5>
		<ol>
			<li>Final Check</li>
			<li>Storing Listed Items</li>
		</ol>

		<h5>Orders</h5>
		<ol>
			<li>Receiving Orders</li>
			<li>Picking</li>
			<li>Postage</li>
			<li>Dispatching</li>
		</ol>

		<h5>Customer Service</h5>
		<ol>
			<li>Canned Responses</li>
		</ol>
	</div>
</div> -->
@endsection
