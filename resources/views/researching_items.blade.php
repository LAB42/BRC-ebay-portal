@extends('layout/master')
@section('content')
<style>
	#pageTitle{
		display: none;
	}
	.pathLink{
		text-align: center;
		vertical-align: middle;
		font-size: 1.8em;		
	}
</style>
<div class="row">
	<div class="col-sm-3 pathLink">
		<a href="{{route('selectingItems')}}"><img src="{{asset('img/back.png')}}" alt=""></a>
	</div>
	<div class="col-sm-6 text-center">
		<h1>How to Research Items</h1>		
	</div>
	<div class="col-sm-3 pathLink">
		<a href="{{route('packingItems')}}"><img src="{{asset('img/next.png')}}" alt=""></a>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">
		<p style="font-size: 1.2em;">
			We receive hundreds of items each week and aim to sell them as fast as possible.
			<br>
			A large amount of time is spent returning low value or unsuitable items.
			<br>
			Please chek your items against the lists below before sending.
		</p>
	</div>	
</div>
<hr>
<div class="row text-center">
	<div class="col-sm-3">
		<img src="{{asset('img/ebay_logo.png')}}" alt="">		
		<h2>Any Items</h2>
		<p>			
			Perfect for researching used items.
			<hr>
			Use the 'sold listings' search option.
			<hr>
			Sixth most popular website in the UK. 360 million visitors a year.			
		</p>
	</div>
	<div class="col-sm-3">
		<img src="{{asset('img/amazon_logo.png')}}" alt="" height="60px">
		<h2>New Items</h2>
		<p>
			Great for researching new items.
			<hr>
			Try to find price from non-Prime items.
			<hr>
			Don't include shipping costs.
		</p>
	</div>
	<div class="col-sm-3">
		<img src="{{asset('img/abe_books_logo.png')}}" alt="" height="60px">
		<h2>Books</h2>
		<p>			
			<strong>Basic search</strong><br>
			Author, tile, keyword or ISBN.<br>
			<strong>Advanced search</strong><br>
			Publisher &amp; date, binding, etc.			
		</p>
		<a href="#" class="cta">Abe Books</a>
		<a href="#" class="cta">World of Books</a>
	</div>
	<div class="col-sm-3">
		<img src="{{asset('img/google_logo.png')}}" alt="" height="60px">
		<h2>General Info</h2>
		<p>
			<a href="#" class="cta">Obviously</a><br>
			<strong>Tips</strong><br>
			Use different search tabs
			<ul style="list-style: none; padding: 0; margin-top: -15px;">
				<li style="display: inline;"><a href="https://www.google.co.uk/">All |</a></li>
				<li style="display: inline;"><a href="https://images.google.co.uk">Images |</a></li>
				<li style="display: inline;"><a href="https://www.google.co.uk/shopping">Shopping |</a></li>
				<li style="display: inline;"><a href="https://books.google.co.uk/">Books</a></li>
			</ul>
		</p>		
		<p>
			Use quotes to search for whole phrases<br>
			Use a hyphen to exclude words<br>
			Search specific sites <em>site: amazon.co.uk</em>
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">		
		<h4>If in doubt...</h4>
		<h5>Email: [address@domain.tld]</h5>
		<h5>Phone: [00000 000000]</h5>		
	</div>
</div>
@endsection