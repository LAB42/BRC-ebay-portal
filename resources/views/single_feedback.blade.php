@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		<hr>
		<form action="{{route('feedback-mark-as-read')}}" method="POST">
			@csrf
			<label for="read" style="display: none;">Mark as read</label>
			<input type="hidden" name="messageId" value="{{$message->id}}">
			<input type="checkbox" name="read" id="read" checked hidden>
			<input type="submit" value="Mark as Read" class="filterBtn">
		</form>
		<br><br>
		<h2>{{$message->title}}</h2>
		<h5><strong>{{strtoupper($message->subject)}}</strong></h5>
		<hr>

		<dl>
			<dt>From</dt>
			<dd>{{$message->submitted_by}}</dd>

			<dt>Sent</dt>
			<dd>{{$message->created_at}}</dd>
		</dl>

		<p>{{$message->message}}</p>

		<hr>

		<form action="{{route('feedback-update-notes')}}" method="POST">
			@csrf
			<label for="notes">Update notes</label>
			<input type="hidden" name="messageId" value="{{$message->id}}">
			<textarea name="notes" id="notes" cols="75" rows="10">{{$message->notes}}</textarea>
			<br><br>
			<input type="submit" value="Update" class="filterBtn">
		</form>

	</div>
</div>
@endsection