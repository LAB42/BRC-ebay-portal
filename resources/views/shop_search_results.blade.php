@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<ul>
		@foreach($results as $result)
			<li><a href="{{url('shops/'.$result->id)}}">{{$result->name}}</a></li>
		@endforeach
		</ul>
	</div>
</div>
@endsection