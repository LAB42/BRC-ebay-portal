<h1>Warehouse Overview</h1>

<p>There are <strong>{{$total}}</strong> items on the inventory</p>

<h4>Breakdown</h4>

<ul>
	@foreach($breakdownCounts as $bc)
	<li>{{$bc['name']}} &ndash; {{$bc['count']}}</li>
	@endforeach
</ul>

<p>
	<em>Generated on {{date('d-m-Y', time())}} at {{date('h:i:s', time())}}</em>
</p>