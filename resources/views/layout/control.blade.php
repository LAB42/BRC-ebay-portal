<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>
		@if(isset($page['title']))
			BRC - {{$page['title']}}
		@else
			BRC
		@endif
	</title>

	<link rel="stylesheet" href="{{asset('css/app.css')}}">
	<link rel="stylesheet" href="{{asset('css/theme.css')}}">
	<link rel="stylesheet" href="{{asset('css/Flaticon.css')}}">
</head>
<body>

	<div class="container-fuild" id="header">		
		<div class="row">
			<div class="col-sm-12" style="text-align: center;">
				<h3 style="color: #ee2a24; text-transform: uppercase; font-weight: bold;">Ebay Application Thing</h3>
			</div>
		</div>
		
	
		
	</div>	
	
	<div class="container-fuild">		 
		 <div class="row">
		 	<div class="col-sm-12">
				@yield('message')
		 	</div>
		 </div>
		
		<div class="row">
			<div class="col-sm-12">
				@if(isset($page['title']))
					<h1 style="margin-bottom: 20px;">{{$page['title']}}</h1>
				@endif
			</div>
		</div>
		@yield('content')		
	@yield('bottomScripts')
</body>
</html>