<div class="mainNav">
	<ul>
		<li><a href="{{route('logbook')}}">Logbook</a></li>
		@role(['ebaymanagement','ebay_staff'])
		<li><a href="{{route('inventory')}}">Inventory</a></li>
		<li><a href="{{route('listings')}}">Listings</a></li>		
		<li><a href="{{route('orders')}}">Orders</a></li>		
		@endrole		
		<li><a href="{{route('shops')}}">Shops</a></li>					
		<li><a href="{{route('guides')}}">Guides</a></li>
		@role(['ebaymanagement','ebay_staff'])
		<li><a href="{{route('reports')}}">Reports</a></li>
		<li><a href="{{route('warehouse')}}">Warehouse</a></li>
		<li><a href="">Customer Service</a></li>				
		@endrole
		<li><a href="{{route('account')}}">Account</a></li>
	</ul>	
	<div style="border-right: solid 2px #333; height: 100%; margin-right: 20px; display: inline;"></div>
	<a href="{{route('feedback-create')}}"><img src="{{asset('img/icons/feedback.svg')}}" alt="" style="width: 42px; margin-right: 15px;"></a>
	@if(Auth::check())
	<a href="{{route('logout')}}" class="accountControl"><img src="{{asset('img/account_in.png')}}" alt="" style="width: 42px; margin-bottom: 10px;"></a>
	@else
	<a href="{{route('login')}}" class="accountControl"><img src="{{asset('img/account_out.png')}}" alt="" style="width: 42px; margin-bottom: 10px;"></a>
	@endif
</div>