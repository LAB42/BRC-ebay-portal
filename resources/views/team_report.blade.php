@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-4">
		<h2 style="padding: 15px 0; background: #c657a9; color: #FFF; text-align: center; font-weight: bold">DJ</h2>
		<dl style="text-align: center; font-size: 1.2em;">
			<dt>Listed today</dt>
			<dd>21</dd>
			<dt>Listed this week</dt>
			<dd>75</dd>
			<dt>Total value today</dt>
			<dd>&pound;452</dd>
			<dt>Total value this week</dt>
			<dd>&pound;1956</dd>
			<dt>ATV</dt>
			<dd>&pound;45.63</dd>
		</dl>
	</div>
	<div class="col-sm-4">
		<h2 style="padding: 15px 0; background: #8f42f4; color: #FFF; text-align: center; font-weight: bold;">KF</h2>
		<dl style="text-align: center; font-size: 1.2em;">
			<dt>Listed today</dt>
			<dd>18</dd>
			<dt>Listed this week</dt>
			<dd>84</dd>
			<dt>Total value today</dt>
			<dd>&pound;514</dd>
			<dt>Total value this week</dt>
			<dd>&pound;2150</dd>
			<dt>ATV</dt>
			<dd>&pound;37.19</dd>
		</dl>
	</div>
	<div class="col-sm-4">
		<h2 style="padding: 15px 0; background: #249331; color: #FFF; text-align: center; font-weight: bold">NH</h2>
		<dl style="text-align: center; font-size: 1.2em;">
			<dt>Listed today</dt>
			<dd>3</dd>
			<dt>Listed this week</dt>
			<dd>9</dd>
			<dt>Total value today</dt>
			<dd>&pound;240</dd>
			<dt>Total value this week</dt>
			<dd>&pound;750</dd>
			<dt>ATV</dt>
			<dd>&pound;28.17</dd>
		</dl>
	</div>
</div>
@endsection

