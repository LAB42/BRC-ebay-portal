@extends('layout.master')

@section('content')
<hr>
<div class="row text-center">
	<div class="col-sm-4">
		<div class="card card-body">
			<h3>Waiting</h3>
			<h2>1</h2>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card card-body">
			<h3>Picked</h3>
			<h2>2</h2>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card card-body">
			<h3>Dispatched</h3>
			<h2>1</h2>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-4 text-center">		
		<h5 class="sectionHeader">Sort By</h5><br>
		<a href="#" class="cta">Royal Mail 48</a>
		<a href="#" class="cta">Royal Mail Special</a>
		<a href="#" class="cta">Courier</a>
		<p>Grouped by location, shipping method and value</p>
	</div>
	<div class="col-sm-4 text-center">
		<h5 class="sectionHeader">Print</h5><br>		
		<form action="{{route('print-invoices')}}" method="POST">
			@csrf
			<input type="hidden" name="ids" value="{{serialize($orderIDs)}}">		
			<input type="submit" class="cta" value="Invoices">
		</form>

		<a href="{{url('order-sheet')}}" class="cta">Pick List</a>
		
		<form action="{{route('print-labels')}}" method="POST">
			@csrf
			<input type="hidden" name="ids" value="{{serialize($orderIDs)}}">		
			<input type="submit" class="cta" value="Labels">
		</form>
		<p>Process waiting</p>
	</div>
	<div class="col-sm-4 text-center">
		<h5 class="sectionHeader">Archive</h5><br>
		<a href="" class="cta">Archive Dispatched</a>
		<a href="" class="cta">Generate Report</a>		
		<p>Manage dispatched orders</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">
		<a href="#" class="cta">Show all</a><br>
		<h5 class="sectionHeader">Filter By</h5><br>		
		<a href="#" class="filterBtn">Waiting</a>
		<a href="#" class="filterBtn">Picked</a>
		<a href="#" class="filterBtn">In Progress</a>
		<a href="#" class="filterBtn">Dispatched</a>
		<a href="#" class="filterBtn">Collection</a>
		<a href="#" class="filterBtn">Cancelled</a>
		<a href="#" class="filterBtn">On Hold</a>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12">
		<table class="table">
			<tr class="text-center">
				<th width="75">ID</th>
				<th width="225">SKU / Shipping</th>
				<th>Item Name</th>				
				<th width="150">Totals</th>
				<th width="150">Status</th>
			</tr>
			@foreach($orders as $order)
			<tr class="text-center">				
				<td style="vertical-align: middle; cursor: pointer;" class="clickable-row" data-id="{{$order->id}}">
					<a href="{{route('single-order', $order['id'])}}">#{{$order['id']}}</a>
				</td>
				<td style="vertical-align: middle; cursor: pointer;" class="clickable-row" data-id="{{$order->id}}">
					<strong>SKU: </strong>{{$order['sku']}}					
					<br>
					<strong>Royal Mail 48</strong> (&pound;9.50)
				</td>
				<td style="vertical-align: middle; cursor: pointer;" class="clickable-row" data-id="{{$order->id}}">
					<h4>Item Name <small>[Location] [Qty]</small></h4>
					<h4>Item Name 2 <small>[Location] [Qty]</small></h4>
				</td>				
				<td style="vertical-align: middle; cursor: pointer;" class="clickable-row" data-id="{{$order->id}}">
					Items: &pound;75.52<br>
					Delivery: &pound;3.50<br>
					<strong>Total: &pound;79.02</strong>
				</td>
				<td>
					<div style=" 
					{{($order['status'] === 'Picked') ? 'cursor: pointer;color: #FFF; vertical-align: middle; text-align: center; font-weight: bold; background: #f1b13b' : ''}}			
					{{($order['status'] === 'Dispatched') ? 'cursor: pointer; color: #FFF; vertical-align: middle; text-align: center; font-weight: bold;  background: #04923e' : ''}}
					{{($order['status'] === 'Waiting') ? 'cursor: pointer; color: #FFF; vertical-align: middle; text-align: center; font-weight: bold; background: #7d1c23' : ''}}
					{{($order['status'] === 'In Progress') ? 'cursor: pointer; color: #FFF; vertical-align: middle; text-align: center; font-weight: bold; background: #1a3351' : ''}}
					{{($order['status'] === 'On Hold') ? 'cursor: pointer; color: #FFF; vertical-align: middle; text-align: center; font-weight: bold; background: #baddea' : ''}}
					{{($order['status'] === 'Collection') ? 'cursor: pointer; color: #FFF; vertical-align: middle; text-align: center; font-weight: bold; background: #baddea' : ''}}
					{{($order['status'] === 'Cancelled') ? 'cursor: pointer; color: #FFF; vertical-align: middle; text-align: center; font-weight: bold; background: #baddea' : ''}}
					">
					</div>
					<div>
						<form action="#" method="post">
								<select name="status" id="status" style="text-align: center;">
								<option value="waiting">Waiting</option>
								<option value="in_progress">In Progress</option>
								<option value="picked">Picked</option>
								<option value="dispatched">Dispatched</option>
								<option value="collection">Collection</option>								
								<option value="on_hold">On Hold</option>
								<option value="cancelled">Cancelled</option>
							</select>
						</form>
					</div>
				</td>
			</td>
			</tr>
			@endforeach
		</table>
	</div>
</div>

@endsection

@section('bottomScripts')
<script>
  jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = '{{url('order')}}' + '/' + $(this).data('id');
    });
});
</script>
@endsection