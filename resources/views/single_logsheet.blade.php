<?php
use App\InventoryStatus;
?>
@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<h3>{{$logsheet->shop->name}}</h3>
		<h4>{{$logsheet->shop->code}}</h4>		
		<?php $formattedArea = preg_replace('$ $', '',strtolower($logsheet->shop->area->name)); ?>
		<div class="area" style="background: {{$colors[$formattedArea]}}; display: inline-block; color: #FFF;">
			@if(!empty($logsheet->shop->area->name))
				{{$logsheet->shop->area->name}}
			@else
				No Area
			@endif
		</div>					
		<hr>
		<div class="row">
			<div class="col-sm-4">
				<dl>
					<dt>Date Submitted</dt>					
					<dd>{{date('d/m/y', time($logsheet->date_submitted))}}</dd>

					<dt>Number of Items</dt>
					<dd>{{count($items)}}</dd>
				</dl>
			</div>
			@if($logsheet->received_by !== NULL)
			<div class="col-sm-4">
				<a href="{{url('logsheet/generateReturnCard/'.$logsheet->id)}}" class="cta posBtn">Create return card</a><br>
				<a href="{{url('logsheet/generateBinCard/'.$logsheet->id)}}" class="cta negBtn">Create BIN card</a><br>				
			</div>
			@endif
			<div class="col-sm-4">
				<a href="{{url('shops/'.$logsheet->shop->id)}}" target="_blank" class="cta">Shop Details</a>
			</div>
		</div>
		

		<hr>
		@if($logsheet->serial === NULL)
		<form action="{{url('logsheet/logsheetToInventory')}}" method="post">
		@endif
		@csrf
		<table class="table">			
			<tr>
				<th width="50" class="text-center">#</th>
				<th width="350">Name</th>
				<th width="350">Info</th>
				<th width="30" class="text-center">Value</th>
				<th width="10" class="text-center">Recieved</th>
				<th width="75" class="text-center">Status</th>
			</tr>
			@foreach($items as $item)
			<tr
			style="
				@if($item->value >= 60)
					background: #dbffd3;
				@elseif($item->value < 10)
					background: #ffdcd3;
				@endif
			"
			>
				<td class="text-center">{{$item->id}}</td>
				<td>{{$item->name}}</td>
				<td>{{$item->info}}</td>
				<td class="text-center">&pound;{{$item->value}}</td>
				<td class="text-center"><span style="padding: 0 10px; color: #04923e; font-size: 1.8em; vertical-align: middle;">&#10004</span></td>
				<td>
					@if($logsheet->received_by === NULL)
					<select name="status{{$item->id}}" id="status{$item->id}" autocomplete="off">
						@foreach($statuses as $status)		
							<!-- Low Value -->
							@if($item->value < 10 && $status->name === 'RTS - Low Value')
								<option value="{{$status->id}}" selected>{{$status->name}}</option>											
							<!-- Listing Value -->
							@elseif($item->value > 10 && $item->value < 60 && $status->name === 'Pending')
								<option value="{{$status->id}}" selected>{{$status->name}}</option>
							<!-- Express -->
							@elseif($item->value >= 60 && $status->name === 'Picked')
								<option value="{{$status->id}}" selected>{{$status->name}}</option>
							<!-- Anything else -->
							@else
								<option value="{{$status->id}}">{{$status->name}}</option>
							@endif
							
						@endforeach
					</select>
					@else
						{{InventoryStatus::find($item->status)->name}}
					@endif
				</td>
			</tr>
			@endforeach
		</table>
			@if($logsheet->serial === NULL)
			<input type="hidden" name="logsheetId" value="{{$logsheet->id}}">
			<label for="receivedBy">Received By</label>			
			<input type="text" name="receivedBy" id="receivedBy" size="47" value="{{Auth::User()->name}}">
			<label for="receivedDate">Received Date</label>
			<input type="date" name="receivedDate" id="receivedDate" size="47" value="{{date('Y-m-d', time())}}">
			<br><br><!-- Replace with CSS -->				
			<input type="hidden" name="numberOfItems" value="{{count($items)}}">
			<input type="hidden" name="shopArea" value="{{$logsheet->shop->area}}">
			<input type="hidden" name="shopCode" value="{{$logsheet->shop->code}}">
			<input type="hidden" name="shopName" value="{{$logsheet->shop->name}}">			
			<?php $i = 1; ?>
			@foreach($items as $item)
				<input type="hidden" name="name{{$i}}" value="{{$item->name}}">
				<input type="hidden" name="location{{$i}}" value="value">
				<input type="hidden" name="crate_item_number{{$i}}" value="{{$item->id}}">
				<input type="hidden" name="gift_aid{{$i}}" value="{{$item->gift_aid}}">
				<input type="hidden" name="est_value{{$i}}" value="{{$item->value}}">				
				<input type="hidden" name="comments{{$i}}" value="{{$item->info}}">			
				<?php $i++; ?>
			@endforeach
			<input type="submit" value="Add to Inventory">			
			@endif
		</form>
	</div>
</div>
@endsection