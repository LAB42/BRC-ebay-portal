<?php
use App\User;
?>
@extends('layout.master')
@section('content')

<div class="row">
	<div class="col-sm-7">
		<div id="itemName" style="width: 100%;"></div>
	</div>
	<div class="col-sm-2">
		<h4 style="margin-top: 15px; margin-left: -15px;
		@if(strlen($item->item_name) >= 80)
			color: #d0011b;
		@else
			color: #04923e;
		@endif
		">{{80 - strlen($item->item_name)}}</h4>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-6">
		<div class="card card-body">
			<div class="row">
				<div class="col-sm-8">
					<div class="row">
						<div class="col-sm-6">
							<form action="#" method="post">
								@method('PATCH')
								@csrf
								<select name="status" id="status" autocomplete="off" style="font-size: 1.2em; margin-bottom: 15px;">
									@foreach($inventoryStatuses as $is)
									@if($is->id === $item->inventoryStatus->id)
									<option value="{{$is->id}}" selected>{{$is->name}}</option>
									@else
									<option value="{{$is->id}}">{{$is->name}}</option>
									@endif
									@endforeach
								</select>
								<input type="submit" value="Update">
							</form>
						</div>
						<div class="col-sm-3">
							@if(strtolower($item->inventoryStatus->name) === 'pending' || strtolower($item->inventoryStatus->name) === 'picked' || strtolower($item->inventoryStatus->name) === 'on hold')
								<a href="{{url('list/'.$item->id)}}" class="posBtn" style="display: inline-block;">Create Listing</a>
							@endif

							<!--  relist if unsold-->
							@if(strtolower($item->inventoryStatus->name) === 'sold')
								<a href="{{url('list/'.$item->id)}}" class="posBtn" style="display: inline-block;">View Order</a>
							@endif
							@if(strtolower($item->inventoryStatus->name) === 'unsold')
								<a href="{{url('list/'.$item->id)}}" class="negBtn" style="display: inline-block;">Relist</a>
							@endif
						</div>
						<div class="row">
							<div class="col-sm-4">

							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-3">
							<h5>Location</h5>
							<div id="location" style="width: 75%"></div>
						</div>

						<div class="col-sm-3">
							<dl>
								<dt>Arrived</dt>
								<dd>{{date('d/m/Y', strtotime($item->date_received))}}</dd>

								<dt>Logsheet ID</dt>
								<dd>[81]</dd>
							</dl>
						</div>

						<div class="col-sm-6" style="margin-top: 10px;">
							<div id="valueIndicator" style="display: inline-block; margin-right: 10px;"></div>
							@if($item->weight_int >= 3 && $item->weight_unit === 'kg')
								<abbr title="Heavy"><img src="{{asset('img/item_icons/heavy_true.svg')}}" alt="Heavy" style="width: 32px; margin-right: 10px;"></abbr>
							@else
								<abbr title="Not heavy"><img src="{{asset('img/item_icons/heavy_false.svg')}}" alt="Not heavy" style="width: 32px; margin-right: 10px;"></abbr>
							@endif
							<abbr title="Small"><img src="{{asset('img/item_icons/large_false.svg')}}" alt="Large" style="width: 32px; margin-right: 10px;"></abbr>
							<abbr title="Not fragile"><img src="{{asset('img/item_icons/fragile_false.svg')}}" alt="Fragile" style="width: 32px; margin-right: 10px;"></abbr>
							<abbr title="Suitable to post"><img src="{{asset('img/item_icons/collection_false.svg')}}" alt="Collection" style="width: 32px; margin-right: 10px;"></abbr>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<form action="#" method="post">
						<h5>Comments</h5>
						<textarea name="comments" id="comments" cols="25" rows="2" style="background: none; border: none; padding: 0;">{{$item->comments}}</textarea>
					</form>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-sm-2">
				<!-- Replace with react -->
				<h5>Area</h5>
				<div class="area" style="background: {{$item->shop->area->color}}; display: inline-block; color: #FFF;">
					@if(!empty($item->shop->area->name))
						{{$item->shop->area->name}}
					@else
						No Area
					@endif
				</div>
			</div>
			<div class="col-sm-4">
				<h5>Shop Name</h5>
				<div id="shopName"></div>
				<a href="{{route('single-shop', ['id' => $item->shop->id])}}" target="_blank" class="filterBtn" style="display: inline-block;">Shop Info</a>
			</div>
			<div class="col-sm-4">
				<h5>Shop Code</h5>
				<div id="shopCode" style="width: 50%;"></div>
			</div>
			</div>
		</div>
	</div>

	<div class="col-sm-3">
		<div class="card card-body">
			<div class="row">
				<div class="col-sm-12">
					<form action="{{route('move-item-to-folder')}}" method="POST">
					@csrf
					<input type="hidden" name="item" value="{{$item->id}}">

					<label for="folder" style="display: none;">Folder</label>
					<select name="folder" id="folder" autocomplete="off">
						@if($item->folder_id === NULL)
							<option value="" selected>---Select Folder---</option>
						@endif

						@foreach($itemFolders as $folder):
							@if($folder->id === $item->folder_id)
							<option value="{{$folder->id}}" selected>{{$folder->name}}</option>
							@else
							<option value="{{$folder->id}}">{{$folder->name}}</option>
							@endif
						@endforeach
					</select>

					<input type="submit" value="Move">
				</form>

				<form action="{{route('remove-item-from-folder')}}" method="POST">
					@csrf
					<input type="hidden" name="item" value="{{$item->id}}">
					<input type="submit" value="Remove">
				</form>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-sm-6">
					<h5>Template</h5>
					<form action="#" method="post">
						<select name="template" id="template" style="width: 100%">
							<option value="master">Master</option>
							<option value="books">Books</option>
							<option value="clothing">Clothing</option>
						</select>
					</form>
				</div>

				<div class="col-sm-6">
					<h5>Tags</h5>
					<form action="#" method="post" style="margin-bottom: 10px;">
						<input type="text" name="tag" id="tag" size="15">
						<input type="submit" value="Add Tag">
					</form>
					<ul style="list-style: none; padding: 0; margin-top: 10px;">
						<li style="background: #eee; border: solid 1px #ccc; cursor: pointer; display: inline-block; font-size: 0.8em; padding: 5px 10px; border-radius: 5px;">Not dangerous</li>
						<li style="background: #eee; border: solid 1px #ccc; cursor: pointer; display: inline-block; font-size: 0.8em; padding: 5px 10px; border-radius: 5px;">Bears</li>
						<li style="background: #eee; border: solid 1px #ccc; cursor: pointer; display: inline-block; font-size: 0.8em; padding: 5px 10px; border-radius: 5px;">Sells fast</li>
					</ul>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-sm-12">
					<p>
					<!-- Breakout into array -->
					<a href="https://www.ebay.co.uk/sch/i.html?_from=R40&_nkw=<?php echo htmlspecialchars($item->item_name); ?>&_sacat=0" target="_blank"><img src="{{asset('img/ebay_logo.png')}}" alt="Ebay" class="searchLink"></a>
					<a href="https://www.google.com/search?source=hp&q=<?php echo htmlspecialchars($item->item_name); ?>&_sacat=0" target="_blank"><img src="{{asset('img/google_logo.png')}}" alt="Google" class="searchLink"></a>
					<a href="https://www.abebooks.co.uk/servlet/SearchResults?sts=t&cm_sp=SearchF-_-home-_-Results&kn=<?php echo htmlspecialchars($item->item_name); ?>&_sacat=0" target="_blank"><img src="{{asset('img/abe_books_logo.png')}}" alt="Abe Books" class="searchLink"></a>
					<a href="https://www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=<?php echo htmlspecialchars($item->item_name); ?>&_sacat=0" target="_blank"><img src="{{asset('img/amazon_logo.png')}}" alt="Amazon" class="searchLink"></a>
					<!-- <form action="{{route('inventory-search')}}" method="POST">
						@csrf
						<input type="hidden" name="query" value="{{$item->item_name}}">
						<input type="submit" value="Similar Items">
					</form> -->
				</p>

				<p>
					<a href="#">Conversion</a>
					|
					<a href="#">Disclaimers</a>
					|
					<a href="#">Authentication</a>
					|
					<a href="{{url('guides/size')}}">Size Guide</a>
				</p>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-2">
		<img src="data:image/png;base64,{{DNS2D::getBarcodePNG($item->id, "QRCODE")}}" alt="barcode" style="width: 35%;" />
	</div>
</div>
<hr>
<div class="card">
	<div class="card-header text-center">
		<h4 class="sectionHeader">
			Listing Information
		</h4>
	</div>

	<div class="card-body">
		<div class="row">
			<div class="col-sm-2">
				<h5>Gift Aid</h5>
				<div id="giftAid"></div>
			</div>

			<div class="col-sm-2">
				<h5>Est. Value</h5>
				<div id="estValue" style="width: 100%"></div>
			</div>

			<div class="col-sm-2">
				<h5>Confirmed Price</h5>
				<h5 style="display: inline-block; font-size: 1.6em;">&pound;</h5>
				<div id="price" style="display: inline-block;"></div>
			</div>

			<div class="col-sm-2">
				<h5 style="display: inline-block;">Category</h5>
				<a href="#" class="filterBtn" style="margin-left: 15px;">Find</a>
				<div id="category" style="width: 75%;"></div>
			</div>

			<div class="col-sm-4">
				<h5 style="display: inline-block;">Shipping</h5>
				<form action="#" method="post">
					<!-- Replace with react -->

					<select name="packaging" id="packaging">
						@foreach($packingTypes as $packingType)
							<option value="{{$packingType->id}}">{{$packingType->name}}</option>
						@endforeach
					</select>

					<select name="predictedShippingCarrier" id="predictedShippingCarrier">
						@foreach($shippingTypes as $shippingType)
							<option value="{{$shippingType->id}}">{{$shippingType->name}}</option>
						@endforeach
					</select>

					<input type="text" name="predictedShippingCost" id="predictedShippingCost" placeholder="Predicted Shipping Cost">
				</form>
			</div>
		</div>

		<hr>
		<div class="row">
			<div class="col-sm-6">
				<h5 class="sectionHeader">Description</h5>
				<div id="itemDesp"></div>

				<h5 class="sectionHeader">Listing Infomation</h5>

				<div class="row">
					<div class="col-sm-3">
						<h4>Weight</h4>
		     			<div id="weightInt"></div>
					</div>
					<div class="col-sm-3">
		     			<h4>Unit</h4>
		     			<div id="weightUnit" style="width: 30%;"></div>
					</div>
					<div class="col-sm-5">
						<h4>Weight Text</h4>
						<div id="weightText"></div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<h4>Size</h4>
						<div id="sizeText"></div>
					</div>
					<div class="col-sm-6">
						<h4>Condition</h4>
						<div id="condition"></div>
					</div>
				</div>
			</div>

			<div class="col-sm-6">
				<h5 class="sectionHeader">Photos</h5>

						<form action="{{route('photo-store', ['id' => $item->id])}}" method="POST" enctype="multipart/form-data">
							@csrf
							<fieldset style="border: solid 1px #ddd; border-bottom: none; padding: 15px;">
								<input type="hidden" name="itemId" value="{{$item->id}}">
								<input type="file" name="files[]" multiple style="display: block; padding: 0; margin-bottom: 15px; color: transparent;">
								<input type="submit" value="Upload">
							</fieldset>
						</form>
						<!-- <div style="display: block; height: 482px; background: #EEE; text-align: center; padding-top: 175px; font-size: 2em; border: dashed 1px #333;">Drop files here</div> -->

						<div class="row" style="border: solid 1px #ddd; border-top: none; margin: 0; padding: 15px 0;">
							<div class="col-sm-3">
								@foreach($item->photos as $photo)
									<img src="{{asset('item_photos/'.$photo->filename)}}" alt="Image unavailable" style="width: 100%; border-radius: 3px;">
									<a href="{{route('photo-update', ['id' => $item->id])}}">Edit</a>
								@endforeach
							</div>
						</div>
			</div>
		</div>
	</div>
</div>

<div class="row" style="margin-top: 15px;">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-header text-center">
				<h4 class="sectionHeader">Item Log</h4>
			</div>

			<div class="card-body">
				@if(count($actions) === 0)
					<h4>No actions in log (please report this)</h4>
				@else
				<table class="table">
					<tr>
						<th>Action</th>
						<th width="250">Date / Time</th>
						<th width="50">Person</th>
					</tr>

					@foreach($actions as $action)
					<tr>
						<td>{{$action->action}}</td>
						<td>{{$action->created_at}}</td>
						<?php
						$user = User::find($action->user_id);
						?>
						<td style="background: {{$user->color}}; font-weight: bold; color: #fff;">{{$user->name}}</td>
					</tr>
					@endforeach
				</table>
				@endif
			</div>
		</div>
	</div>
</div>

<script src="{{ asset('js/item.js') }}" defer></script>

@endsection
