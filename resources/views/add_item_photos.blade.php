@extends('layout.master')

@section('content')
<div class="row">
  <div class="col-sm-12">
    @if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
		@endif
    
    <h4>{{$item->item_name}}</h4>

    <form action="{{route('photo-store', ['id' => $item->id])}}" enctype="multipart/form-data" method="post">
      @csrf

      <input type="hidden" name="itemId" value="{{$item->id}}">

      <input type="file" name="files[]" multiple>

      <input type="submit" value="Upload">

    </form>
  </div>
</div>
@endsection
