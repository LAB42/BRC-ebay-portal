@extends('layout.master')

@section('content')
<hr>
<div class="row">
<hr>
@foreach($bears as $bear)
	<div class="col-sm-4 text-center">
		<h3>{{$bear['name']}}</h3>
		<h5>{{$bear['role']}}</h5>
		<img src="{{asset($bear['img'])}}" alt="" style="height: 125px; border-radius: 5px;">
		<hr>
	</div>
@endforeach
</div>
@endsection