@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<h4 class="sectionHeader">All items</h4>
		<h5>{{count($items)}}</h5>
		<table class="table" style="text-align: center;">
			<tr>
				<th>Date</th>
				<th>Area</th>
				<th>Shop Code</th>
				<th>Shop Name</th>				
				<th>Name</th>
				<th>Gift Aid</th>
				<th>Status</th>
			</tr>			
			@foreach($items as $item)		
			<tr>	
				<td>DD/MM/YY</td>
				<?php 
				$area = strtolower($item->area);
				$ac = preg_replace('$ $','', $area);								
				?>
				@if(!in_array($item->area, $areas))
				<td style="background: {{$colors[$ac]}}; color: #fff; font-weight: bold" width="85">
					{{$item->area}}
				@else
				<td>
					N/A
				@endif
				</td>

				<td>{{$item->shop_code}}</td>
				<td>{{$item->shop_name}}</td>
				<td><a href="{{url('/item/'.$item->id)}}">{{$item->item_name}}</a></td>
				<td>{{$item->gift_aid}}</td>
				<td
				@if($item->status == 'LISTED')
				style="background: {{$colors['listed']}}; color: #fff;
				@elseif($item->status == 'NIS')
				style="background: {{$colors['nis']}}; color: #fff;				
				@elseif($item->status == 'A')
				style="background: {{$colors['a']}}; color: #fff;			
				@endif
				vertical-align: middle; font-weight: bold;">
				{{$item->status}}</td>				
			</tr>
			@endforeach		
			
		</table>
	</div>
</div>
@endsection