@extends('layout.master')

@section('content')
	<form action="{{url('doBulkEstValue')}}" method="post">	
		@csrf
		<div class="row">
			<div class="col-sm-4">				
				<label for="items">Items</label>
				<p>One item per line. Avoid short names. Best performance with maximum of 25.</p>
				<textarea name="items" id="items" cols="70" rows="10"></textarea>
				<hr>
				<input type="submit" value="Run">
			</div>
			<div class="col-sm-8">
				@if(isset($results))
				<h3>Results</h3>
				<table class="table">
					<tr>
						<th>Item</th>
						<th>Average Value</th>
						<th>Results Data</th>
					</tr>					
					@foreach($results as $result)
					<tr>
						<td style="border-bottom: solid 2px #f00";>
							<strong>{{$result['name']}}</strong>
						</td>
						<td style="border-bottom: solid 2px #f00";>
							<strong>{{$result['average']}}</strong>
						</td>
						<td style="border-bottom: solid 2px #f00";>
							@foreach($result['rawData'] as $rd)
							<div class="row">
								<div class="col-sm-4">
									{{$rd['title']}}		
								</div>
								<div class="col-sm-4 text-center">
									&pound;{{$rd['value']}}
								</div>
								<div class="col-sm-4 text-center">
									<img src="{{$rd['image']}}" alt="" style="height: 75px; border-radius: 3px;">
								</div>
							</div>
							<br>
							@endforeach
						</td>											
					</tr>
					@endforeach
				</table>
				@endif
			</div>
		</div>
	</form>
@endsection