@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-6">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		@if(count($messages) === 0)
			<h2>No messages</h2>
		@else
		<table class="table text-center">
			<thead>
				<tr>
					<th>Read</th>
					<th>Delete</th>
					<th>ID</th>
					<th>From</th>
					<th>Subject</th>
					<th>Title</th>
				</tr>
			</thead>
			<tbody>
				@foreach($messages as $message)
				<tr>
					<td style="width: 5%;">
						@if($message->read)
							<span style="color: #04923e;">&#10004;</span>
						@else
							<span style="color: #7d1c23; font-size: 1.4em;">&times;</span>
						@endif
					</td>
					<td>
						<form action="{{route('feedback-delete', ['id' => $message->id])}}" method="POST">
							@method('DELETE')
							@csrf
							<input type="submit" value="[x]" class="filterBtn" style="color: #f00; background: none;">
						</form>
					</td>
					<td style="width: 5%;">{{$message->id}}</td>
					<td style="width: 15%;">{{$message->submitted_by}}</td>
					<td style="width: 15%;">{{strtoupper($message->subject)}}</td>
					<td style="width: 75%;"><a href="{{route('feedback-show', ['id' => $message->id])}}">{{$message->title}}</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@endif

	</div>
</div>
@endsection
