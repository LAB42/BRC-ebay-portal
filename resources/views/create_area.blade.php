@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form action="{{route('area-store')}}" method="POST">
			@csrf
			<label for="name">Name</label>
			<input type="text" name="name" id="name" size="40">

			<label for="area_manager">Area manager</label>
			<input type="text" name="area_manager" id="area_manager" size="40">
			
			<label for="area_manager_email">Area manager - email address</label>
			<input type="text" name="area_manager_email" id="area_manager_email" size="40">

			<label for="cluster_manager">Cluster manager</label>
			<input type="text" name="cluster_manager" id="cluster_manager" size="40">

			<label for="cluster_manager_email">Cluster manager - email address</label>
			<input type="text" name="cluster_manager_email" id="cluster_manager_email" size="40">

			<label for="notes">Notes</label>
			<textarea name="notes" id="notes" cols="40" rows="10"></textarea>

			<label for="color">Colour</label>
			<input type="color" name="color" id="color" style="padding: 0; height: 2.4em; width: 5%;">

			<br><br><!-- FIX IN CSS -->
			<input type="submit" value="Create area">
		</form>
	</div>
</div>
@endsection