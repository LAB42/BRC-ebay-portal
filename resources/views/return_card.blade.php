<?php use App\InventoryStatus; ?>
@extends('layout.master')
@section('printStyle')
<link rel="stylesheet" href="{{asset('css/returnCard.css')}}" media="print">
@endsection
@section('content')
@if(count($returnItems) === 0)
<div class="row">
	<div class="col-sm-12 text-center">
		<h2>No items to return</h2>
		<p>Just what we like to see :)</p>
	</div>
</div>
@else
<div class="row">
	<div class="col sm-4">
		<h2 class="sectionHeader">Returns</h2><br>
		<div class="row">
			<div class="col-sm-6">
				<h2>
					@if(!empty($logsheet->shop->area->name))
						{{$logsheet->shop->area->name}}
					@else
						No Area
					@endif
				</h2>		
				<h2>{{$logsheet->shop->name}}</h2>
				<h2>{{$logsheet->shop->code}}</h2>	
			</div>
			<div class="col-sm-6">
				<h2>{{date('d/m/y', time())}}</h2>
				<h2>Processed by: NH</h2>
				<h2>Number of items: {{count($returnItems)}}</h2>
			</div>
		</div>
	</div>
	<div class="col-sm-8 text-center">		
		<h2 class="sectionHeader">Ebay Team Message</h2>
		<p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere quo eveniet dolores, delectus mollitia aspernatur asperiores non, aliquam amet nam voluptas corporis molestiae dolorem eum tenetur itaque, quis repellendus. Rem.</span>
		<span>Maiores architecto molestias, nemo, id cumque expedita suscipit qui repellendus quos similique eaque voluptatem maxime, perferendis dolore iusto sequi iste. Aperiam quos, possimus voluptatum cum labore. Cum labore dicta aliquid.</span>
		<span>Ducimus accusantium pariatur error quaerat ab autem ea. Nesciunt perspiciatis omnis, incidunt, tempora architecto, quam aspernatur rerum accusamus praesentium facere sit quidem repudiandae vitae est cumque minima! Sunt, a, voluptate!</span></p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12">		
		<table class="table">
			<tr>
				<th style="color: #333; background: #EEE;">#</th>
				<th style="color: #333; background: #EEE;">Name</th>
				<th style="color: #333; background: #EEE;">Info</th>
				<th style="color: #333; background: #EEE;">Value</th>
				<th style="color: #333; background: #EEE;">Reason</th>
			</tr>			

			@foreach($returnItems as $item)
			<tr>
				<td class="text-center">{{$item->id}}</td>
				<td class="text-center">{{$item->name}}</td>
				<td class="text-center">{{$item->info}}</td>
				<td class="text-center">&pound;{{$item->value}}</td>
				<td class="text-center">RTS - Low Value</td>
			</tr>			
			@endforeach			
		</table>
	</div>
</div>
@endif
@endsection