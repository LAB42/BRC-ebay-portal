@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12 text-center">
		<h3 style="font-weight: bold;">Week {{date('W', time())}}</h3>
		<h3 class="sectionHeader">Totals</h3>
	</div>
</div>
<div class="row">
	<div class="col-sm-4 text-center">
		<div class="card card-body">
			<h3>Listed</h3>
			<h2>[###]</h2>
		</div>
	</div>
	<div class="col-sm-4 text-center">
		<div class="card card-body">
			<h3>Value</h3>
			<h2>[&pound;####.##]</h2>
		</div>
	</div>
	<div class="col-sm-4 text-center">
		<div class="card card-body">
			<h3>ATV</h3>
			<h2>[&pound;##.##]</h2>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">
		<h5 class="sectionHeader">Filter By</h5><br>
		@foreach($filters as $filter)
			<a href="{{route('listings-filter', ['id' => $filter->id])}}" class="filterBtn">{{strtoupper($filter->section)}}</a>
		@endforeach
	</div>
</div>
<hr>
@if(isset($currentFilter) && strtolower($currentFilter->section) !== strtolower($currentTask))
<div class="row">
	<div class="col-sm-12 text-center">
		<form action="{{route('warehouse-check-in')}}" method="post">
			@csrf
			<input type="hidden" name="process" value="{{$currentFilter->id}}">
			<input type="hidden" name="redirect" value="none">
			<input type="image" src="{{asset('img/icons/check_in.svg')}}" alt="Check In" style="width: 64px;">
			<p>Check in for {{$currentFilter->section}}</p>
		</form>
	</div>
</div>
@endif
<div class="row">
	<div class="col-sm-12">
		@if(count($items) === 0)
		<h3 class="text-center">No items to process</h3>
		@else
		<table class="table">
			<tr class="text-center">
				<th width="50">ID</th>
				<th width="350">Shop</th>
				<th>Item Name</th>
				<th width="600"><!-- BLANK --></th>
				<th width="75">Price</th>
				<th width="150">Location</th>
			</tr>

			@foreach($items as $item)
			<tr class="text-center">
				<td>{{$item->id}}</td>
				<td>
					<p><span style="background: {{$item->shop->area->color}}; padding: 5px 10px; color: #fff;">{{$item->shop->area->name}}</span></p>
					<h5>{{$item->shop->name}} | {{$item->shop->code}}</h5>
				</td>
				<td>
						<h5><a href="{{route('item', $item->id)}}">{{$item->item_name}}</a></h5>
				</td>
				<td>
					<a href="{{route('list', ['id' => $item->id])}}" class="filterBtn" style="font-size: 0.9em;">Create Listing</a>
					<a href="{{route('item', ['id' => $item->id])}}" class="filterBtn" style="font-size: 0.9em;">View</a>
				</td>
				<td>
					@if($item->price === NULL)
					N/A
					@else
					<span style="font-size: 1.2em;">&pound;{{$item->price}}</span>
					@endif
				</td>
				<td><span style="font-size: 1.2em">{{strtoupper($item->location)}}</span></td>
			</tr>

			@endforeach
		@endif
		</table>
	</div>
</div>
@endsection
