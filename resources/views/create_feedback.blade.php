@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		<form action="{{route('feedback-store')}}" method="POST">
			@csrf
			<label for="submitted_by">From</label>
			<input type="text" name="submitted_by" id="submitted_by" value="{{Auth::user()->name}}" size="30">

			<label for="subject">Subject</label>
			<select name="subject" id="subject" autocomplete="off">
				<option value="" selected="selected" disabled>---Select---</option>
				<option value="support">Need help using application</option>
				<option value="general">Submit feedback about the application</option>
				<option value="suggestion">Submit an idea for the application</option>
				<option value="error">Report an error message or problem with the application</option>
			</select>

			<label for="title">Title</label>
			<input type="text" name="title" id="title" size="75" placeholder="Brief overview">

			<label for="message">Message</label>
			<textarea name="message" id="message" cols="75" rows="10" placeholder="Please provide as much detail as possible"></textarea>

			<br><br><!-- FIX IN CSS -->
			<input type="submit" value="Submit Feedback" class="rounded">

		</form>
	</div>
</div>
@endsection
