@extends('layout.master')
@section('printStyle')
<link rel="stylesheet" href="{{asset('css/invoice.css')}}" media="print">
<style>
	#pageTitle{
		display: none;
	}
</style>
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12 text-center">
		<img src="{{asset('img/brc_logo.png')}}" alt="BRC Logo" width="24%">		
	</div>
</div>
<div class="row">	
	<div class="col-sm-4">
		<h4 class="sectionHeader">
			Payment
		</h4>
		<p style="font-size: 1.2em;">
			[NAME]<br>
			[ADDRESS LINE 1]<br>
			[ADDRESS LINE 2]<br>
			[TOWN]<br>
			[COUNTY]<br>
			[POSTCODE]<br>
			[COUNTRY]					
		</p>
	</div>
	<div class="col-sm-4 text-center">
		<h3 style="margin-top: 42px; color: #9d1f21;">Thank you for <br>your order and support.</h3>				
	</div>
	<div class="col-sm-4 text-right">
		<h4 class="sectionHeader">Delivery</h4>
		<p style="font-size: 1.2em;">
			[NAME]<br>
			[ADDRESS LINE 1]<br>
			[ADDRESS LINE 2]<br>
			[TOWN]<br>
			[COUNTY]<br>
			[POSTCODE]<br>
			[COUNTRY]					
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 text-center">
		<h2>[Order Number]</h2>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<h4 class="sectionHeader">Items</h4>
		<table class="table">
			<tr>
				<th width="125">SKU</th>
				<th>Item</th>
				<th width="75" class="text-center">Qty</th>
				<th width="125" class="text-right">Cost</th>
			</tr>
			<tr>
				<td>[SKU]</td>
				<td>[Item Name]</td>
				<td class="text-center">[#]</td>
				<td class="text-right">[&pound;£##.##]</td>
			</tr>
			<tr>
				<td>[SKU]</td>
				<td>[Item Name</td>
				<td class="text-center">[#]</td>
				<td class="text-right">[&pound;£##.##]</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td class="text-center"><strong>Total</strong></td>
				<td class="text-right"><strong>[&pound;£##.##]</strong></td>
			</tr>
		</table>
	</div>	
</div>
<div class="row">
	<div class="col-sm-6 text-center">
		<h5 class="sectionHeader">Contact</h5>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium repudiandae inventore, ducimus, optio velit maxime facere laborum provident, natus repellat fugiat aspernatur vitae vero corrupti ratione sit quae. Dignissimos, beatae!</p>	
	</div>
	<div class="col-sm-6 text-center">
		<h5 class="sectionHeader">Promotion</h5>		
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium repudiandae inventore, ducimus, optio velit maxime facere laborum provident, natus repellat fugiat aspernatur vitae vero corrupti ratione sit quae. Dignissimos, beatae!</p>	
	</div>	
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">
		<h5 class="sectionHeader">Legal</h5>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit quas nisi iste atque voluptates, quidem illo qui, ipsum dignissimos nihil reiciendis eaque ratione voluptate voluptas! Nobis porro quis, dignissimos corrupti.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti sequi ut possimus tenetur ullam vero, consectetur debitis consequuntur suscipit laudantium officia repudiandae quisquam quas nisi praesentium aliquid ex distinctio harum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa accusamus, reiciendis neque perspiciatis, dicta commodi fuga, at mollitia magni doloremque quae minima dolor maiores sapiente. Quam fuga, molestiae nam fugiat.
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 text-center">
		<img src="{{asset('img/brc_logo.png')}}" alt="BRC Logo" width="24%">		
	</div>
</div>
<div class="row">	
	<div class="col-sm-4">
		<h4 class="sectionHeader">
			Payment
		</h4>
		<p style="font-size: 1.2em;">
			[NAME]<br>
			[ADDRESS LINE 1]<br>
			[ADDRESS LINE 2]<br>
			[TOWN]<br>
			[COUNTY]<br>
			[POSTCODE]<br>
			[COUNTRY]					
		</p>
	</div>
	<div class="col-sm-4 text-center">
		<h3 style="margin-top: 42px; color: #9d1f21;">Thank you for <br>your order and support.</h3>				
	</div>
	<div class="col-sm-4 text-right">
		<h4 class="sectionHeader">Delivery</h4>
		<p style="font-size: 1.2em;">
			[NAME]<br>
			[ADDRESS LINE 1]<br>
			[ADDRESS LINE 2]<br>
			[TOWN]<br>
			[COUNTY]<br>
			[POSTCODE]<br>
			[COUNTRY]					
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 text-center">
		<h2>[Order Number]</h2>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<h4 class="sectionHeader">Items</h4>
		<table class="table">
			<tr>
				<th width="125">SKU</th>
				<th>Item</th>
				<th width="75" class="text-center">Qty</th>
				<th width="125" class="text-right">Cost</th>
			</tr>
			<tr>
				<td>[SKU]</td>
				<td>[Item Name]</td>
				<td class="text-center">[#]</td>
				<td class="text-right">[&pound;£##.##]</td>
			</tr>
			<tr>
				<td>[SKU]</td>
				<td>[Item Name</td>
				<td class="text-center">[#]</td>
				<td class="text-right">[&pound;£##.##]</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td class="text-center"><strong>Total</strong></td>
				<td class="text-right"><strong>[&pound;£##.##]</strong></td>
			</tr>
		</table>
	</div>	
</div>
<div class="row">
	<div class="col-sm-6 text-center">
		<h5 class="sectionHeader">Contact</h5>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium repudiandae inventore, ducimus, optio velit maxime facere laborum provident, natus repellat fugiat aspernatur vitae vero corrupti ratione sit quae. Dignissimos, beatae!</p>	
	</div>
	<div class="col-sm-6 text-center">
		<h5 class="sectionHeader">Promotion</h5>		
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium repudiandae inventore, ducimus, optio velit maxime facere laborum provident, natus repellat fugiat aspernatur vitae vero corrupti ratione sit quae. Dignissimos, beatae!</p>	
	</div>	
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">
		<h5 class="sectionHeader">Legal</h5>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit quas nisi iste atque voluptates, quidem illo qui, ipsum dignissimos nihil reiciendis eaque ratione voluptate voluptas! Nobis porro quis, dignissimos corrupti.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti sequi ut possimus tenetur ullam vero, consectetur debitis consequuntur suscipit laudantium officia repudiandae quisquam quas nisi praesentium aliquid ex distinctio harum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa accusamus, reiciendis neque perspiciatis, dicta commodi fuga, at mollitia magni doloremque quae minima dolor maiores sapiente. Quam fuga, molestiae nam fugiat.
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 text-center">
		<img src="{{asset('img/brc_logo.png')}}" alt="BRC Logo" width="24%">		
	</div>
</div>
<div class="row">	
	<div class="col-sm-4">
		<h4 class="sectionHeader">
			Payment
		</h4>
		<p style="font-size: 1.2em;">
			[NAME]<br>
			[ADDRESS LINE 1]<br>
			[ADDRESS LINE 2]<br>
			[TOWN]<br>
			[COUNTY]<br>
			[POSTCODE]<br>
			[COUNTRY]					
		</p>
	</div>
	<div class="col-sm-4 text-center">
		<h3 style="margin-top: 42px; color: #9d1f21;">Thank you for <br>your order and support.</h3>				
	</div>
	<div class="col-sm-4 text-right">
		<h4 class="sectionHeader">Delivery</h4>
		<p style="font-size: 1.2em;">
			[NAME]<br>
			[ADDRESS LINE 1]<br>
			[ADDRESS LINE 2]<br>
			[TOWN]<br>
			[COUNTY]<br>
			[POSTCODE]<br>
			[COUNTRY]					
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 text-center">
		<h2>[Order Number]</h2>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<h4 class="sectionHeader">Items</h4>
		<table class="table">
			<tr>
				<th width="125">SKU</th>
				<th>Item</th>
				<th width="75" class="text-center">Qty</th>
				<th width="125" class="text-right">Cost</th>
			</tr>
			<tr>
				<td>[SKU]</td>
				<td>[Item Name]</td>
				<td class="text-center">[#]</td>
				<td class="text-right">[&pound;£##.##]</td>
			</tr>
			<tr>
				<td>[SKU]</td>
				<td>[Item Name</td>
				<td class="text-center">[#]</td>
				<td class="text-right">[&pound;£##.##]</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td class="text-center"><strong>Total</strong></td>
				<td class="text-right"><strong>[&pound;£##.##]</strong></td>
			</tr>
		</table>
	</div>	
</div>
<div class="row">
	<div class="col-sm-6 text-center">
		<h5 class="sectionHeader">Contact</h5>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium repudiandae inventore, ducimus, optio velit maxime facere laborum provident, natus repellat fugiat aspernatur vitae vero corrupti ratione sit quae. Dignissimos, beatae!</p>	
	</div>
	<div class="col-sm-6 text-center">
		<h5 class="sectionHeader">Promotion</h5>		
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium repudiandae inventore, ducimus, optio velit maxime facere laborum provident, natus repellat fugiat aspernatur vitae vero corrupti ratione sit quae. Dignissimos, beatae!</p>	
	</div>	
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">
		<h5 class="sectionHeader">Legal</h5>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit quas nisi iste atque voluptates, quidem illo qui, ipsum dignissimos nihil reiciendis eaque ratione voluptate voluptas! Nobis porro quis, dignissimos corrupti.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti sequi ut possimus tenetur ullam vero, consectetur debitis consequuntur suscipit laudantium officia repudiandae quisquam quas nisi praesentium aliquid ex distinctio harum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa accusamus, reiciendis neque perspiciatis, dicta commodi fuga, at mollitia magni doloremque quae minima dolor maiores sapiente. Quam fuga, molestiae nam fugiat.
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 text-center">
		<img src="{{asset('img/brc_logo.png')}}" alt="BRC Logo" width="24%">		
	</div>
</div>
<div class="row">	
	<div class="col-sm-4">
		<h4 class="sectionHeader">
			Payment
		</h4>
		<p style="font-size: 1.2em;">
			[NAME]<br>
			[ADDRESS LINE 1]<br>
			[ADDRESS LINE 2]<br>
			[TOWN]<br>
			[COUNTY]<br>
			[POSTCODE]<br>
			[COUNTRY]					
		</p>
	</div>
	<div class="col-sm-4 text-center">
		<h3 style="margin-top: 42px; color: #9d1f21;">Thank you for <br>your order and support.</h3>				
	</div>
	<div class="col-sm-4 text-right">
		<h4 class="sectionHeader">Delivery</h4>
		<p style="font-size: 1.2em;">
			[NAME]<br>
			[ADDRESS LINE 1]<br>
			[ADDRESS LINE 2]<br>
			[TOWN]<br>
			[COUNTY]<br>
			[POSTCODE]<br>
			[COUNTRY]					
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 text-center">
		<h2>[Order Number]</h2>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<h4 class="sectionHeader">Items</h4>
		<table class="table">
			<tr>
				<th width="125">SKU</th>
				<th>Item</th>
				<th width="75" class="text-center">Qty</th>
				<th width="125" class="text-right">Cost</th>
			</tr>
			<tr>
				<td>[SKU]</td>
				<td>[Item Name]</td>
				<td class="text-center">[#]</td>
				<td class="text-right">[&pound;£##.##]</td>
			</tr>
			<tr>
				<td>[SKU]</td>
				<td>[Item Name</td>
				<td class="text-center">[#]</td>
				<td class="text-right">[&pound;£##.##]</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td class="text-center"><strong>Total</strong></td>
				<td class="text-right"><strong>[&pound;£##.##]</strong></td>
			</tr>
		</table>
	</div>	
</div>
<div class="row">
	<div class="col-sm-6 text-center">
		<h5 class="sectionHeader">Contact</h5>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium repudiandae inventore, ducimus, optio velit maxime facere laborum provident, natus repellat fugiat aspernatur vitae vero corrupti ratione sit quae. Dignissimos, beatae!</p>	
	</div>
	<div class="col-sm-6 text-center">
		<h5 class="sectionHeader">Promotion</h5>		
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium repudiandae inventore, ducimus, optio velit maxime facere laborum provident, natus repellat fugiat aspernatur vitae vero corrupti ratione sit quae. Dignissimos, beatae!</p>	
	</div>	
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">
		<h5 class="sectionHeader">Legal</h5>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit quas nisi iste atque voluptates, quidem illo qui, ipsum dignissimos nihil reiciendis eaque ratione voluptate voluptas! Nobis porro quis, dignissimos corrupti.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti sequi ut possimus tenetur ullam vero, consectetur debitis consequuntur suscipit laudantium officia repudiandae quisquam quas nisi praesentium aliquid ex distinctio harum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa accusamus, reiciendis neque perspiciatis, dicta commodi fuga, at mollitia magni doloremque quae minima dolor maiores sapiente. Quam fuga, molestiae nam fugiat.
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 text-center">
		<img src="{{asset('img/brc_logo.png')}}" alt="BRC Logo" width="24%">		
	</div>
</div>
<div class="row">	
	<div class="col-sm-4">
		<h4 class="sectionHeader">
			Payment
		</h4>
		<p style="font-size: 1.2em;">
			[NAME]<br>
			[ADDRESS LINE 1]<br>
			[ADDRESS LINE 2]<br>
			[TOWN]<br>
			[COUNTY]<br>
			[POSTCODE]<br>
			[COUNTRY]					
		</p>
	</div>
	<div class="col-sm-4 text-center">
		<h3 style="margin-top: 42px; color: #9d1f21;">Thank you for <br>your order and support.</h3>				
	</div>
	<div class="col-sm-4 text-right">
		<h4 class="sectionHeader">Delivery</h4>
		<p style="font-size: 1.2em;">
			[NAME]<br>
			[ADDRESS LINE 1]<br>
			[ADDRESS LINE 2]<br>
			[TOWN]<br>
			[COUNTY]<br>
			[POSTCODE]<br>
			[COUNTRY]					
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 text-center">
		<h2>[Order Number]</h2>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<h4 class="sectionHeader">Items</h4>
		<table class="table">
			<tr>
				<th width="125">SKU</th>
				<th>Item</th>
				<th width="75" class="text-center">Qty</th>
				<th width="125" class="text-right">Cost</th>
			</tr>
			<tr>
				<td>[SKU]</td>
				<td>[Item Name]</td>
				<td class="text-center">[#]</td>
				<td class="text-right">[&pound;£##.##]</td>
			</tr>
			<tr>
				<td>[SKU]</td>
				<td>[Item Name</td>
				<td class="text-center">[#]</td>
				<td class="text-right">[&pound;£##.##]</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td class="text-center"><strong>Total</strong></td>
				<td class="text-right"><strong>[&pound;£##.##]</strong></td>
			</tr>
		</table>
	</div>	
</div>
<div class="row">
	<div class="col-sm-6 text-center">
		<h5 class="sectionHeader">Contact</h5>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium repudiandae inventore, ducimus, optio velit maxime facere laborum provident, natus repellat fugiat aspernatur vitae vero corrupti ratione sit quae. Dignissimos, beatae!</p>	
	</div>
	<div class="col-sm-6 text-center">
		<h5 class="sectionHeader">Promotion</h5>		
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium repudiandae inventore, ducimus, optio velit maxime facere laborum provident, natus repellat fugiat aspernatur vitae vero corrupti ratione sit quae. Dignissimos, beatae!</p>	
	</div>	
</div>
<hr>
<div class="row">
	<div class="col-sm-12 text-center">
		<h5 class="sectionHeader">Legal</h5>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit quas nisi iste atque voluptates, quidem illo qui, ipsum dignissimos nihil reiciendis eaque ratione voluptate voluptas! Nobis porro quis, dignissimos corrupti.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti sequi ut possimus tenetur ullam vero, consectetur debitis consequuntur suscipit laudantium officia repudiandae quisquam quas nisi praesentium aliquid ex distinctio harum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa accusamus, reiciendis neque perspiciatis, dicta commodi fuga, at mollitia magni doloremque quae minima dolor maiores sapiente. Quam fuga, molestiae nam fugiat.
		</p>
	</div>
</div>
@endsection