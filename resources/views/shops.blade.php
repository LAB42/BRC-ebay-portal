@extends('layout.master')

@section('content')	
<div class="row">
	<div class="col-sm-12">
		<h4>Find a Shop</h4>
		<form action="{{route('doFindShop')}}" method="post">
			@csrf
			<label for="query" style="display: none;">Shop Code / Name</label>
			<input type="text" name="query" id="query" placeholder="Shop Code/Name" autofocus>

			<input type="submit" value="Search" style="margin-left: -5px;">
		</form>

		<hr>
		<h5 class="sectionHeader">Areas</h5>
		<ul class="list-inline">	
			@foreach($areas as $area)
			<li class="list-inline-item"><a style="font-weight: bold; color: #FFF; background: {{$area->color}};" href="{{url('shops/area/'.$area->id)}}" class="filterBtn">{{strtoupper($area->name)}}</a></li>
			@endforeach
		</ul>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-6">			
		<table class="table">
			<tr class="text-center">
				<th>Area</th>
				<th>Code</th>
				<th class="text-left">Name</th>
			</tr>
			@foreach($shops as $shop)
			<tr class="text-center">
					@if(in_array($shop->area->name, $areaList))
					<td style="
					background: {{$colors[strtolower(preg_replace('$ $', '', $shop->area->name))]}};
					text-align: center; 
					font-weight: bold;
					width: 15%;
					">
						<span style="background: #eee; padding: 5px; text-transform: uppercase;">{{$shop->area->name}}</span>
					@else
					<td>
						N/A
					@endif
					</td>
				<td style="width: 15%;">{{$shop->code}}</td>
				<td class="text-left"><a href="{{url('shops/'.$shop->id)}}">{{$shop->name}}</a></td>
			</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection