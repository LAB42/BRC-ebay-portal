@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<a href="{{url('logsheet/generate')}}" class="cta">Add Logsheet</a>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-3">
		<div class="card card-body">
			<h4 class="sectionHeader">Filter</h4><br>
			<div style="display: inline-block;">
				<a href="{{route('logbook')}}" class="filterBtn">Show all</a>
				<a href="{{route('logbookFilter', 'received')}}" class="filterBtn">Received</a>
				<a href="{{route('logbookFilter', 'not-received')}}" class="filterBtn">Not Received</a>
			</div>

			<form action="{{route('logbookFilter', 'search')}}" method="post">
				@csrf
				<label for="shopCode">Shop Code</label>
				<input type="text" name="shopCode" id="shopCode">
				<input type="submit" class="btn" value="Search">
			</form>

			<form action="{{route('logbookFilter', 'crateCode')}}" method="post">
				@csrf
				<label for="shopCode">Crate Code</label>
				<input type="text" name="crateCode" id="crateCode">
				<input type="submit" class="btn" value="Search">
			</form>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-6">
		@if(count($entries) === 0)
		<h3>No logsheets</h3>
		@else
		<table class="table table-bordered">
			<thead>
				<tr>
					<th style="width: 5%" class="text-center">Received</th>
					<th style="width: 5%" class="text-center">ID</th>
					<th style="width: 50%" class="text-center">Shop</th>
					<th style="width: 10%" class="text-center">Area</th>
					<th style="width: 10%" class="text-center">Code</th>
					<th style="width: 10%" class="text-center">Due</th>
					<th style="width: 10%" class="text-center">Submitted</th>
				</tr>
			</thead>
			<tbody>
			@foreach($entries as $entry)
				<tr>
					<td class="text-center" style="font-size: 1.4em;">

						@if($entry->received_by !== NULL && $entry->received_date !== NULL)
							<span style="color: #04923e;">&#10004;</span>
						@else
							<span style="color: #7d1c23; font-size: 1.4em;">&times;</span>
						@endif
					</td>
					<td class="text-center">{{$entry->id}}</td>
					<td class="text-center"><a href="{{url('logsheet/'.$entry->id)}}">{{$entry->shop->name}}</a></td>
					<td class="text-center">{{$entry->shop->area->name}}</td>
					<td class="text-center">{{$entry->shop->code}}</td>
					<td class="text-center">{{date('d/m/y', strtotime($entry->due_date))}}</td>
					<td class="text-center">{{date('d/m/y', strtotime($entry->created_at))}}</td>
				</tr>
			</tbody>
			@endforeach
		</table>
		@endif
	</div>
</div>

@endsection
