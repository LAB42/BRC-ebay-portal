@extends('layout.master')

@section('content')

<form action="#" method="post">

@foreach($results as $r)
    @foreach($r->NameRecommendation as $nr)
     	<label for="{{$nr->Name}}">{{$nr->Name}}</label>
        <!-- $nr->ValidationRules->SelectionMode -->
        <select name="{{$nr->Name}}Values" id="{{$nr->Name}}Values">
        @foreach($nr->ValueRecommendation as $vr)
            <option value="{{$vr->Value}}">{{$vr->Value}}</option>
        @endforeach
		</select>
    @endforeach
@endforeach

</form>

@endsection