@extends('layout.master')

@section('content')
<table class="table">
	<tr>
		<td>ID</td>
		<td>Name</td>
	</tr>
	@foreach($results as $result)
	<tr>
		<td>{{$result['id']}}</td>
		<td>{{$result['name']}}</td>
	</tr>
	@endforeach	
</table>

@endsection